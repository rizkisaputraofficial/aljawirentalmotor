<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        security_access();
        clearSession();
        // Your own constructor code

    }

    public function index()
    {


        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('home/index');
        $this->load->view('template/footer');

        // $data = $this->karyawan->getAllKaryawan();

    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Denda extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        security_access();
        clearSession();
        // Your own constructor code
        $this->load->model('Denda_model', 'denda');
    }

    public function index()
    {
        $data['title'] = "Data Denda";
        $data['data'] = $this->denda->getAllData();
        load_view('denda/index', $data);
    }

    public function tambah()
    {
        $data = $this->denda->tambah();
        redirect_back($data, 'Ditambah');
    }

    public function edit()
    {
        $data = $this->denda->edit();
        redirect_back($data, 'Diubah');
    }

    public function delete()
    {
        $data = $this->denda->delete();
        redirect_back($data, 'Dihapus');
    }
}

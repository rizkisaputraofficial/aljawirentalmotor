<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Statistik extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		security_access();
		clearSession();
		// Your own constructor code
		// $this->load->model('Transaksi_model', 'transaksi');
		// $this->load->model('Motor_model', 'motor');
		// $this->load->model('Customer_model', 'customer');
	}

	public function index()
	{


		$data['title'] = "Data Transaksi";
		load_view('statistik/index', $data);
	}


	public function getAllTransaksiByMonth()
	{

		$tahun = $this->input->post('tahunKirimKeControler');


		$query = "SELECT MONTH(tanggal_rental) AS label, COUNT(id_transaksi) AS `y` FROM transaksi WHERE tanggal_rental LIKE '%" . $tahun . "%'
					GROUP BY label";
		$data = $this->db->query($query)->result_array();

		foreach ($data as $value) {
			$dataTransaksi[] = [
				'label' => montString($value['label']),
				'y' =>  (int)$value['y']
			];
		}
		header('Content-Type: application/json');
		echo json_encode($dataTransaksi);
	}






	public function getAllPendapatanByMonth()
	{

		$tahun = $this->input->post('tahun');


		$query = "SELECT  MONTH(tanggal_pengembalian) AS label, SUM(total_harga + total_denda) AS `y` FROM transaksi
                WHERE status_transaksi = 2  AND  tanggal_rental LIKE '%" . $tahun . "%'
                GROUP BY MONTH(tanggal_pengembalian)";

		$data = $this->db->query($query)->result_array();

		foreach ($data as $value) {
			$dataPendapatan[] = [
				'label' => montString($value['label']),
				'y' =>  (int)$value['y']
			];
		}
		header('Content-Type: application/json');
		echo json_encode($dataPendapatan);
	}
}

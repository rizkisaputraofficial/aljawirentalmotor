<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Motor extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        security_access();
        clearSession();
        // Your own constructor code
        $this->load->model('Motor_model', 'motor');
    }

    public function test()
    {
        $data = $this->motor->getDataTarif();
    }

    public function index()
    {
        $data['title'] = "Data motor";
        $data['data'] = $this->motor->getAllData();
        load_view('motor/index', $data);
    }
    public function detail($id)
    {
        $data['title'] = "Detail";
        $data['data'] = $this->motor->getDataById($id);
        $data['tarif'] = $this->motor->getDataTarifByIdMotor($id);
        $data['jenisTarif'] = $this->motor->getDataJenisTarifByIdMotor($id);
        load_view('motor/detail', $data);
    }



    public function tambah()
    {
        $data = $this->motor->tambah();
        redirect_back($data, 'Ditambah');
    }
    public function tambahTarif()
    {
        // var_dump($_POST);
        $data = $this->motor->tambahTarif();
        redirect_back($data, 'Ditambah');
    }



    public function edit()
    {
        $data = $this->motor->edit();
        redirect_back($data, 'Diubah');
    }

    public function editTarif()
    {
        $data = $this->motor->editTarif();
        redirect_back($data, 'Diubah');
    }

    public function delete()
    {
        $data = $this->motor->delete();
        redirect_back($data, 'Dihapus');
    }

    public function deleteTarif()
    {
        $data = $this->motor->deleteTarif();
        redirect_back($data, 'Dihapus');
    }
}

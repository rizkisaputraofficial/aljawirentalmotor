<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $this->load->view('auth/index');
    }

    public function prosesLogin()
    {

        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $user = $this->db->get_where('user', ['username' => $username, 'password' => $password])->row_array();
        if (!empty($user)) {
            $data = [

                'id_user' => $user['id_user'],
                'nama' => $user['nama'],
                'username' => $user['username'],
                'password' => $user['password'],
                'level' => $user['level'],
            ];

            $this->session->set_userdata($data);
            redirect('home');
        } else {
            $this->session->set_flashdata('message', ' <div class=" alert alert-danger" role="alert"> Password salah </div>');
            redirect('auth');
        }
    }




    // public function logout()
    // {

    //     $this->session->unset_userdata('username');
    //     $this->session->unset_userdata('password');
    //     $this->session->unset_userdata('role');
    //     $this->session->unset_userdata('nama_customer');
    //     $this->session->unset_userdata('id_admin');
    //     $this->session->unset_userdata('id_customer');

    //     $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible">
    //                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    //                 <span><i class="icon fas fa-check"></i> Logout Berhasil!</span>
    //             </div>');

    //     redirect('auth/index');
    // }

    public function logout()
    {
        $this->session->sess_destroy();

        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <span><i class="icon fas fa-check"></i> Logout Berhasil!</span>
                </div>');
        redirect('auth');
    }

    public function AccessDenied()
    {

        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');

        $this->session->unset_userdata('token');
        $this->session->set_flashdata('message', '<div class=" alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button><strong>Access Denied!</strong></div>');
        redirect('auth');
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Pagination extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        security_access();
        clearSession();
    }

    public function index()
    {
        $show = inpos('show');
        $this->session->set_userdata(['show' => $show]);

        echo json_encode(1);
    }
    public function show()
    {
        $data = $this->session->userdata('show');
        var_dump($data);
    }

    //paginastion
}

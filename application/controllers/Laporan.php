<?php

class Laporan extends CI_controller
{

	public function __construct()
	{
		parent::__construct();
		security_access();
		clearSession();
		// Your own constructor code
		$this->load->model('Transaksi_model', 'transaksi');
	}

	public function index()
	{


		$data['title'] = 'Laporan Setiap Bulan';
		$bln = $this->input->get('bln');
		$thn = $this->input->get('thn');
		$data['bln'] = $bln;
		$data['thn'] =  $thn;
		$data['laporan'] = $this->db->query("SELECT * FROM transaksi LEFT JOIN customer ON transaksi.id_customer_fk=customer.id_customer LEFT JOIN motor ON transaksi.id_motor_fk=motor.id_motor WHERE MONTH(tanggal_pengembalian) = '$bln' AND YEAR(tanggal_pengembalian) = '$thn' ")->result_array();

		load_view('laporan/laporan', $data);
		// var_dump($_GET);
		// die();
		// if($this->form_validation->run() == FALSE){
		// 	load_view('laporan/laporan', $data);
		// } else{

		// 	$data['bln'] = $bln;
		// 	$data['thn'] = $thn;
		// 	$data['laporan'] = $this->db->query("SELECT * FROM transaksi LEFT JOIN customer ON transaksi.id_customer_fk=customer.id_customer LEFT JOIN motor ON transaksi.id_motor_fk=motor.id_motor WHERE MONTH(tanggal_pengembalian) = '$bln' AND YEAR(tanggal_pengembalian) = '$thn' ")->result_array();

		// 	load_view('laporan/laporan', $data);
		// }
	}

	public function print_laporan()
	{
		$bln = $this->input->get('bln');
		$thn = $this->input->get('thn');
		$data['title'] = 'Laporan Print Out Transaksi';
		$data['bln'] = $bln;
		$data['thn'] = $thn;

		// $data['date'] = $this->transaksi->bulan($date);
		$download = false;
		$this->pdf->setPaper('A4', 'landscape');
		// $this->pdf->filename = "Invoice Penawaran.pdf";
		$data['laporan'] = $this->db->query("SELECT * FROM transaksi LEFT JOIN customer ON transaksi.id_customer_fk=customer.id_customer LEFT JOIN motor ON transaksi.id_motor_fk=motor.id_motor
		 WHERE MONTH(tanggal_pengembalian) = '$bln' AND YEAR(tanggal_pengembalian) = '$thn' ")->result_array();

		$this->pdf->load_view('laporan/print_laporan', $data, $download);
	}
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Register extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Customer_model', 'customer');
    }

    public function index()
    {
        $data['title'] = "Register";
        load_view_customer('auth/register', $data);
    }

    public function prosesRegister()
    {

        $data = $this->customer->tambah();
        redirect('auth');
    }
}
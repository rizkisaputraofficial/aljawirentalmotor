<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Customer_model', 'customer');
    }

    public function index()
    {

        $data['title'] = "Data Customer";
        $data['customer'] = $this->customer->getAllDataCustomer();
        // var_dump($data);
        load_view('customer/customer', $data);
    }

    public function detail($id)
    {
        $data['title'] = "Detail Customer";
        $data['customer'] = $this->customer->getDataById($id);
        $data['transaksi'] = $this->customer->getDataTransaksiById($id);
        load_view('customer/customer_detail', $data);
    }
    public function tambah()
    {
        $data = $this->customer->tambah();
        redirect_back($data, 'Ditambah');
    }

    public function edit()
    {
        $data = $this->customer->edit();
        redirect_back($data, 'Diubah');
    }

    public function delete()
    {
        $data = $this->customer->delete();
        redirect_back($data, 'Dihapus');
    }

    public function uploadKTP()
    {
        $data = $this->customer->uploadKTP();
        redirect_back($data, 'Diubah');
    }
}

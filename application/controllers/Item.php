<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Item extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        security_access();
        clearSession();
        // Your own constructor code
        $this->load->model('Item_model', 'item');
    }

    public function index()
    {

        $data['title'] = "Data Item";
        $data['item'] = $this->item->getAllItem();
        $data['kategori'] = $this->item->getAllKategori();
        $data['supplier'] = $this->item->getAllSupplier();

        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar');
        $this->load->view('item/index', $data);
        $this->load->view('template/footer');
    }


    public function tambah()
    {
        $data = $this->item->tambah();

        if ($data > 0) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <span><i class="icon fas fa-check"></i>Data Berhasil Ditambah</span>
                </div>');
            redirect('item');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <span><i class="icon fas fa-ban"></i> Data Gagal Ditambah!</span>
                </div>');
            redirect('item');
        }
    }

    public function edit()
    {
        $data = $this->item->edit();

        // die();
        if ($data > 0) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <span><i class="icon fas fa-check"></i>Data Berhasil Diubah</span>
                </div>');
            redirect('item');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <span><i class="icon fas fa-ban"></i> Data Gagal Diubah!</span>
                </div>');
            redirect('item');
        }
    }
    public function delete()
    {
        $data = $this->karyawan->delete();
        if ($data > 0) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <span><i class="icon fas fa-check"></i>Data Berhasil Dihapus</span>
                </div>');
            redirect('karyawan');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <span><i class="icon fas fa-ban"></i> Data Gagal Dihapus!</span>
                </div>');
            redirect('karyawan');
        }
    }
}

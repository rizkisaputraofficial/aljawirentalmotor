<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        security_access();
        clearSession();

        $this->load->model('Transaksi_model', 'transaksi');
        $this->load->model('Motor_model', 'motor');
        $this->load->model('Customer_model', 'customer');
    }

    public function test()
    {
        // $data = $this->transaksi->jamTerlambat();


        $awal  = date_create('2021-08-01 09:00:00');
        $akhir = date_create('2021-08-01 10:00:00'); // waktu sekarang
        $diff  = date_diff($awal, $akhir);
        $hari = $diff->d;
        $jam = $diff->h;
        $menit = $diff->i;
        $totalJam = ($hari * 24) + $jam;
        var_dump($totalJam);
    }

    public function index()
    {

        // $data['start'] = start_pagination();
        // $data['title'] = "Data Transaksi";
        // $data['transaksi'] = $this->transaksi->getAllData(['start']);
        // load_view('transaksi/index', $data);

        $url =  url_pagination();
        $page = show();
        $data['start'] = start_pagination();
        $data['row'] =  $this->transaksi->countAllData();
        pagination($url, $page, $data['row']);
        $data['judul'] = "List Team";
        $data['transaksi'] = $this->transaksi->getAllData($page, $data['start']);
        load_view('transaksi/index', $data);
    }




    public function detail($id)
    {

        $data['title'] = "Detail Transaksi";
        $data['customer'] = $this->transaksi->getDataById($id);
        $data['transaksi'] = $this->transaksi->getDataTransaksiById($id);
        $data['motor'] = $this->motor->getDataMotor();
        load_view('transaksi/transaksi_detail', $data);
    }

    public function getDataMotorById()
    {
        $data['motor'] = $this->transaksi->getDataMotorById();
        $data['transaksi'] = $this->transaksi->getDataTransaksiByIdTransaksi();
        echo json_encode($data);
    }

    public function getDataTarifByIdMotor()
    {

        $data['tarif'] = $this->transaksi->getDataTarifByIdMotor();
        $data['transaksi'] = $this->transaksi->getDataTransaksiByIdTransaksi();
        echo json_encode($data);
    }
    public function getDataTarifByIdMotorOther()
    {

        $data = $this->transaksi->getDataTarifByIdMotor();
        // $data['transaksi'] = $this->transaksi->getDataTransaksiByIdTransaksi();
        echo json_encode($data);
    }
    public function getDataTarifByIdTarif()
    {

        $data = $this->transaksi->getDataTarifByIdTarif();

        echo json_encode($data);
    }

    public function tambah()
    {
        $data = $this->transaksi->tambah();
        redirect_back($data, 'Ditambah');
    }

    public function edit()
    {
        $data = $this->transaksi->edit();
        redirect_back($data, 'Diubah');
    }
    public function editTransaksi()
    {
        // $tanggalPengembalian =  $this->input->post('tanggal_pengembalian');
        // var_dump($tanggalPengembalian);
        $data = $this->transaksi->editTransaksi();
        redirect_back($data, 'Diubah');
    }

    public function delete()
    {
        $data = $this->transaksi->delete();
        redirect_back($data, 'Dihapus');
    }


    public function uploadKTP()
    {

        $data = $this->customer->uploadKTP();
        redirect_back($data, 'Diubah');
    }
}

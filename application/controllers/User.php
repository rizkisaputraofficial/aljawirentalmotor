<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        security_access();
        clearSession();
        // Your own constructor code
        $this->load->model('User_model', 'user');
    }

    public function index()
    {
        $data['title'] = "Data user";
        $data['data'] = $this->user->getAllData();
        load_view('user/index', $data);
    }

    public function tambah()
    {
        $data = $this->user->tambah();
        redirect_back($data, 'Ditambah');
    }

    public function edit()
    {
        $data = $this->user->edit();
        redirect_back($data, 'Diubah');
    }

    public function delete()
    {
        $data = $this->user->delete();
        redirect_back($data, 'Dihapus');
    }
}

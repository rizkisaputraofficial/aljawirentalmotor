<div class="row" style="padding-top:20px">
    <div class="col-md-12">
        <?= $this->session->flashdata("message") ?>
        <div class="card">
            <div class="card-header">

                <div class="row">
                    <div class="col-md-6">
                        <h3 class="card-title">Data Denda</h3>
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#tambah">+ Tambah</button>
                    </div>
                </div>

            </div>
            <!-- /.card-header -->
            <div class="card-body">



                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="no">No</th>
                                    <th>Denda</th>


                                    <th class="action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($data as $value) : ?>
                                <tr>
                                    <td class="no"><?= $no++ ?></td>
                                    <td><?= money($value['denda']) ?></td>
                                    <td class="action">
                                        <button class="btn btn-success btn-sm" data-toggle="modal"
                                            data-target="#edit<?= $value['id_denda'] ?>"><i
                                                class="fa fa-edit"></i></button>


                                    </td>
                                </tr>
                                <?php endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.card-body -->

        </div>
    </div>
</div>



<!-- begin edit data -->
<?php
foreach ($data as  $value) : ?>
<div class="modal fade" id="edit<?= $value['id_denda'] ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" action="<?= base_url(url(1) . '/edit') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Denda</label>
                        <input type="text" class="form-control" name="denda" value="<?= $value['denda'] ?>">
                    </div>


                </div>
                <div class="modal-footer justify-content-between">
                    <input type="hidden" class="form-control" name="id" value="<?= $value['id_denda'] ?>">
                    <input type="hidden" name="url" value="<?= url() ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;
?>
<!-- end edit data -->
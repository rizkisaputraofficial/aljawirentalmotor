 <?= $this->session->flashdata("message") ?>

 <div class="row" style="padding-top:20px">

     <div class="col-md-3">
         <!-- Profile Image -->
         <div class="card card-primary card-outline">
             <div class="card-body box-profile">

                 <h3 class="profile-username text-center"><?= $customer['nama_customer'] ?></h3>



                 <ul class="list-group list-group-unbordered mb-3">
                     <li class="list-group-item">
                         <b>Tempat Lahir</b> <a class="float-right"><?= $customer['tempat_lahir'] ?></a>
                     </li>
                     <li class="list-group-item">
                         <b>Tanggal Lahir</b> <a class="float-right"><?= $customer['tanggal_lahir'] ?></a>
                     </li>
                     <li class="list-group-item">
                         <b>Gender</b> <a class="float-right">
                             <?php
                                $gender = $customer['gender'];
                                if ($gender == 'L') {
                                    echo "Laki - Laki";
                                } else if ($gender == 'P') {
                                    echo "Perempuan";
                                } else {
                                    echo "-";
                                }
                                ?>
                         </a>
                     </li>
                 </ul>


             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->

         <!-- About Me Box -->
         <div class="card card-primary">
             <div class="card-header">
                 <h3 class="card-title">Detail</h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <strong><i class="fas fa-book mr-1"></i>Telpon</strong>

                 <p class="text-muted"><?= $customer['telpon'] ?></p>

                 <hr>

                 <strong><i class="fas fa-map-marker-alt mr-1"></i>Alamat</strong>

                 <p class="text-muted"><?= $customer['alamat'] ?></p>

                 <hr>

                 <strong><i class="fas fa-pencil-alt mr-1"></i>KTP</strong>

                 <p class="text-muted">
                     <img src="<?= base_url() ?>assets/img/<?= $customer['foto_ktp'] ?>" alt="" width="220px"
                         height="160px">
                 </p>


             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
     <!-- /.col -->
     <div class="col-md-9">
         <div class="card">
             <div class="card-header p-2">
                 <ul class="nav nav-pills">
                     <li class="nav-item"><a class="nav-link active" href="#rental" data-toggle="tab">Rental</a>
                     </li>
                     <li class="nav-item"><a class="nav-link" href="#profil" data-toggle="tab">Profil</a>
                     </li>
                     <li class="nav-item"><a class="nav-link" href="#ktp" data-toggle="tab">KTP</a>
                     </li>
                 </ul>
             </div><!-- /.card-header -->
             <div class="card-body">

                 <div class="tab-content">
                     <div class="active tab-pane" id="rental">
                         <form class="form-horizontal" role="form"
                             action="<?= base_url(url(1) .  '/editTransaksi') ?>" method="POST"
                             enctype="multipart/form-data">
                             <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">motor</label>
                                 <div class="col-sm-10">
                                     <select name="id_motor" id="id_motor" class="form-control motor" required>
                                         <option value="">Pilih</option>
                                         <?php

                                            foreach ($motor as $motor) :
                                                if ($transaksi['id_motor_fk'] == $motor['id_motor']) { ?>

                                         <option value="<?= $motor['id_motor'] ?>" selected>
                                             <?= $motor['merek'] ?> - <?= $motor['warna'] ?></option>
                                         <?php } else { ?>

                                         <option value="<?= $motor['id_motor'] ?>">
                                             <?= $motor['merek'] ?> - <?= $motor['warna'] ?></option>
                                         <?php  }
                                                ?>

                                         <?php endforeach;   ?>
                                     </select>
                                 </div>
                             </div>

                             <div class="form-group row ">
                                 <label for="inputName" class="col-sm-2 col-form-label">Tarif</label>
                                 <div class="col-sm-10">
                                     <div class="show-tarif">
                                         <select name="id_tarif" id="tarif" class="form-control" required>
                                             <option value="">Pilih</option>
                                             <?php
                                                $this->db->join('jenis_tarif', 'id_jenis_tarif = id_jenis_tarif_fk');
                                                $jenisTarif = $this->db->get_where('tarif', ['id_motor_fk' => $transaksi['id_motor_fk']])->result_array();
                                                foreach ($jenisTarif as $jenisTarif) :
                                                    if ($transaksi['id_tarif_fk'] == $jenisTarif['id_tarif']) { ?>
                                             <option value="<?= $jenisTarif['id_tarif'] ?>" selected>
                                                 <?= $jenisTarif['jenis_tarif'] ?> - (<?= $jenisTarif['harga_tarif'] ?>)
                                             </option>
                                             <?php } else { ?>
                                             <option value="<?= $jenisTarif['id_tarif'] ?>">
                                                 <?= $jenisTarif['jenis_tarif'] ?> - (<?= $jenisTarif['harga_tarif'] ?>)
                                             </option>
                                             <?php  }
                                                    ?>
                                             <?php endforeach;   ?>
                                         </select>
                                     </div>

                                 </div>
                             </div>

                             <?php
                                $dateTime = explode(' ', $transaksi['tanggal_rental']);
                                $tanggalRental = $dateTime[0];
                                $jamRental = $dateTime[1];
                                ?>
                             <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">Tanggal Rental</label>
                                 <div class="col-sm-7">
                                     <input type="date" class="form-control" name="tanggal_rental"
                                         value="<?= $tanggalRental ?>">
                                 </div>
                                 <div class="col-sm-3">
                                     <input type="time" class="form-control" name="jam_rental"
                                         value="<?= $jamRental ?>">
                                 </div>
                             </div>
                             <?php
                                $dateTime = explode(' ', $transaksi['tanggal_kembali']);
                                $tanggalKembali = $dateTime[0];
                                $jamKembali = $dateTime[1];
                                ?>
                             <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">Tanggal Kembali</label>
                                 <div class="col-sm-7">
                                     <input type="date" class="form-control" name="tanggal_kembali"
                                         value="<?= $tanggalKembali ?>">
                                 </div>
                                 <div class="col-sm-3">
                                     <input type="time" class="form-control" name="jam_kembali"
                                         value="<?= $jamKembali ?>">
                                 </div>
                             </div>
                             <?php
                                $dateTime = explode(' ', $transaksi['tanggal_pengembalian']);
                                $tanggalPengembalian = $dateTime[0];
                                $jamPengembalian = $dateTime[1];
                                ?>
                             <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">Tanggal Pengembalian</label>
                                 <div class="col-sm-7">
                                     <input type="date" class="form-control" name="tanggal_pengembalian"
                                         value="<?= $tanggalPengembalian ?>">
                                 </div>
                                 <div class="col-sm-3">
                                     <input type="time" class="form-control" name="jam_pengembalian"
                                         value="<?= $jamPengembalian ?>">
                                 </div>
                             </div>


                             <div class="form-group row">
                                 <label for="inputExperience" class="col-sm-2 col-form-label">Harga</label>
                                 <div class="col-sm-10">
                                     <div class="show-harga">
                                         <input type="Number" class="form-control" name="total_harga" id="harga"
                                             value="<?= $transaksi['total_harga'] ?>">
                                     </div>
                                 </div>
                             </div>
                             <div class="form-group row">
                                 <label for="inputExperience" class="col-sm-2 col-form-label">Denda</label>
                                 <div class="col-sm-10">
                                     <input type="Number" class="form-control" name="total_denda"
                                         value="<?= $transaksi['total_denda'] ?>">
                                     <span class="description" style="font-size:12px ; color:red">Total jam terlambat
                                         (<?= $transaksi['jam_terlambat'] ?> jam x Rp 10.000) =
                                         <?= money($transaksi['jam_terlambat'] * 10000) ?></span>
                                 </div>
                             </div>
                             <div class="form-group row">
                                 <label for="inputExperience" class="col-sm-2 col-form-label">Total Bayar</label>
                                 <div class="col-sm-10">
                                     <input type="Number" class="form-control" name="total_bayar"
                                         value="<?= $transaksi['total_harga'] + $transaksi['total_denda'] ?>">
                                 </div>
                             </div>
                             <div class="form-group row">
                                 <label for="inputExperience" class="col-sm-2 col-form-label">Status</label>
                                 <div class="col-sm-10">

                                     <select name="status_transaksi" id="" class="form-control">

                                         <?php
                                            $status = $transaksi['status_transaksi'];
                                            if ($status == 1) { ?>
                                         <option value="">Pilih</option>
                                         <option value="1" selected>Running</option>
                                         <option value="2">Done</option>
                                         <?php } else if ($status == 2) { ?>
                                         <option value="">Pilih</option>
                                         <option value="1">Running</option>
                                         <option value="2" selected>Done</option>

                                         <?php } else { ?>
                                         <option value="">Pilih</option>
                                         <option value="1">Running</option>
                                         <option value="2">Done</option>
                                         <?php }

                                            ?>
                                     </select>
                                 </div>
                             </div>



                             <div class="form-group row">
                                 <div class="offset-sm-2 col-sm-10 pull-right" style="text-align:right;">
                                     <input type="hidden" name="url" value="<?= url('all') ?>">
                                     <input type="hidden" name="id" id="id_transaksi"
                                         value="<?= $transaksi['id_transaksi'] ?>">
                                     <button type="submit" class="btn btn-success">Simpan</button>
                                 </div>
                             </div>
                         </form>
                     </div>
                     <div class="tab-pane" id="profil">
                         <form class="form-horizontal" role="form"
                             action="<?= base_url(url(1) .  '/edit') ?>" method="POST"
                             enctype="multipart/form-data">
                             <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">Nama</label>
                                 <div class="col-sm-10">
                                     <input type="text" class="form-control" name="nama_customer"
                                         value="<?= $customer['nama_customer'] ?>">
                                 </div>
                             </div>
                             <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                 <div class="col-sm-10">
                                     <input type="text" class="form-control" name="tempat_lahir"
                                         value="<?= $customer['tempat_lahir'] ?>">
                                 </div>
                             </div>
                             <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                 <div class="col-sm-10">
                                     <input type="date" class="form-control" name="tanggal_lahir"
                                         value="<?= $customer['tanggal_lahir'] ?>">
                                 </div>
                             </div>
                             <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">Gender</label>
                                 <div class="col-sm-10">
                                     <select type="text" class="form-control" name="gender" value="">
                                         <?php
                                            $gender = $customer['gender'];
                                            if ($gender == 'L') { ?>
                                         <option value="">Pilih</option>
                                         <option value="L" selected>Laki - Laki</option>
                                         <option value="P">Perempuan</option>
                                         <?php  } else if ($gender == 'P') { ?>
                                         <option value="">Pilih</option>
                                         <option value="L">Laki - Laki</option>
                                         <option value="P" selected>Perempuan</option>
                                         <?php  } else { ?>
                                         <option value="">Pilih</option>
                                         <option value="L">Laki - Laki</option>
                                         <option value="P">Perempuan</option>
                                         <?php   } ?>
                                     </select>
                                     <!-- <input type="text" class="form-control"name="gender" value="<?= $customer['gender'] ?>"> -->
                                 </div>
                             </div>
                             <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">Telpon</label>
                                 <div class="col-sm-10">
                                     <input type="text" class="form-control" name="telpon"
                                         value="<?= $customer['telpon'] ?>">
                                 </div>
                             </div>


                             <div class="form-group row">
                                 <label for="inputExperience" class="col-sm-2 col-form-label">Alamat</label>
                                 <div class="col-sm-10">
                                     <textarea class="form-control" id="inputExperience"
                                         name="alamat"><?= $customer['alamat'] ?></textarea>
                                 </div>
                             </div>
                             

                             <div class="form-group row">
                                 <div class="offset-sm-2 col-sm-10 pull-right" style="text-align:right;">
                                     <input type="hidden" name="url" value="<?= url('all') ?>">
                                     <input type="hidden" name="id" value="<?= $customer['id_customer'] ?>">
                                     <button type="submit" class="btn btn-success">Simpan</button>
                                 </div>
                             </div>
                         </form>
                     </div>


                     <!-- /.tab-pane -->
                     <div class="tab-pane" id="ktp">
                         <form role="form" action="<?= base_url(url(1) .  '/uploadKTP') ?>" method="POST"
                             enctype="multipart/form-data">


                             <div class="form-group">
                                 <!-- <label for="customFile">Custom File</label> -->
                                 <div class="row">
                                     <div class="col-md-12">
                                         <div class="form-group">
                                             <!-- <label for="customFile">Custom File</label> -->
                                             <div class="row">
                                                 <div class="col-md-10">
                                                     <img src="<?= base_url() ?>assets/img/<?= $customer['foto_ktp'] ?>"
                                                         width=" 720px" height="520px">
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                             <div class="form-group">
                                 <!-- <label for="customFile">Custom File</label> -->
                                 <div class="row">
                                     <div class="col-md-10">
                                         <div class="custom-file">
                                             <input type="file" class="custom-file-input" id="customFile" name="image">
                                             <label class="custom-file-label" for="customFile">Pilih file</label>
                                         </div>
                                     </div>
                                     <div class="col-md-2">
                                         <input type="hidden" name="url" value="<?= url('all') ?>">
                                         <input type="hidden" name="id" value="<?= $customer['id_customer'] ?>">
                                         <button class="btn btn-primary">Upload</button>
                                     </div>
                                 </div>

                             </div>
                         </form>
                     </div>
                     <!-- /.tab-pane -->


                     <!-- /.tab-pane -->
                 </div>
                 <!-- /.tab-content -->
             </div><!-- /.card-body -->
         </div>
         <!-- /.nav-tabs-custom -->
     </div>
     <!-- /.col -->
 </div>
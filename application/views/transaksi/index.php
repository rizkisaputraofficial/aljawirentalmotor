<div class="row" style="padding-top:20px">
    <div class="col-md-12">
        <?= $this->session->flashdata("message") ?>
        <div class="card">
            <div class="card-header">

                <div class="row">
                    <div class="col-md-6">
                        <h3 class="card-title">Data Transaksi</h3>
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#tambah">+ Tambah</button>
                    </div>
                </div>

            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="<?= base_url(url()) ?>" method="post">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group m-form__group">
                                <div class="input-group input-group-sm">
                                    <select type="text" class="form-control m-input m-input--air short" id="shortBy" name="shortBy" style="padding-right:5px">
                                        <option value="">Sort & Search By</option>

                                        <?php
                                        $filter = array(
                                            'menu' => 'Menu',
                                            'title' => 'Submenu',
                                            'link' => 'Link',
                                        );
                                        $this->session->set_userdata(['autoFilter' => array_keys($filter)[0]]);
                                        foreach ($filter as $key => $fill) :
                                            if ($this->session->userdata('sortBy') == $key) { ?>
                                                <option value="<?= $key  ?>" selected><?= $fill  ?></option>
                                            <?php    } else {
                                            ?>
                                                <option value="<?= $key  ?>"><?= $fill  ?></option>
                                        <?php }
                                        endforeach;
                                        ?>

                                        <?php
                                        foreach ($filter as $key => $fill) : ?>
                                            <input type="hidden" name="listFilter[]" value="<?= $key ?>">
                                        <?php endforeach;

                                        ?>


                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group m-form__group">
                                <div class="input-group input-group-sm showStatus">
                                    <select type="text" class="form-control m-input m-input--air " id="urutan" name="urutan" style="padding-right:5px">
                                        <option value="">Urutan</option>
                                        <?php
                                        $sorting = array(
                                            'asc' => 'Menaik',
                                            'desc' => 'Menurun',
                                        );
                                        foreach ($sorting as $key => $sort) {
                                            if ($this->session->userdata('urutan') == $key) { ?>
                                                <option value="<?= $key  ?>" selected><?= $sort  ?></option>
                                            <?php    } else {
                                            ?>
                                                <option value="<?= $key  ?>"><?= $sort  ?></option>
                                        <?php }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-2">
                            <div class="form-group m-form__group">
                                <div class="input-group input-group-sm">

                                    <?php
                                    if ($this->session->userdata('keyword') != NULL) { ?>
                                        <input type="text" class="form-control m-input m-input--air" name="keyword" style="padding-right:5px" placeholder="search.." value="<?= $this->session->userdata('keyword') ?>">
                                    <?php   } else { ?>
                                        <input type="text" class="form-control m-input m-input--air" name="keyword" style="padding-right:5px" placeholder="search.." value="">
                                    <?php  }

                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-" style="text-align:right">
                            <div class="form-group m-form__group">
                                <button class="btn btn-info btn-sm m-btn m-btn--custom m-btn--icon m-btn--air"> <i type="submit" class="la la-search"></i></button>
                            </div>
                        </div>
                        <div class="col-md-1 pull-left">
                            <div class="form-group m-form__group">
                                <div class="input-group input-group-sm">
                                    <button class="btn btn-secondary btn-sm" type="submit" name="submit" value="clear" style="width:100%; color:gray" title="clear filter">clear</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="no" rowspan="2">No</th>
                                    <th rowspan="2">Kode Transaksi</th>
                                    <th rowspan="2">Customer</th>
                                    <th rowspan="2">Motor</th>
                                    <th rowspan="2" style="width:90px">Harga</th>
                                    <th rowspan="2" style="width:90px">Denda</th>
                                    <th rowspan="2" style="width:90px">Total</th>
                                    <th colspan="3">Tanggal</th>
                                    <th rowspan="2" style="width:100px">Status</th>
                                    <th class="action" rowspan="2">Action</th>
                                </tr>
                                <tr>
                                    <th style="width:100px">Rental</th>
                                    <th style="width:100px">Kembali</th>
                                    <th style="width:100px">Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($transaksi as  $value) : ?>
                                    <tr>
                                        <td class="no"><?= ++$start ?> </td>
                                        <td><?= $value['kode_transaksi'] ?> </td>
                                        <td style="text-align: left;"><?= $value['nama_customer'] ?> </td>
                                        <td><?= $value['merek'] ?> </td>
                                        <td style="text-align: right;"><?= money($value['total_harga']) ?> </td>
                                        <td style="text-align: right;"><?= money($value['total_denda']) ?> </td>
                                        <td style="text-align: right;">
                                            <?= money($value['total_denda'] + $value['total_harga']) ?> </td>
                                        <td><?= $value['tanggal_rental'] ?> </td>
                                        <td><?= $value['tanggal_kembali'] ?> </td>
                                        <td><?= $value['tanggal_pengembalian'] ?> </td>
                                        <td> <?php
                                                $status = $value['status_transaksi'];
                                                if ($status == 1) { ?>
                                                <small class="badge badge-success" style="width:80px">Running</small>
                                            <?php    } else if ($status == 2) { ?>
                                                <small class="badge badge-danger" style="width:80px">Done</small>
                                            <?php   } else { ?>
                                                <small class="badge badge-default" style="width:80px">-</small>
                                            <?php     } ?>
                                        </td>
                                        <td>
                                            <a href="<?= base_url('transaksi/detail/') ?><?= $value['id_transaksi'] ?>" class="btn btn-primary btn-sm"><b>D</b></a>
                                            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete<?= $value['id_transaksi'] ?>"><i class="fa fa-times"></i></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>


                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 ">
                        <p style="padding-top:5px">Tampil <?= begin() ?> ke <?= $start ?> dari <?= $row ?> data
                        </p>
                    </div>
                    <div class="col-sm-6 ">
                        <div class="pull-right">
                            <?= $this->pagination->create_links(); ?>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.card-body -->

        </div>
    </div>
</div>

<!-- begin tambah data -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form role="form" action="<?= base_url(url(1)  . '/tambah') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nama</label>
                                        <input type="text" class="form-control" name="nama_customer" value="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Telpon</label>
                                        <input type="text" class="form-control" name="telpon" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Tempat Lahir</label>
                                        <input type="text" class="form-control" name="tempat_lahir" value="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Tanggal Lahir</label>
                                        <input type="date" class="form-control" name="tanggal_lahir" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Gender</label>
                                        <select type="text" class="form-control" name="gender" value="">
                                            <option value="">Pilih</option>
                                            <option value="L">Laki - Laki</option>
                                            <option value="P">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Alamat</label>
                                        <textarea rows="1" type="text" class="form-control" name="alamat" value=""></textarea>
                                    </div>

                                </div>
                            </div>

                            <!-- <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Foto KTP</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="customFile" name="image">
                                            <label class="custom-file-label" for="customFile">Pilih file</label>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                        </div>
                        <div class="col-md-6">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Motor</label>

                                        <div class="form-group">
                                            <select name="id_motor" id="id_motor" class="form-control data-motor" required>
                                                <option value="">Pilih</option>
                                                <?php

                                                $motor = $this->db->get('motor')->result_array();

                                                foreach ($motor as $motor) : ?>

                                                    <option value="<?= $motor['id_motor'] ?>"> <?= $motor['merek'] ?> -
                                                        <?= $motor['warna'] ?></option>

                                                <?php endforeach;   ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Tarif</label>
                                        <div class="show-tarif">
                                            <div class="form-group">
                                                <select name="id_tarif" id="tarif" class="form-control">
                                                    <option value="">Pilih</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Tanggal Rental</label>
                                        <input type="date" class="form-control" name="tanggal_rental" value="">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Jam</label>
                                        <input type="time" class="form-control" name="jam_rental" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Tanggal Kembali</label>
                                        <input type="date" class="form-control" name="tanggal_kembali" value="">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Jam</label>
                                        <input type="time" class="form-control" name="jam_kembali" value="">
                                    </div>
                                </div>
                            </div>


                            <!-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">DP</label>
                                        <input type="number" class="form-control" name="username" value="0">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Sisa Pelunasan</label>
                                        <input type="number" class="form-control" name="username" value="0">
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <input type="hidden" name="url" value="<?= url() ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end tambah data -->

<!-- begin edit data -->
<?php
foreach ($transaksi as  $value) : ?>
    <div class="modal fade" id="edit<?= $value['id_motor'] ?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <form role="form" action="<?= base_url(url(1) . '/edit') ?>" method="POST" enctype="multipart/form-data">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Data</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Merek</label>
                                    <input type="text" class="form-control" name="nama_admin" value="<?= $value['merek'] ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Warna</label>
                                    <input type="text" class="form-control" name="warna" value="<?= $value['warna'] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Nomor Plat</label>
                                    <input type="text" class="form-control" name="no_plat" value="<?= $value['no_plat'] ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Tahun</label>
                                    <input type="text" class="form-control" name="tahun" value="<?= $value['tahun'] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Denda</label>
                                    <input type="text" class="form-control" name="denda" value="<?= $value['denda'] ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Harga</label>
                                    <input type="text" class="form-control" name="harga" value="<?= $value['harga'] ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <input type="hidden" class="form-control" name="id" value="<?= $value['id_motor'] ?>">
                        <input type="hidden" name="url" value="<?= url() ?>">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach;
?>
<!-- end edit data -->


<!--begin proses delete -->
<?php
foreach ($transaksi as  $value) {
    echo  modal_delete($value['id_transaksi'], 'delete');
}
?>
<!--end proses delete -->
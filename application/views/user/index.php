<div class="row" style="padding-top:20px">
    <div class="col-md-12">
        <?= $this->session->flashdata("message") ?>
        <div class="card">
            <div class="card-header">

                <div class="row">
                    <div class="col-md-6">
                        <h3 class="card-title">Data User</h3>
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#tambah">+ Tambah</button>
                    </div>
                </div>

            </div>
            <!-- /.card-header -->
            <div class="card-body">



                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="no">No</th>
                                    <th>Nama</th>
                                    <th>Username</th>
                                    <th>Telpon</th>
                                    <th class="action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($data as $value) : ?>
                                <tr>
                                    <td class="no"><?= $no++ ?></td>
                                    <td><?= $value['nama'] ?></td>
                                    <td><?= $value['telpon'] ?></td>
                                    <td><?= $value['username'] ?></td>

                                    <!--                                         
                                         <td style='text-align:center; '>
                                            <?php
                                            $level = $value['level'];
                                            if ($level == 1) {
                                                 echo "Admin";
                                            } else if ($level == 2) {
                                                echo "Petugas";
                                            } else {
                                                echo "-";
                                            }
                                            ?>
                                            </td> -->



                                    <td class="action">
                                        <button class="btn btn-success btn-sm" data-toggle="modal"
                                            data-target="#edit<?= $value['id_user'] ?>"><i
                                                class="fa fa-edit"></i></button>

                                        <button class="btn btn-danger btn-sm" data-toggle="modal"
                                            data-target="#delete<?= $value['id_user'] ?>"><i
                                                class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <?php endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.card-body -->

        </div>
    </div>
</div>


<!-- begin tambah data -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" action="<?= base_url(url(1).'/tambah') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama</label>
                        <input type="text" class="form-control" name="nama">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Telpon</label>
                        <input type="text" class="form-control" name="telpon">
                    </div>
                    <!-- <div class="form-group">
                        <label for="" style="font-size: 1rem">Level</label>

                        <select class="form-control" type="text" name="level">
                            <option value="">Pilih</option>
                            <option value="1">Admin</option>
                            <option value="2">Petugas</option>
                        </select>

                    </div> -->
                    <div class="form-group">
                        <label for="exampleInputPassword1">Username</label>
                        <input type="text" class="form-control" name="username">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="text" class="form-control" name="password">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <input type="hidden" name="url" value="<?= url() ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end tambah data -->

<!-- begin edit data -->
<?php
foreach ($data as  $value) : ?>
<div class="modal fade" id="edit<?= $value['id_user'] ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" action="<?= base_url(url(1) . '/edit') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama</label>
                        <input type="text" class="form-control" name="nama" value="<?= $value['nama'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Telpon</label>
                        <input type="text" class="form-control" name="telpon" value="<?= $value['telpon'] ?>">
                    </div>
                    <!-- <div class="form-group">

                        <label for="" style="font-size: 1rem">Hak Akses</label>

                        <select class="form-control" type="text" name="level" required>
                            <?php 
                                    $akses = $value['level'];
                                    if($akses == 1){ ?>
                            <option value="">Pilih</option>
                            <option value="1" selected>Admin</option>
                            <option value="2">Petugas</option>
                            <?php   }else if($akses == 2){ ?>
                            <option value="">Pilih</option>
                            <option value="1">Admin</option>
                            <option value="2" selected>Petugas</option>
                            <?php  }else{ ?>
                            <option value="">Pilih</option>
                            <option value="1">Admin</option>
                            <option value="2">Petugas</option>
                            <?php    }

                                 ?>

                        </select>


                    </div> -->

                    <div class="form-group">
                        <label for="exampleInputPassword1">Username</label>
                        <input type="text" class="form-control" name="username" value="<?= $value['username'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="text" class="form-control" name="password" value="<?= $value['password'] ?>">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <input type="hidden" class="form-control" name="id" value="<?= $value['id_user'] ?>">
                    <input type="hidden" name="url" value="<?= url() ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;
?>
<!-- end edit data -->


<!--begin proses delete -->
<?php
foreach ($data as  $value) {
    echo  modal_delete($value['id_user'], 'delete');
}
?>
<!--end proses delete -->
<div class="row justify-content-center mt-5">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-primary text-white text-center">
                Filter Laporan Setiap Bulan
            </div>
            <div class="card-body">
                <form action="<?= base_url('laporan') ?>" method="GET">
                    <div class="form-group">
                        <label for="Bulan">Bulan</label>
                        <select name="bln" class="form-control">
                            <option value="">-- Pilih Bulan --</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>

                    </div>
                    <div class="form-group">
                        <label for="Tahun">Tahun</label>
                        <select name="thn" class="form-control">
                            <option value="">-- Pilih Tahun --</option>
                            <?php $tahun = date('Y');
								for($i=2021;$i<$tahun+5;$i++) { ?>

                            <option value="<?= $i ?>"><?= $i ?></option>

                            <?php } ?>
                        </select>

                    </div>
                    <div class="text-center mt-3">
                        <button type="submit" class="btn btn-danger">Cetak Laporan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <a href="<?= base_url('laporan/print_laporan/?bln=') . $bln . '&thn=' . $thn ?>"
            class="btn btn-success mt-5 mb-3" target="_blank">Print Laporan</a>
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th class="no" rowspan="2">No</th>
                    <th rowspan="2" style="vertical-align: middle;">Kode Transkasi</th>
                    <th rowspan="2">Customer</th>
                    <th rowspan="2">Motor</th>
                    <th rowspan="2" style="width:90px">Harga</th>
                    <th rowspan="2" style="width:90px">Denda</th>
                    <th rowspan="2" style="width:90px">Total</th>
                    <th colspan="3">Tanggal</th>
                    <th rowspan="2" style="width:100px">Status</th>
                </tr>
                <tr>
                    <th style="width:100px">Rental</th>
                    <th style="width:100px">Kembali</th>
                    <th style="width:100px">Selesai</th>
                </tr>
            </thead>
            <tbody>
                <?php
					$no = 1;
					if(empty($laporan)){ ?>
                <td colspan="10">Tidak ada data.</td>
                <?php } else{
					foreach ($laporan as  $value) : ?>
                <tr>
                    <td class="no"><?= $no++ ?> </td>
                    <td style="text-align: left;"><?= $value['kode_transaksi'] ?> </td>
                    <td style="text-align: left;"><?= $value['nama_customer'] ?> </td>
                    <td><?= $value['merek'] ?> </td>
                    <td style="text-align: right;"><?= money($value['total_harga']) ?> </td>
                    <td style="text-align: right;"><?= money($value['total_denda']) ?> </td>
                    <td style="text-align: right;">
                        <?= money($value['total_denda'] + $value['total_harga']) ?> </td>
                    <td><?= $value['tanggal_rental'] ?> </td>
                    <td><?= $value['tanggal_kembali'] ?> </td>
                    <td><?= $value['tanggal_pengembalian'] ?> </td>
                    <td> <?php
								$status = $value['status_transaksi'];
								if ($status == 1) { ?>
                        <small class="badge badge-success" style="width:80px">Running</small>
                        <?php    } else if ($status == 2) { ?>
                        <small class="badge badge-danger" style="width:80px">Done</small>
                        <?php   } else { ?>
                        <small class="badge badge-default" style="width:80px">-</small>
                        <?php     } ?>
                    </td>
                </tr>
                <?php endforeach; ?>
                <?php } ?>

            </tbody>


        </table>
    </div>

</div>
<!-- </div> -->
<!-- </div> -->
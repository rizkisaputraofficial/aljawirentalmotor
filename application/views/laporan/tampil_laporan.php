<!-- <div class="card-header bg-primary text-white text-center mt-5">
	Filter Laporan Setiap Bulan
</div>

<div class="card-body">
 -->
<!-- <div class="form-group row">
		<label for="inp"></label>
	</div> -->

<div class="row justify-content-center mt-5">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-primary text-white text-center">
                Filter Laporan Setiap Bulan
            </div>
            <div class="card-body">
                <form action="<?= base_url('laporan') ?>" method="post">
                    <div class="form-group">
                        <label for="Bulan">Bulan</label>
                        <select name="bln" class="form-control">
                            <option value="1" <?= (!empty($bln) && $bln == 1) ? 'selected' : '' ?>>Januari</option>
                            <option value="2" <?= (!empty($bln) && $bln == 2) ? 'selected' : '' ?>>Februari</option>
                            <option value="3" <?= (!empty($bln) && $bln == 3) ? 'selected' : '' ?>>Maret</option>
                            <option value="4" <?= (!empty($bln) && $bln == 4) ? 'selected' : '' ?>>April</option>
                            <option value="5" <?= (!empty($bln) && $bln == 5) ? 'selected' : '' ?>>Mei</option>
                            <option value="6" <?= (!empty($bln) && $bln == 6) ? 'selected' : '' ?>>Juni</option>
                            <option value="7" <?= (!empty($bln) && $bln == 7) ? 'selected' : '' ?>>Juli</option>
                            <option value="8" <?= (!empty($bln) && $bln == 8) ? 'selected' : '' ?>>Agustus</option>
                            <option value="9" <?= (!empty($bln) && $bln == 9) ? 'selected' : '' ?>>September</option>
                            <option value="10" <?= (!empty($bln) && $bln == 10) ? 'selected' : '' ?>>Oktober</option>
                            <option value="11" <?= (!empty($bln) && $bln == 11) ? 'selected' : '' ?>>November</option>
                            <option value="12" <?= (!empty($bln) && $bln == 12) ? 'selected' : '' ?>>Desember</option>
                        </select>

                    </div>
                    <div class="form-group">
                        <label for="Tahun">Tahun</label>
                        <select name="thn" class="form-control">
                            <?php $tahun = date('Y');
								for($i=2021;$i<$tahun+5;$i++) { ?>

                            <option value="<?= $i ?>" <?= ($thn == $i) ? 'selected' : '' ?>><?= $i ?></option>

                            <?php } ?>
                        </select>

                    </div>
                    <div class="text-center mt-3">
                        <button type="submit" class="btn btn-danger">Cetak Laporan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
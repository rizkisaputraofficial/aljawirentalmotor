<!DOCTYPE html>
<meta charset="utf-8" />

<head>
    <title><?= $title ?></title>

    <link href="<?= base_url(); ?>/vendor/twbs/bootstrap/dist/css/bootstrap.css" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="<?= base_url(); ?>/assets/img/logo.png" />
    <style>
    #footer {
        position: fixed;
        /* left: 0px; */
        bottom: 0px;
        /* right: 0px; */
        height: 100px;
        background-color: lightblue;
    }

    .text-up {
        position: relative;
        top: -70px;
        /* Sesuaikan nilai ini sesuai dengan kebutuhan Anda */
    }
     .text-cek {
        position: relative;
        top: -80px;
        /* Sesuaikan nilai ini sesuai dengan kebutuhan Anda */
    }
    .text-cekout {
        position: relative;
        top: -80px;
        /* Sesuaikan nilai ini sesuai dengan kebutuhan Anda */
    }

    .text-upps {
        position: relative;
        top: -70px;
        /* Sesuaikan nilai ini sesuai dengan kebutuhan Anda */
    }

    table tbody td {
        padding: 0.2rem;
    }
    </style>

</head>

<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <?php
                $path = 'assets/img/download.png';
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);
                $base64 = 'data:/' . $type . ';base64,' . base64_encode($data);
                ?>

                <div class="col-md-3">
                    <img src="<?php echo $base64 ?>" width="120px" height="120px"
                        style="margin-bottom:-15px; margin-top:-15px; padding-top:-15px; padding-bottom:-15px">

                </div>

            </div>
            <div class="text-up" style="text-align: center;">

                <h1 style="margin-bottom:0px; margin-top:-20px; padding-top:0px; padding-bottom:0px"><strong> Laporan Transaksi Rental Motor Aljawi  
                       </strong></h1>
                         </div>
            <div class="text-cek" style="text-align: center;">
                <h3>Condong Catur, Depok Sleman . ( 0262 ) 082328334199 Yogyakarta 45376</h3>
                </div>
                
                 <div class="text-cekout" style="text-align: center;">
                <h3> Bulan <?= $bln ?> Tahun <?= $thn ?></h3>
                     </div>
          

            <div class="text-upps">
                <div class="col-md-12" style="padding-top:10px; margin-top:0px">
                    <div class="row" style="padding-top:0px; margin-top:0px">
                        <table border="1px" style="font-size:11px; border-collapse: collapse;width: 100%; ">
                            <thead class="thead-light">
                                <tr>
                                    <th class="no" rowspan="2" style="width:20px;vertical-align: middle;">No
                                    </th>
                                    <th rowspan="2" style="vertical-align: middle;">Kode Transkasi</th>
                                    <th rowspan="2" style="vertical-align: middle;">Customer</th>
                                    <th rowspan="2" style="vertical-align: middle;">Motor</th>
                                    <th rowspan="2" style="width:90px;vertical-align: middle;">Harga</th>
                                    <th rowspan="2" style="width:90px;vertical-align: middle;">Denda</th>
                                    <th rowspan="2" style="width:90px;vertical-align: middle;">Total</th>
                                    <th class="text-center" colspan="3" style="vertical-align: middle;">
                                        Tanggal</th>
                                    <th rowspan="2" style="width:100px;vertical-align: middle;">Status</th>
                                </tr>
                                <tr>
                                    <th style="width:100px;vertical-align: middle;">Rental</th>
                                    <th style="width:100px;vertical-align: middle;">Kembali</th>
                                    <th style="width:100px;vertical-align: middle;">Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            $no = 1;
                            foreach ($laporan as $value) : ?>

                                <tr>
                                    <td class="no"><?= $no++ ?> </td>
                                    <td style="text-align: left;"><?= $value['kode_transaksi'] ?> </td>
                                    <td style="text-align: left;"><?= $value['nama_customer'] ?> </td>
                                    <td><?= $value['merek'] ?> </td>
                                    <td style="text-align: right;"><?= money($value['total_harga']) ?> </td>
                                    <td style="text-align: right;"><?= money($value['total_denda']) ?> </td>
                                    <td style="text-align: right;">
                                        <?= money($value['total_denda'] + $value['total_harga']) ?> </td>
                                    <td><?= $value['tanggal_rental'] ?> </td>
                                    <td><?= $value['tanggal_kembali'] ?> </td>
                                    <td><?= $value['tanggal_pengembalian'] ?> </td>
                                    <td> <?php
            $status = $value['status_transaksi'];
            if ($status == 1) { ?>
                                        <small class="badge badge-success" style="width:80px">Running</small>
                                        <?php    } else if ($status == 2) { ?>
                                        <small class="badge badge-danger" style="width:80px">Done</small>
                                        <?php   } else { ?>
                                        <small class="badge badge-default" style="width:80px">-</small>
                                        <?php     } ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>





                    </div>
                </div>
            </div>
            <div class="row" style=" position: static;">
                <div class="col-md-12" style="text-align:right;font-size:14px">
                    <div>
                        <p style="padding-bottom:0px; margin-bottom:0px; ">Yogyakarta,....<?= $bln ?>
                            <?= $thn ?></p>
                        <p style="padding-top:0px; margin-top:0px; margin-right:30px">Owner </p>
                        <br>
                        <br>
                        <br>

                        <p style="padding-top:0px; margin-top:0px; margin-right:35px">Khabib </p>
                    </div>


                </div>
            </div>



</body>



</html>
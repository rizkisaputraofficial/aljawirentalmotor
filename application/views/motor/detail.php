  <?= $this->session->flashdata("message") ?>

  <div class="row" style="padding-top:20px">
      <div class="col-12">
          <div class="card card-default">
              <div class="card-header">
                  <h3 class="card-title">
                      Detail Motor
                  </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  <div class="row">
                      <div class="col-md-4">

                          <div class="callout callout-success">
                              <img src="<?= base_url('assets/img/') ?><?= $data['gambar'] ?>" alt="" width="300px"
                                  height="300px">
                          </div>

                      </div>
                      <div class="col-md-4" style="text-align: center;">

                          <div class="row" style="text-align: center;">
                              <div class="col-md-10"><label>Data Motor</label></div>
                              <div class="col-md-2">
                                  <button class="btn btn-info btn-sm" data-toggle="modal"
                                      data-target="#edit">Edit</button>
                              </div>

                          </div>
                          <div class="table-responsive" style="padding-top:5px">
                              <table class="table">
                                  <tbody>

                                      <tr>
                                          <th style="width:30%; text-align:left">Merek</th>
                                          <td>:</td>
                                          <td style="text-align: left;"><?= $data['merek'] ?></td>
                                      </tr>
                                      <tr>
                                          <th style="width:30%; text-align:left">Warna</th>
                                          <td>:</td>
                                          <td style="text-align: left;"><?= $data['warna'] ?></td>
                                      </tr>
                                      <tr>
                                          <th style="width:30%; text-align:left">Tahun</th>
                                          <td>:</td>
                                          <td style="text-align: left;"><?= $data['tahun'] ?></td>
                                      </tr>
                                      <tr>
                                          <th style="width:30%; text-align:left">No. Plat</th>
                                          <td>:</td>
                                          <td style="text-align: left;"><?= $data['no_plat'] ?></td>
                                      </tr>
                                      <tr>
                                          <th style="width:30%; text-align:left">Bahan Bakar</th>
                                          <td>:</td>
                                          <td style="text-align: left;"><?= $data['bahan_bakar'] ?></td>
                                      </tr>

                                      <tr>
                                          <th style="width:30%; text-align:left">Status</th>
                                          <td>:</td>
                                          <td style="text-align: left;">
                                              <?php
                                                $status = $data['status'];
                                                if ($status == 1) { ?>
                                              <small class="badge badge-success" style="width:80px">Available</small>
                                              <?php    } else if ($status == 2) { ?>
                                              <small class="badge badge-danger" style="width:80px">Unavailable</small>
                                              <?php   } else { ?>
                                              <small class="badge badge-default" style="width:80px">-</small>
                                              <?php     } ?>
                                          </td>
                                      </tr>


                                  </tbody>
                              </table>


                          </div>
                          <!-- <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="edit">Edit</button> -->
                      </div>
                      <div class="col-md-4">
                          <div class="row" style="text-align: center;">
                              <div class="col-md-10"><label>Tarif</label></div>
                              <div class="col-md-2">
                                  <button class="btn btn-success btn-sm" data-toggle="modal"
                                      data-target="#tambah">Add</button>
                              </div>

                          </div>
                          <div class="table-responsive" style="padding-top:5px">
                              <table class="table table-bordered">
                                  <thead>
                                      <th>No</th>
                                      <th>Jenis</th>
                                      <th style="width:80px">Jumlah</th>
                                      <th>Action</th>
                                  </thead>
                                  <tbody>
                                      <?php




                                        $no = 1;
                                        foreach ($tarif as  $value) : ?>
                                      <tr>
                                          <td class="no"><?= $no++ ?></td>
                                          <td><?= $value['jenis_tarif'] ?></td>
                                          <td style="text-align:right"><?= money($value['harga_tarif']) ?></td>
                                          <td class="action">
                                              <button class="btn btn-success btn-sm" data-toggle="modal"
                                                  data-target="#editTarif<?= $value['id_tarif'] ?>"><b>E</b></button>
                                              <button class="btn btn-danger btn-sm" data-toggle="modal"
                                                  data-target="#delete<?= $value['id_tarif'] ?>"><i
                                                      class="fa fa-times"></i></button>
                                          </td>
                                      </tr>
                                      <?php endforeach;

                                        ?>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- /.card-body -->
          </div>
      </div>
  </div>











  <div class="modal fade" id="edit">
      <div class="modal-dialog">
          <div class="modal-content">
              <form role="form" action="<?= base_url(url(1) . '/edit') ?>" method="POST" enctype="multipart/form-data">
                  <div class="modal-header">
                      <h4 class="modal-title">Edit Data</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">

                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label for="exampleInputEmail1">Merek</label>
                                  <input type="text" class="form-control" name="merek" value="<?= $data['merek'] ?>">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label for="exampleInputPassword1">Warna</label>
                                  <input type="text" class="form-control" name="warna" value="<?= $data['warna'] ?>">
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label for="exampleInputPassword1">Nomor Plat</label>
                                  <input type="text" class="form-control" name="no_plat"
                                      value="<?= $data['no_plat'] ?>">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label for="exampleInputPassword1">Tahun</label>
                                  <input type="text" class="form-control" name="tahun" value="<?= $data['tahun'] ?>">
                              </div>
                          </div>
                      </div>


                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label for="exampleInputPassword1">Bahan Bakar</label>
                                  <input type="text" class="form-control" name="bahan_bakar"
                                      value="<?= $data['bahan_bakar'] ?>">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label for="exampleInputPassword1">Gambar </label>
                                  <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="customFile" name="image">
                                      <label class="custom-file-label" for="customFile">Pilih file</label>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label for="exampleInputEmail1">Status</label>
                                  <select type="text" class="form-control" name="status">
                                      <option value="">Pilih</option>

                                      <?php
                                        $status = array(
                                            1 => 'Available',
                                            2 => 'Unavailable',
                                        );

                                        foreach ($status as $key =>  $status) :
                                        ?>
                                      <?php if ($key == $data['status']) { ?>
                                      <option value="<?= $key ?>" selected><?= $status ?></option>
                                      <?php  } else { ?>
                                      <option value="<?= $key ?>"><?= $status ?></option>
                                      <?php  }
                                        endforeach;
                                        ?>
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer justify-content-between">
                      <input type="hidden" class="form-control" name="id" value="<?= $data['id_motor'] ?>">
                      <input type="hidden" name="url" value="<?= url() ?>">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
              </form>
          </div>
      </div>
  </div>

  <?php
    foreach ($tarif as  $value) : ?>
  <div class="modal fade" id="editTarif<?= $value['id_tarif'] ?>">
      <div class="modal-dialog">
          <div class="modal-content">
              <form role="form" action="<?= base_url(url(1) .'/editTarif') ?>" method="POST"
                  enctype="multipart/form-data">
                  <div class="modal-header">
                      <h4 class="modal-title">Edit Tarif</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">

                      <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label for="exampleInputEmail1">Jenis</label>
                                  <input type="text" class="form-control" name="" value="<?= $value['jenis_tarif'] ?>"
                                      readonly>
                              </div>
                          </div>

                      </div>
                      <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label for="exampleInputPassword1">Harga</label>
                                  <input type="text" class="form-control" name="harga_tarif"
                                      value="<?= $value['harga_tarif'] ?>">
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer justify-content-between">
                      <input type="hidden" class="form-control" name="id" value="<?= $value['id_tarif'] ?>">
                      <input type="hidden" name="url" value="<?= url('all') ?>">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
  <?php endforeach;
    ?>
  <!--begin proses delete -->
  <?php
    foreach ($tarif as  $value) {
        echo  modal_delete($value['id_tarif'], 'deleteTarif', 'all');
    }
    ?>
  <!--end proses delete -->


  <div class="modal fade" id="tambah">
      <div class="modal-dialog">
          <div class="modal-content">
              <form role="form" action="<?= base_url(url(1) . '/tambahTarif') ?>" method="POST"
                  enctype="multipart/form-data">
                  <div class="modal-header">
                      <h4 class="modal-title">Tambah Tarif</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                      <table>

                          <?php
                            foreach ($jenisTarif as $jenisTarif) : ?>
                          <tr>
                              <td style="width: 250px; text-align:left"><label><?= $jenisTarif['jenis_tarif'] ?></label>
                              </td>
                              <td>
                                  <div class="form-group">
                                      <input type="text" class="form-control" name="harga_tarif[]" value="0">
                                  </div>
                              </td>
                          </tr>
                          <input type="hidden" name="id_jenis_tarif[]" value="<?= $jenisTarif['id_jenis_tarif'] ?>">
                          <?php endforeach;
                            ?>

                      </table>
                      <div class="modal-footer justify-content-between">
                          <input type="hidden" name="url" value="<?= url('all') ?>">
                          <input type="hidden" name="id_motor" value="<?= $data['id_motor'] ?>">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Tambah</button>
                      </div>
              </form>
          </div>
      </div>
  </div>
  <!-- end tambah data -->
  <!-- end tambah data -->
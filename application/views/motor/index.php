<div class="row" style="padding-top:20px">
    <div class="col-md-12">
        <?= $this->session->flashdata("message") ?>
        <div class="card">
            <div class="card-header">

                <div class="row">
                    <div class="col-md-6">
                        <h3 class="card-title">Data Motor</h3>
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#tambah">+ Tambah</button>
                    </div>
                </div>

            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <!-- <form action="<?= base_url('admin/index') ?>" method="post">
                    <div class="row">
                        <div class="col-md-8">
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control" name="keyword">
                            </div>
                        </div>
                        <div class="col-md-1" style="text-align:right">
                            <button type="submit" class="btn btn-success ">Search</button>
                        </div>
                    </div>
                </form> -->


                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="no">No</th>
                                   
                                    <th>Motor</th>
                                    <th>Warna</th>
                                    <th>Gambar</th>
                                    <th>Tahun</th>

                                    <th>Status</th>
                                    <th class="action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($data as $value) : ?>
                                <tr>
                                    <td class="no"><?= $no++ ?></td>
                                 
                                    <td><?= $value['merek'] ?></td>
                                    <td><?= $value['warna'] ?></td>
                                    <td><img src="<?= base_url('assets/img/') ?><?= $value['gambar'] ?>" alt=""
                                            style="width: 100px; height:100px;"></td>
                                    <td><?= $value['tahun'] ?></td>


                                    <td>
                                        <?php
                                            $status = $value['status'];
                                            if ($status == 1) { ?>
                                        <small class="badge badge-success" style="width:80px">Available</small>
                                        <?php    } else if ($status == 2) { ?>
                                        <small class="badge badge-danger" style="width:80px">Unavailable</small>
                                        <?php   } else { ?>
                                        <small class="badge badge-default" style="width:80px">-</small>
                                        <?php     } ?>
                                    </td>


                                    <td class="action">
                                        <a class="btn btn-success btn-sm"
                                            href="<?= base_url('motor/detail/') ?><?= $value['id_motor'] ?>"><b>D</b></a>
                                        <!-- <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#edit<?= $value['id_motor'] ?>"><b>E</b></button> -->
                                        <button class="btn btn-danger btn-sm" data-toggle="modal"
                                            data-target="#delete<?= $value['id_motor'] ?>"><i
                                                class="fa fa-times"></i></button>
                                    </td>
                                </tr>
                                <?php endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.card-body -->

        </div>
    </div>
</div>


<!-- begin tambah data -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" action="<?= base_url(url(1) . '/tambah') ?>" method="POST"
                enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Motor</label>
                                <input type="text" class="form-control" name="merek" value="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Warna</label>
                                <input type="text" class="form-control" name="warna" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Nomor Plat</label>
                                <input type="text" class="form-control" name="no_plat" value="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Tahun</label>
                                <input type="text" class="form-control" name="tahun" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Bahan Bakar</label>
                                <input type="text" class="form-control" name="bahan_bakar" value="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Gambar </label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="image">
                                    <label class="custom-file-label" for="customFile">Pilih file</label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer justify-content-between">
                    <input type="hidden" name="url" value="<?= url() ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end tambah data -->

<!-- begin edit data -->
<?php
foreach ($data as  $value) : ?>
<div class="modal fade" id="edit<?= $value['id_motor'] ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" action="<?= base_url(url(1). '/edit') ?>" method="POST"
                enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Motor</label>
                                <input type="text" class="form-control" name="merek" value="<?= $value['merek'] ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Warna</label>
                                <input type="text" class="form-control" name="warna" value="<?= $value['warna'] ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Nomor Plat</label>
                                <input type="text" class="form-control" name="no_plat" value="<?= $value['no_plat'] ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Tahun</label>
                                <input type="text" class="form-control" name="tahun" value="<?= $value['tahun'] ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Denda</label>
                                <input type="text" class="form-control" name="harga" value="<?= $value['denda'] ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Harga</label>
                                <input type="text" class="form-control" name="denda" value="<?= $value['harga'] ?>">
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Gambar</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="image">
                                    <label class="custom-file-label" for="customFile">Pilih file</label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer justify-content-between">
                    <input type="hidden" class="form-control" name="id" value="<?= $value['id_motor'] ?>">
                    <input type="hidden" name="url" value="<?= url('all') ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;
?>
<!-- end edit data -->


<!--begin proses delete -->
<?php
foreach ($data as  $value) {
    echo  modal_delete($value['id_motor'], 'delete');
}
?>
<!--end proses delete -->
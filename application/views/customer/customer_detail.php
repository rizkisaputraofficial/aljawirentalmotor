 <?= $this->session->flashdata("message") ?>

 <div class="row" style="padding-top:20px">

     <div class="col-md-3">
         <!-- Profile Image -->
         <div class="card card-primary card-outline">
             <div class="card-body box-profile">

                 <h3 class="profile-username text-center"><?= $customer['nama_customer'] ?></h3>



                 <ul class="list-group list-group-unbordered mb-3">
                     <li class="list-group-item">
                         <b>Tempat Lahir</b> <a class="float-right"><?= $customer['tempat_lahir'] ?></a>
                     </li>
                     <li class="list-group-item">
                         <b>Tanggal Lahir</b> <a class="float-right"><?= $customer['tanggal_lahir'] ?></a>
                     </li>
                     <li class="list-group-item">
                         <b>Gender</b> <a class="float-right">
                             <?php
                                $gender = $customer['gender'];
                                if ($gender == 'L') {
                                    echo "Laki - Laki";
                                } else if ($gender == 'P') {
                                    echo "Perempuan";
                                } else {
                                    echo "-";
                                }
                                ?>
                         </a>
                     </li>
                 </ul>


             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->

         <!-- About Me Box -->
         <div class="card card-primary">
             <div class="card-header">
                 <h3 class="card-title">Detail</h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
                 <strong><i class="fas fa-book mr-1"></i>Telpon</strong>

                 <p class="text-muted"><?= $customer['telpon'] ?></p>

                 <hr>

                 <strong><i class="fas fa-map-marker-alt mr-1"></i>Alamat</strong>

                 <p class="text-muted"><?= $customer['alamat'] ?></p>

                 <hr>

                 <strong><i class="fas fa-pencil-alt mr-1"></i>KTP</strong>

                 <p class="text-muted">
                     <img src="<?= base_url() ?>assets/img/<?= $customer['foto_ktp'] ?>" alt="" width="220px"
                         height="160px">
                 </p>


             </div>
             <!-- /.card-body -->
         </div>
         <!-- /.card -->
     </div>
     <!-- /.col -->
     <div class="col-md-9">
         <div class="card">
             <div class="card-header p-2">
                 <ul class="nav nav-pills">
                     <li class="nav-item"><a class="nav-link active" href="#rental" data-toggle="tab">Rental</a>
                     </li>
                     <li class="nav-item"><a class="nav-link" href="#profil" data-toggle="tab">Profil</a>
                     </li>
                     <li class="nav-item"><a class="nav-link" href="#ktp" data-toggle="tab">KTP</a>
                     </li>
                 </ul>
             </div><!-- /.card-header -->
             <div class="card-body">

                 <div class="tab-content">
                     <div class="active tab-pane" id="rental">
                         <table class="table table-bordered table-hover">
                             <thead>
                                 <tr>
                                     <th class="no" rowspan="2">No</th>
                                     <th rowspan="2">Mobil</th>
                                     <th rowspan="2">Harga</th>
                                     <th rowspan="2">Denda</th>
                                     <th rowspan="2">Total</th>
                                     <th colspan="3">Tanggal</th>
                                     <th rowspan="2">Status</th>
                                     <!-- <th class="action" rowspan="2">Action</th> -->
                                 </tr>
                                 <tr>
                                     <th>Rental</th>
                                     <th>Kembali</th>
                                     <th>Selesai</th>
                                 </tr>
                             </thead>
                             <tbody>
                                 <?php
                                    $no = 1;
                                    foreach ($transaksi as  $value) : ?>
                                 <tr>
                                     <td class="no"><?= $no++ ?> </td>
                                     <td><?= $value['merek'] ?> </td>
                                     <td class="right"><?= money($value['total_harga']) ?> </td>
                                     <td class="right"><?= money($value['total_denda']) ?> </td>
                                     <td class="right"><?= money($value['total_denda'] + $value['total_harga']) ?> </td>
                                     <td><?= $value['tanggal_rental'] ?> </td>
                                     <td><?= $value['tanggal_kembali'] ?> </td>
                                     <td><?= $value['tanggal_pengembalian'] ?> </td>
                                     <td>


                                         <?php
                                               $status = $value['status_transaksi'];
                                                if ($status == 1) { ?>
                                         <small class="badge badge-success" style="width:80px">Running</small>
                                         <?php    } else if ($status == 2) { ?>
                                         <small class="badge badge-danger" style="width:80px">Done</small>
                                         <?php   } else { ?>
                                         <small class="badge badge-default" style="width:80px">-</small>
                                         <?php     } ?>



                                     </td>



                                     <!-- <td><?= $value['id_transaksi'] ?> </td> -->
                                 </tr>
                                 <?php endforeach; ?>

                             </tbody>


                         </table>
                     </div>
                     <div class="tab-pane" id="profil">
                         <form class="form-horizontal" role="form" action="<?= base_url(url(1) . '/edit') ?>"
                             method="POST" enctype="multipart/form-data">
                             <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">Nama</label>
                                 <div class="col-sm-10">
                                     <input type="text" class="form-control" id="inputName" name="nama_customer"
                                         value="<?= $customer['nama_customer'] ?>">
                                 </div>
                             </div>
                             <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                 <div class="col-sm-10">
                                     <input type="text" class="form-control" id="inputName" name="tempat_lahir"
                                         value="<?= $customer['tempat_lahir'] ?>">
                                 </div>
                             </div>
                             <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                 <div class="col-sm-10">
                                     <input type="date" class="form-control" id="inputName" name="tanggal_lahir"
                                         value="<?= $customer['tanggal_lahir'] ?>">
                                 </div>
                             </div>
                             <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">Gender</label>
                                 <div class="col-sm-10">
                                     <select type="text" class="form-control" name="gender" value="">
                                         <?php
                                            $gender = $customer['gender'];
                                            if ($gender == 'L') { ?>
                                         <option value="">Pilih</option>
                                         <option value="L" selected>Laki - Laki</option>
                                         <option value="P">Perempuan</option>
                                         <?php  } else if ($gender == 'P') { ?>
                                         <option value="">Pilih</option>
                                         <option value="L">Laki - Laki</option>
                                         <option value="P" selected>Perempuan</option>
                                         <?php  } else { ?>
                                         <option value="">Pilih</option>
                                         <option value="L">Laki - Laki</option>
                                         <option value="P">Perempuan</option>
                                         <?php   } ?>
                                     </select>
                                     <!-- <input type="text" class="form-control" id="inputName" name="gender" value="<?= $customer['gender'] ?>"> -->
                                 </div>
                             </div>
                             <div class="form-group row">
                                 <label for="inputName" class="col-sm-2 col-form-label">Telpon</label>
                                 <div class="col-sm-10">
                                     <input type="text" class="form-control" id="inputName" name="telpon"
                                         value="<?= $customer['telpon'] ?>">
                                 </div>
                             </div>


                             <div class="form-group row">
                                 <label for="inputExperience" class="col-sm-2 col-form-label">Alamat</label>
                                 <div class="col-sm-10">
                                     <textarea class="form-control" id="inputExperience"
                                         name="alamat"><?= $customer['alamat'] ?></textarea>
                                 </div>
                             </div>


                             <div class="form-group row">
                                 <div class="offset-sm-2 col-sm-10 pull-right" style="text-align:right;">
                                     <input type="hidden" name="url" value="<?= url('all') ?>">
                                     <input type="hidden" name="id" value="<?= $customer['id_customer'] ?>">
                                     <button type="submit" class="btn btn-success">Simpan</button>
                                 </div>
                             </div>
                         </form>
                     </div>


                     <!-- /.tab-pane -->
                     <div class="tab-pane" id="ktp">
                         <form role="form" action="<?= base_url(url(1) . '/uploadKTP') ?>" method="POST"
                             enctype="multipart/form-data">


                             <div class="form-group">
                                 <!-- <label for="customFile">Custom File</label> -->
                                 <div class="row">
                                     <div class="col-md-12">
                                         <div class="form-group">
                                             <!-- <label for="customFile">Custom File</label> -->
                                             <div class="row">
                                                 <div class="col-md-10">
                                                     <img src="<?= base_url() ?>assets/img/<?= $customer['foto_ktp'] ?>"
                                                         width=" 720px" height="520px">
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                             <div class="form-group">
                                 <!-- <label for="customFile">Custom File</label> -->
                                 <div class="row">
                                     <div class="col-md-10">
                                         <div class="custom-file">
                                             <input type="file" class="custom-file-input" id="customFile" name="image">
                                             <label class="custom-file-label" for="customFile">Pilih file</label>
                                         </div>
                                     </div>
                                     <div class="col-md-2">
                                         <input type="hidden" name="url" value="<?= url('all') ?>">
                                         <input type="hidden" name="id" value="<?= $customer['id_customer'] ?>">
                                         <button class="btn btn-primary">Upload</button>
                                     </div>
                                 </div>

                             </div>
                         </form>
                     </div>
                     <!-- /.tab-pane -->


                     <!-- /.tab-pane -->
                 </div>
                 <!-- /.tab-content -->
             </div><!-- /.card-body -->
         </div>
         <!-- /.nav-tabs-custom -->
     </div>
     <!-- /.col -->
 </div>
<div class="row" style="padding-top:20px">

    <div class="col-md-12">
        <?= $this->session->flashdata("message") ?>
        <div class="card">
            <div class=" card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="card-title">Data Customer</h3>
                    </div>
                    <div class="col-md-6" style="text-align: right;">

                        <button class="btn btn-primary" data-toggle="modal" data-target="#tambah">+ Tambah</button>
                    </div>
                </div>


            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="no">No</th>
                            <th>Nama</th>
                            <th>TTL</th>
                            <th>Gender</th>
                            <th>Telepon</th>
                            <th>Alamat</th>
                            <th class="action">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($customer as  $value) : ?>
                        <tr>
                            <td class="no"><?= $no++; ?></td>
                            <td><?= $value['nama_customer'] ?></td>
                            <td><?= $value['tempat_lahir'] ?>, <?= $value['tanggal_lahir'] ?></td>
                            <td>
                                <?php
                                    $gender = $value['gender'];
                                    if ($gender == 'L') {
                                        echo "Laki - Laki";
                                    } else if ($gender == 'P') {
                                        echo "Perempuan";
                                    } else {
                                        echo "";
                                    }
                                    ?>

                            </td>
                            <td><?= $value['telpon'] ?></td>
                            <td><?= $value['alamat'] ?></td>
                            <td class="action">
                                <a href="<?= base_url('customer/detail/') ?><?= $value['id_customer'] ?>"
                                    class="btn btn-primary btn-sm"><b>D</b></a>
                                <button class="btn btn-danger btn-sm" data-toggle="modal"
                                    data-target="#delete<?= $value['id_customer'] ?>"><i
                                        class="fa fa-times"></i></button>
                            </td>

                        </tr>
                        <?php endforeach;
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- modal untuk tambah data -->


<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" action="<?= base_url(url(1) . '/tambah') ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama</label>
                                <input type="text" class="form-control" name="nama_customer" value="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Telpon</label>
                                <input type="text" class="form-control" name="telpon" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Tempat Lahir</label>
                                <input type="text" class="form-control" name="tempat_lahir" value="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Tanggal Lahir</label>
                                <input type="date" class="form-control" name="tanggal_lahir" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Gender</label>
                                <select type="text" class="form-control" name="gender" value="" required>
                                    <option value="">Pilih</option>
                                    <option value="L">Laki - Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Alamat</label>
                                <textarea rows="1" type="text" class="form-control" name="alamat" value=""></textarea>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Username</label>
                                <input type="text" class="form-control" name="username" value="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control" name="password" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Foto KTP</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="image">
                                    <label class="custom-file-label" for="customFile">Pilih file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <input type="hidden" name="url" value="<?= url() ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!--begin proses delete -->
<?php
foreach ($customer as  $value) {
    echo  modal_delete($value['id_customer'], 'delete');
}
?>
<!--end proses delete -->

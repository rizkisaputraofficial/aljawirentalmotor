</section>

</div>
<!-- /.content-wrapper -->
<!-- <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        <b>Version</b> 1.0
    </div>
    <strong>Aljawi Rental Motor&copy; 2021 </strong>
</footer> -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?= base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url() ?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>/assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>/assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="<?= base_url() ?>/assets/dist/js/demo.js"></script>
<script src="<?= base_url() ?>/assets/plugins/daterangepicker/daterangepicker.js"></script>


<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://cdn.canvasjs.com/jquery.canvasjs.min.js"></script>


<script type="text/javascript">
$(document).ready(function() {
    $('.motor').change(function() {

        const idMotor = $('#id_motor').val();
        const idTransaki = $('#id_transaksi').val();
        $.ajax({
            url: "<?= base_url('transaksi/getDataTarifByIdMotor'); ?>",
            method: "POST",
            data: {
                id_motor: idMotor,
                id_transaksi: idTransaki
            },
            dataType: 'json',
            success: function(data) {

                console.log(data);
                var html = '';
                html +=
                    '<select class="form-control" name="id_tarif" id="data-tarif" required>'
                html += '<option value="">Pilih</option>';
                for (let i = 0; i < data.tarif.length; i++) {
                    if (data.transaksi.id_tarif_fk == data.tarif[i].id_tarif) {
                        html += '<option value=' + data.tarif[i].id_tarif +
                            ' selected>' + data.tarif[i].jenis_tarif + ' - (' + data.tarif[
                                i]
                            .harga_tarif +
                            ')</option>';
                    } else {
                        html += '<option value=' + data.tarif[i].id_tarif +
                            '>' + data.tarif[i].jenis_tarif + ' - (' + data.tarif[i]
                            .harga_tarif +
                            ')</option>';
                    }
                }
                html += '</select>';
                $('.show-tarif').html(html);
                $('#tarif').hide();

            }

        });

    });
    $('.show-tarif').change(function() {

        const id1 = $('#data-tarif').val();
        const id2 = $('#tarif').val();

        if (id1 == null) {
            id = id2;
        } else {
            di = id1;
        }
        if (id2 == null) {
            id = id1;
        } else {
            id = id2;
        }


        $.ajax({
            url: "<?= base_url('transaksi/getDataTarifByIdTarif'); ?>",
            method: "POST",
            data: {
                id: id
            },
            dataType: 'json',


            success: function(data) {
                console.log(data.harga_tarif);
                var html = '';
                html +=
                    '<input type="Number" class="form-control" name="total_harga"  value=' +
                    data.harga_tarif + '>';

                $('.show-harga').html(html);
                $('#harga').hide();
            }
        });
    });

});
</script>





























<script type="text/javascript">
$(document).ready(function() {
    $('.data-motor').change(function() {

        const idMotor = $('#id_motor').val();

        $.ajax({
            url: "<?= base_url('transaksi/getDataTarifByIdMotorOther'); ?>",
            method: "POST",
            data: {
                id_motor: idMotor,
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);

                var html = '';
                html +=
                    '<select class="form-control" name="id_tarif" id="data-tarif" required>'
                html += '<option value="">Pilih</option>';
                for (let i = 0; i < data.length; i++) {

                    html += '<option value=' + data[i].id_tarif +
                        '>' + data[i].jenis_tarif + ' - (' + data[i]
                        .harga_tarif +
                        ')</option>';

                }
                html += '</select>';
                $('.show-tarif').html(html);
                $('#tarif').hide();

            }

        });

    });







});
</script>









<script>
window.onload = function() {


    $('.pilih-tahun').change(function() {

        var tahunDariView = $('#tahun').val();
        $.ajax({
            url: 'http://localhost/aljawirentalmotor/statistik/getAllTransaksiByMonth',
            type: 'post',
            data: {
                tahunKirimKeControler: tahunDariView
            },
            dataType: 'JSON',
            success: function(data) {


                var options = {
                    exportEnabled: true,
                    animationEnabled: true,
                    theme: "light2",
                    title: {
                        text: 'Jumlah Transaksi Selama Tahun ' + tahunDariView,
                        fontSize: 20
                    },
                    data: [{
                        // Change type to "doughnut", "line", "splineArea", etc.
                        type: "column",
                        indexLabel: "{y}",
                        dataPoints: data
                    }]
                };
                $("#chartContainer").CanvasJSChart(options);
            }

        });


    })

    $.ajax({
        url: 'http://localhost/aljawirentalmotor/statistik/getAllTransaksiByMonth',
        type: 'post',
        dataType: 'JSON',
        success: function(data) {


            var options = {
                exportEnabled: true,
                animationEnabled: true,
                theme: "light2",
                title: {
                    text: 'Jumlah Pendapatan Selama Tahun 2023',
                    fontSize: 20
                },
                data: [{
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    indexLabel: "{y}",
                    dataPoints: data
                }]
            };

            $("#chartContainer").CanvasJSChart(options);
        }

    });







    $('.chartPendapatan').change(function() {

        var tahun = $('#tahun1').val();
        $.ajax({
            url: 'http://localhost/aljawirentalmotor/statistik/getAllPendapatanByMonth',
            type: 'post',
            data: {
                tahun: tahun
            },
            dataType: 'JSON',
            success: function(data) {

                var options = {
                    exportEnabled: true,
                    animationEnabled: true,
                    theme: "light2",
                    title: {
                        text: 'Jumlah Pendapatan Selama Tahun ' + tahun,
                        fontSize: 20
                    },
                    axisY: {

                        includeZero: true,

                    },
                    data: [{
                        type: "spline",
                        indexLabel: "{y}",
                        dataPoints: data
                    }]
                };
                $("#resultChartPendapatan").CanvasJSChart(options);
            }

        });


    })

    $.ajax({
        url: 'http://localhost/aljawirentalmotor/statistik/getAllPendapatanByMonth',
        type: 'post',
        dataType: 'JSON',

        success: function(data) {
            var options = {
                exportEnabled: true,
                animationEnabled: true,
                theme: "light2",
                title: {
                    text: 'Jumlah Pendapatan Selama Tahun 2023',
                    fontSize: 20
                },
                axisY: {

                    includeZero: true,

                },
                data: [{
                    type: "spline",
                    indexLabel: "{y}",
                    dataPoints: data
                }]
            };

            $("#resultChartPendapatan").CanvasJSChart(options);
        }

    });


}
</script>















</body>

</html>
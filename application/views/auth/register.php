<section class="section-padding overlay">
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">

                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Register</h4>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="<?php echo base_url('register/prosesRegister') ?>">
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="nama">Nama</label>
                                    <input id="nama" type="text" class="form-control" name="nama_customer" autofocus>

                                </div>
                                <div class="form-group col-6">
                                    <label for="username">Username</label>
                                    <input id="username" type="text" class="form-control" name="username">

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="nama">Tempat Lahir</label>
                                    <input id="nama" type="text" class="form-control" name="tempat_lahir" autofocus>

                                </div>
                                <div class="form-group col-6">
                                    <label for="username">Tanggal Lahir</label>
                                    <input id="username" type="date" class="form-control" name="tanggal_lahir">

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <input id="alamat" type="text" class="form-control" name="alamat">

                                <div class="invalid-feedback">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="password" class="d-block">Gender</label>
                                    <select class="form-control" name="gender">
                                        <option value="">--Pilih Gender--</option>
                                        <option value="L">Laki-laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>

                                </div>
                                <div class="form-group col-6">
                                    <label for="no_telepon" class="d-block">No. Telepon</label>
                                    <input id="no_telepon" type="text" class="form-control" name="telpon">

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-6">
                                    <label>No. KTP</label>
                                    <input type="text" name="no_ktp" class="form-control">
                                </div>
                                <div class="form-group col-6">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">
                                    Register
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="simple-footer">
                    Copyright &copy; Stisla 2018
                </div>
            </div>
        </div>
    </div>
</section>
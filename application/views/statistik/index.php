<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-success text-white text-center">
                Statistik Laporan Transaksi Customer
            </div>

            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-8">

                    </div>
                    <div class="col-md-4">
                        <div class="form-group">

                            <select class="form-control pilih-tahun" name="tahun" value="" id="tahun">
                                <option value="">Pilih Tahun</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                    </div>
                </div>


            </div>
            <!-- /.card-body -->

        </div>
    </div>
    <br>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-success text-white text-center">
                Statistik Laporan Pendapatan
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-8">
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">

                            <select class="form-control chartPendapatan" name="tahun" value="" id="tahun1">
                                <option value="">Pilih Tahun</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="col-md-2">
                        <div class="form-group">

                            <select class="form-control chartPendapatan" name="bulan" value="" id="bulan">
                                <option value="">Pilih Bulan</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">11</option>
                            </select>
                        </div>
                    </div> -->
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div id="resultChartPendapatan" style="height: 300px; width: 100%;"></div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
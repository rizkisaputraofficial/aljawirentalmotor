<?php

function session($index)
{
    $ci = get_instance();
    return    $ci->session->userdata($index);
}
function encrypt_decrypt($action, $string)
{
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'ini adalah my secret key';
    $secret_iv = 'ini adalah my secret iv';
    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}


function  security_access()
{
    if (empty(session('id_user'))) {
        redirect('auth/AccessDenied');
    }
}

// function start_pagination()
// {
//     $ci = get_instance();
//     $ci->session->unset_userdata('limit');
//     return $ci->uri->segment(3);
// }

// function showData()
// {
//     $show = show();
//     if ($show == 10) {
//         echo '<div class="dataTables_length">
//             <select name="data" aria-controls="m_table_3"  id="show-data" class="custom-select custom-select-sm form-control form-control-sm show-data">

//                 <option value="10" selected>10</option>
//                 <option value="25" >25</option>
//                 <option value="50">50</option>
//                 <option value="100">100</option>
//             </select>
//         </div>';
//     } else if ($show == 25) {
//         echo '<div class="dataTables_length">
//             <select name="data" aria-controls="m_table_3"  id="show-data" class="custom-select custom-select-sm form-control form-control-sm show-data">

//                 <option value="10" >10</option>
//                 <option value="25" selected>25</option>
//                 <option value="50">50</option>
//                 <option value="100">100</option>
//             </select>
//         </div>';
//     } else if ($show == 50) {
//         echo '<div class="dataTables_length">
//             <select name="data" aria-controls="m_table_3"  id="show-data" class="custom-select custom-select-sm form-control form-control-sm show-data">

//                 <option value="10" >10</option>
//                 <option value="25" >25</option>
//                 <option value="50" selected>50</option>
//                 <option value="100">100</option>
//             </select>
//         </div>';
//     } else if ($show == 100) {
//         echo '<div class="dataTables_length">
//             <select name="data" aria-controls="m_table_3"  id="show-data" class="custom-select custom-select-sm form-control form-control-sm show-data">

//                 <option value="10" >10</option>
//                 <option value="25" >25</option>
//                 <option value="50" >50</option>
//                 <option value="100" selected>100</option>
//             </select>
//         </div>';
//     } else {
//         echo '<div class="dataTables_length">
//             <select name="data" aria-controls="m_table_3"  id="show-data" class="custom-select custom-select-sm form-control form-control-sm show-data">

//                 <option value="10" selected>10</option>
//                 <option value="25" >25</option>
//                 <option value="50">50</option>
//                 <option value="100">100</option>
//             </select>
//         </div>';
//     }
// }

// function begin()
// {

//     $ci = get_instance();


//     $limit = session('limit');
//     if (!empty($limit)) {
//         $data = html_escape($ci->input->get('page'));
//         if (!empty($data)) {
//             $data =  (($data - 1) * page()) + 1;
//         } else {
//             if (empty(session('row-result'))) {
//                 $data = 0;
//             } else {
//                 $data = 1;
//             }
//         }
//     } else {
//         if (session('page-id') == true) {
//             $data = $ci->uri->segment(2);
//             if (empty($data)) {
//                 $data = 1;
//             } else {
//                 $data = $data + 1;
//             }
//         } else {
//             $data = $ci->uri->segment(3);
//             if (empty($data)) {
//                 $data = 1;
//             } else {
//                 $data = $data + 1;
//             }
//         }
//     }








//     return $data;
// }



function clearSession()
{
    $ci = get_instance();
    $controller = $ci->session->userdata('controller');
    $page = $ci->session->userdata('page');


    $segment1 = $ci->uri->segment(1);
    $segment2 =  $ci->uri->segment(2);


    if ($controller != $segment1) {
        $ci->session->unset_userdata('autoFilter');
        $ci->session->unset_userdata('urutan');
        $ci->session->unset_userdata('sortBy');
        $ci->session->unset_userdata('keyword');
        $ci->session->unset_userdata('bulan');
        $ci->session->unset_userdata('tahun');
        $ci->session->unset_userdata('tanggal_awal');
        $ci->session->unset_userdata('tanggal_akhir');
        $ci->session->unset_userdata('tahun');
        $ci->session->unset_userdata('show');
    } else {

        if ($page != $segment2) {

            if (preg_match('/create/', strtolower($segment2))) {
            } else if (preg_match('/edit/', strtolower($segment2))) {
            } else if (preg_match('/delete/', strtolower($segment2))) {
            } else if (preg_match('/hapus/', strtolower($segment2))) {
            } else if (preg_match('/tambah/', strtolower($segment2))) {
            } else if (preg_match('/ubah/', strtolower($segment2))) {
            } else if (preg_match('/update/', strtolower($segment2))) {
            } else {
                $ci->session->unset_userdata('autoFilter');
                $ci->session->unset_userdata('urutan');
                $ci->session->unset_userdata('sortBy');
                $ci->session->unset_userdata('keyword');
                $ci->session->unset_userdata('bulan');
                $ci->session->unset_userdata('tahun');
                $ci->session->unset_userdata('tanggal_awal');
                $ci->session->unset_userdata('tanggal_akhir');
                $ci->session->unset_userdata('show');
            }
        }
    }
}



// function pagination($url, $page, $row)
// {
//     $ci = get_instance();
//     $config['base_url'] = base_url() . $url;
//     $config['total_rows'] = $row;
//     $config['per_page'] = $page;
//     $config['num_links'] = 5;
//     $config['full_tag_open'] = '<div class="dataTables_paginate paging_simple_numbers" >
//                 <ul class="pagination">';
//     $config['full_tag_close'] = '</ul></div> ';
//     $config['first_link'] = 'First';
//     $config['first_tag_open'] = '  <li class="paginate_button page-item ">';
//     $config['first_tag_close'] = '</li >';
//     $config['last_link'] = 'Last';
//     $config['last_tag_open'] = '  <li class="paginate_button page-item next" id="m_table_3_next">';
//     $config['last_tag_close'] = '</li >';
//     $config['next_link'] = '&raquo';
//     $config['next_tag_open'] = '   <li id="m_table_3_next" class="paginate_button page-item ">';
//     $config['next_tag_close'] = '</li >';
//     $config['prev_link'] = '&laquo';
//     $config['prev_tag_open'] = '<li id="previous" class="paginate_button page-item ">';
//     $config['prev_tag_close'] = '</li >';

//     $config['cur_tag_open'] = '<li class="paginate_button page-item active"><a href="#" aria-controls="m_table_3" data-dt-idx="3" tabindex="0" class="page-link">';
//     $config['cur_tag_close'] = '</a></li >';

//     $config['num_tag_open'] = '<li class="paginate_button page-item ">';
//     $config['num_tag_close'] = '</li >';

//     $config['attributes'] = array('class' => 'page-link');


//     return  $ci->pagination->initialize($config);
// }

function url($data = null)
{
    $ci = get_instance();
    if ($data == 1) {
        $url = $ci->uri->segment(1);
    } else if ($data == 2) {
        $url = $ci->uri->segment(2);
    } else if ($data == 3) {
        $url = $ci->uri->segment(3);
    } else if ($data == 'all') {
        $url = $ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/' . $ci->uri->segment(3) . '/' . $ci->uri->segment(4);
    } else {
        $url = $ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/' . $ci->uri->segment(3);
    }
    return $url;
}



function notification_succes($data =  null, $more = null)
{

    $ci = get_instance();
    if ($more == null) {
        $koma = '';
    } else {
        $koma = ',';
    }
    $ci->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      Data <strong> Berhasil</strong> ' . $data . '!' . $koma . ' ' . $more . '.
    </div>');
}
function notification_fail($data =  null, $more = null)
{

    $ci = get_instance();
    if ($more == null) {
        $koma = '';
    } else {
        $koma = ',';
    }
    $ci->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      Data <strong> Gagal</strong> ' . $data . '!' . $koma . ' ' . $more . '.
    </div>');
}

function notification_fail_image($data =  null, $error = null)
{

    $ci = get_instance();
    if ($error == -1) {
        $more = 'File gambar harus di isi';
    } else if ($error == -2) {
        $more = 'Ektensi file harus jpg/jpeg/png';
    } else if ($error == -3) {
        $more = 'Ukuran file terlalu besar';
    } else if ($error == -4) {
        $more = 'Ukuran file terlalu kecil';
    }

    $ci->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data <strong>Gagal </strong>' . $data . '!, ' . $more . '</div>  ');
}
function notification_warning($data =  null, $more = null)
{

    $ci = get_instance();
    if ($more == null) {
        $koma = '';
    } else {
        $koma = ',';
    }
    $ci->session->set_flashdata('message', '<div class=" alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>Data <strong>Gagal </strong>' . $data . '!' . $koma . ' ' . $more . '</div>  ');
}


function timestamp()
{
    return date('Y-m-d G:i:s');
}


function uploadImage($data = null)
{
    $namaFile = $_FILES[$data['name']]['name'];
    $ukuranFile = $_FILES[$data['name']]['size'];
    $error = $_FILES[$data['name']]['error'];
    $tmpName = $_FILES[$data['name']]['tmp_name'];

    $ekstensiGambarValid = $data['ekstensi'];
    $ekstensiGambar = explode('.', $namaFile);
    $ekstensiGambar = strtolower(end($ekstensiGambar));


    // cek file 
    $emptyFile = $data['empty'];
    if ($emptyFile == TRUE) {
        $error = $_FILES[$data['name']]['error'];
        if ($error == 4) {
            return 'default.jpg';
        }
    } else {
        if ($error == 4) {
            return -1;
        }
    }

    //cek ekstensi
    if ($ekstensiGambarValid == NULL) {
        $ekstensiDefault = ['jpg', 'jpeg', 'png'];
        if (!in_array($ekstensiGambar, $ekstensiDefault)) {

            return -2;
        }
    } else {
        if (!in_array($ekstensiGambar, $ekstensiGambarValid)) {
            return -2;
        }
    }

    // cek ukuran
    $maxSize =  $data['max_size'];
    if ($maxSize != NULL) {
        $ukuranValidMax = $maxSize * 1000000;
        $sizeKb = $maxSize * 1000;

        if ($ukuranFile > $ukuranValidMax) {
            return -3;
        }
    }

    $minSize =  $data['min_size'];
    if ($minSize != NULL) {
        $ukuranValidMin = $minSize * 1000000;
        $sizeKb = $minSize * 1000;

        if ($ukuranFile < $ukuranValidMin) {
            return -4;
        }
    }

    $firstName = $data['first_name'];
    if ($firstName != NULL) {
        $firstName = $firstName . '_';
    } else {
        $firstName = '';
    }


    $lastName  = $data['last_name'];
    if ($lastName != NULL) {
        $lastName = $lastName . '_';
    } else {
        $lastName = '';
    }


    $namaFileBaru = strtoupper($firstName . $lastName);
    $namaFileBaru .= uniqid();
    $namaFileBaru .= '.';
    $namaFileBaru .= $ekstensiGambar;

    $location  = $data['location'];
    move_uploaded_file($tmpName, '.' . $location . $namaFileBaru);
    return $namaFileBaru;
}


function modal_delete($id = null, $method = null, $url = null)
{
    if (empty($method)) {
        $method = 'delete';
    } else {
        $method = $method;
    }
    if (empty($url)) {
        $url = url();
    } else {
        $url =  url($url);
    }

    $data = ' <div class="modal fade" id="delete' . $id . '">
<div class="modal-dialog modal-md">
    <form role="form" action=' . base_url(url(1) . "/$method") . ' method="post">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Delete</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body" style="text-align: center;">


        <img src="' . base_url("assets/icon/myicon/icon-delete.png") . '" alt="">
        <h4>Apakah anda ingin menghapus data ini?</h4>
    </div>

    <div class="modal-footer justify-content-between">
        <input type="hidden" name="url" value=' . $url  . '>
<input type="hidden" name="id" value=' . $id . '>
<button type="button" class="btn btn-primary" data-dismiss="modal" style="width: 70px;">Tidak</button>
<button type="submit" class="btn btn-danger" style="width: 70px;">Iya</button>
</div>

</div>
</form>
</div>
</div> ';
    return $data;
}

function money($nominal = null)
{
    return number_format($nominal, 0, '.', '.');
}

function load_view($view = null, $data = null)
{

    $ci = get_instance();
    $ci->load->view('template/header', $data);
    $ci->load->view('template/sidebar');
    $ci->load->view($view, $data);
    $ci->load->view('template/footer');
}
function load_view_customer($view = null, $data = null)
{

    $ci = get_instance();
    $ci->load->view('template_customer/header', $data);
    // $ci->load->view('template_customer/sidebar');
    $ci->load->view($view, $data);
    $ci->load->view('template_customer/footer');
}

function redirect_back($data = null, $notif = null, $notif2 = null)
{

    $ci = get_instance();
    $url = $ci->input->post('url');
    if ($data > 0) {
        notification_succes($notif, $notif2);
    } else {
        notification_fail($notif, $notif2);
    }
    redirect($url);
}


function upload($location = null, $ekstensi = null)
{
    if ($location == null) {
        $location = 'assets/img/';
    } else {
        $location = $location;
    }
    if ($ekstensi == null) {
        $ekstensi = ['jpg', 'png', 'jpeg'];
    } else {
        $ekstensi = $ekstensi;
    }

    $nama = $_FILES['image']['name'];
    $type = $_FILES['image']['type'];
    $error = $_FILES['image']['error'];
    $size = $_FILES['image']['size'];
    $tempName = $_FILES['image']['tmp_name'];

    //cek ada file yang diupload atau tidak
    if ($error == 4) {
        redirect_back($data = 0, 'Diubah', 'Tidak ada file yang di upload');
    }

    //cek ekstensi file
    $ekstensiGambarValid =  $ekstensi;
    $ekstensiGambar  = explode('.', $nama);
    $ekstensiGambar = strtolower(end($ekstensiGambar));
    if (!in_array($ekstensiGambar, $ekstensiGambarValid)) {
        redirect_back($data = 0, 'Diubah', 'Ekstensi file tidak diizinkan');
    }

    //cek ukuran
    if ($size > 5000000) {
        redirect_back($data = 0, 'Diubah', 'Ukuran file terlalu besar (max 5 mb)');
    }

    $namaBaru = uniqid() . '.' . $ekstensiGambar;
    move_uploaded_file($tempName, $location . $namaBaru);
    return $namaBaru;
}

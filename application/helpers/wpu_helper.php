<?php




function check_maintanance(){

	$data = db_get_row('maintanance',1);
	if($data['status'] == 1){
		$door = session('secret_door');
		if($door != 'developer'){			
			redirect('maintanance');
		}
	}
	

}

function is_logged_in()
{
	$ci = get_instance();
	if (!$ci->session->userdata('email')) {
		redirect('auth');
	} else {
		$role_id = $ci->session->userdata('role_id');
		$menu = $ci->uri->segment(1);
		$queryMenu = $ci->db->get_where('user_menu', ['menu' => $menu])->row_array();
		$menu_id = $queryMenu['id_user_menu'];
		$userAccess = $ci->db->get_where('user_access_menu', [
			'role_id' => $role_id,
			'menu_id' => $menu_id
		]);
		//    var_dump($userAccess);

		if ($userAccess->num_rows() < 1) {
			redirect('auth/blocked');
		}
	}
}

function  security_access(){
	if(empty(session('id'))){
		redirect('auth/AccessDenied');
	 }else{
	 	$email =   session('email');
		$role_id = session('role_id');
		$password =session('password');

		db_where('email', $email);
		db_where('role_id', $role_id);
		db_where('password', $password);
		db_where('is_active', 1);
		$user = db_get_row('user');

		if (!empty($user)) {
		
			// $menu = url(1);		
			// $queryMenu = db_get_row('user_menu', ['menu_access_url' => $menu]);
			// $idMenu = $queryMenu['id_user_menu'];


			// db_where('id_user_role_fk', $role_id);
			// db_where('id_user_menu_fk', $idMenu);
			// $data = db_get_row('user_access_menu');
			
			// $menuAccess = $data['access_menu'];
			// if($menuAccess != 1){
			// 	redirect('auth/AccessDenied');
			// }
		}else{
			redirect('auth/AccessDenied');
		}
	 }
}


function version(){
	$ci = get_instance();
	$result = $ci->db->get('version')->last_row();

	return $result->version;

}
function check_access($role_id, $menu_id)
{
	$ci = get_instance();
	$ci->db->where('role_id', $role_id);
	$ci->db->where('menu_id', $menu_id);
	$result = $ci->db->get('user_access_menu');
	if ($result->num_rows() > 0) {
		return "checked = 'checked'";
	}
}

function check_access_new($role_id, $menu_id, $submenu_id)
{
	
	db_where('role_id',$role_id);
	db_where('menu_id',$menu_id);
	db_where('sub_menu_id',$submenu_id);
	$result = db_get_row('user_access_menu');

	if ($result > 0) {
		return "checked = 'checked'";
	}
}



function check_access_crud($role_id, $menu_id, $crud)
{
	$dbcrud = '';
	$ci = get_instance();
	$ci->db->select('*');
	$ci->db->from('user_access_menu');
	$ci->db->where('role_id', $role_id);
	$ci->db->where('menu_id', $menu_id);
	$result = $ci->db->get()->row_array();
	if ($result['create'] == $crud) {
		$dbcrud = $crud;
	} else if ($result['read'] == $crud) {
		$dbcrud = $crud;
	} else if ($result['update'] == $crud) {
		$dbcrud = $crud;
	} else if ($result['delete'] == $crud) {
		$dbcrud = $crud;
	}

	if ($dbcrud == $crud) {
		return "checked = 'checked'";
	}
}


function check_access_crud_new($role_id, $menu_id, $submenu_id)
{
	$dbcrud = '';
	$ci = get_instance();
	$ci->db->select('*');
	$ci->db->from('user_access_menu');
	$ci->db->where('role_id', $role_id);
	$ci->db->where('menu_id', $menu_id);
	$ci->db->where('sub_menu_id', $submenu_id);
	$result = $ci->db->get()->row_array();

}




function check_access_menu($role_id, $menu_id)
{
	
	
	db_where('id_user_role_fk',$role_id);
	db_where('id_user_menu_fk',$menu_id);
	$result = db_get_row('user_access_menu');
	return $result['access_menu'];

}

function check_access_sub_menu($role_id, $menu_id,$submenu_id){
	db_where('id_user_role_fk',$role_id);
	db_where('id_user_menu_fk',$menu_id);
	$result = db_get_row('user_access_menu');
	$idUserAccessMenu =  $result['id_user_access_menu'];

	db_where('id_user_sub_menu_fk',$submenu_id);
	db_where('id_user_access_menu_fk',$idUserAccessMenu);
	$result = db_get_row('user_access_sub_menu');

	return $result['access_sub_menu'];


}

function get_id_access_menu($role_id, $menu_id)
{
	
	
	db_where('id_user_role_fk',$role_id);
	db_where('id_user_menu_fk',$menu_id);
	$result = db_get_row('user_access_menu');
	return $result['id_user_access_menu'];

}




function check_access_submenu($role_id, $menu_id, $submenu_id)
{
	
	db_join('user_access_menu');
	db_where('id_user_role_fk',$role_id);
	db_where('id_user_menu_fk',$menu_id);
	db_where('id_user_sub_menu_fk',$submenu_id);
	db_where('access_sub_menu',1);
	return db_get_num('user_access_sub_menu');

}


function check_access_read($role_id, $menu_id, $submenu_id)
{
	
	

	db_join('user_access_menu');
	db_where('id_user_role_fk',$role_id);
	db_where('id_user_menu_fk',$menu_id);
	db_where('id_user_sub_menu_fk',$submenu_id);
	db_where('read',1);
	return db_get_num('user_access_sub_menu');
	

}


function check_access_create($role_id, $menu_id, $submenu_id)
{
	
	db_join('user_access_menu');
	db_where('id_user_role_fk',$role_id);
	db_where('id_user_menu_fk',$menu_id);
	db_where('id_user_sub_menu_fk',$submenu_id);
	db_where('create',1);
	return db_get_num('user_access_sub_menu');

}

function check_access_update($role_id, $menu_id, $submenu_id)
{
	
	db_join('user_access_menu');
	db_where('id_user_role_fk',$role_id);
	db_where('id_user_menu_fk',$menu_id);
	db_where('id_user_sub_menu_fk',$submenu_id);
	db_where('update',1);
	return db_get_num('user_access_sub_menu');

}


function check_access_delete($role_id, $menu_id, $submenu_id)
{
	
	db_join('user_access_menu');
	db_where('id_user_role_fk',$role_id);
	db_where('id_user_menu_fk',$menu_id);
	db_where('id_user_sub_menu_fk',$submenu_id);
	db_where('delete',1);
	return db_get_num('user_access_sub_menu');

}

function user_access($crud=null) 
{
return "success";
}

// function user_access($crud=null)
// {
// 	$ci = get_instance();
// 	$email =   session('email');
// 	$role_id = session('role_id');
// 	$password =session('password');

	
// 	db_where('email', $email);
// 	db_where('role_id', $role_id);
// 	db_where('password', $password);


// 	$user = db_get_row('user');;

// 	if ($user['role_id'] == $role_id && $user['password'] == $password && $user['email'] == $email) {
	
// 		$menu = $ci->uri->segment(1);		
// 		$queryMenu = $ci->db->get_where('user_menu', ['menu_access_url' => $menu])->row_array();
// 		$idMenu = $queryMenu['id_user_menu'];
// 		db_where('id_user_role_fk', $role_id);
// 		db_where('id_user_menu_fk', $idMenu);
// 		$data = db_get_row('user_access_menu');
		
// 		if ($crud == "create" ) {
// 			return $data['create'];
// 		} else if ($crud == "read") {
// 			return $data['read'];
// 		} else if ($crud == "update") {
// 			return $data['update'];
// 		} else if ($crud == "delete") {
// 			return $data['delete'];
// 		} else {
// 			return "fail";
// 		}
// 	} else {
// 		redirect('auth/AccessDenied');
// 	}
// }

function verify_token($token)
{
	$ci = get_instance();
	$tokenLogin = $ci->session->userdata('token');
	if ($tokenLogin == $token) {
		return 'success';
	} else {
		return "fail";
		// redirect('auth/AccessDenied');
	}
}

function createUserLoginHistory($user = null ,$email = null,$password = null)
{
	if(!empty($user['id'])){
			$status = 1;
		}else{
			$status =0;
		}
		
	$dataLogin = [
		'tanggal' => timestamp(),
		'id_user_fk' => $user['id'],
		'created_at' => timestamp(),
		'username' => $email,
		'password' => md6($password),
		'device' => device(),
		'status_login' =>  $status,
		'browser' => getClientBrowser(),
		'ip_address' => getHostByName(getHostName())
	];
	create('user_login_history',$dataLogin);
}

function device() {
		$device = is_numeric(strpos((strtolower($_SERVER['HTTP_USER_AGENT'])),'mobile'));
		if($device){
			$device = "MOBILE";
		}else{
			$device = "LAPTOP/PC";
		}

		return $device;
}


function getIpAddress(){

 
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'IP tidak dikenali';
    return $ipaddress;
}

function getClientBrowser() {
    $browser = '';
    if(strpos($_SERVER['HTTP_USER_AGENT'], 'Netscape'))
        $browser = 'Netscape';
    else if (strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox'))
        $browser = 'Firefox';
    else if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome'))
        $browser = 'Chrome';
    else if (strpos($_SERVER['HTTP_USER_AGENT'], 'Opera'))
        $browser = 'Opera';
    else if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
        $browser = 'Internet Explorer';
     else if (strpos($_SERVER['HTTP_USER_AGENT'], 'Edg'))
        $browser = 'Edge';
    else
        $browser = 'Other';
    return $browser;
}
 


function getIpAddress2(){

 
  $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'IP tidak dikenali';
    return $ipaddress;
}
 

function view($token)
{
	$ci = get_instance();
	$tokenLogin = $ci->session->userdata('token');
	if ($tokenLogin == $token) {
		$data = base64_encode(openssl_random_pseudo_bytes(32));
		return 'SUCCESS-';
	} else {
		$data = base64_encode(openssl_random_pseudo_bytes(32));
		return "FAILED-";
		// redirect('auth/AccessDenied');
	}
}
function security_view()
{
	$ci = get_instance();
	$session = $ci->session->userdata('token');
	// $session = '';
	if (empty($session)) {
		redirect('auth/AccessDenied');
	}
}



function check_gajian($id, $post)
{

	$ci = get_instance();
	$ci->db->select('*');
	$ci->db->from('user_gajian');
	$ci->db->where('id_user_gajian', $id);
	$ci->db->where('post', $post);
	$result = $ci->db->get()->row_array();
	if ($result['post'] == 1) {
		return "checked";
	} else {
		return "";
	}
}

function check_gajian_percetakan($id, $post)
{

	$ci = get_instance();
	$ci->db->select('*');
	$ci->db->from('user_gajian_percetakan');
	$ci->db->where('id_user_gajian_percetakan', $id);
	$ci->db->where('post', $post);
	$result = $ci->db->get()->row_array();
	if ($result['post'] == 1) {
		return "checked";
	} else {
		return "";
	}
}

function encrypt_decrypt($action, $string)
{
	$output = false;
	$encrypt_method = "AES-256-CBC";
	$secret_key = 'ini adalah my secret key';
	$secret_iv = 'ini adalah my secret iv';
	// hash
	$key = hash('sha256', $secret_key);

	// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	$iv = substr(hash('sha256', $secret_iv), 0, 16);
	if ($action == 'encrypt') {
		$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
		$output = base64_encode($output);
	} else if ($action == 'decrypt') {
		$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	}
	return $output;
}
function md6($string = null){
	$output = false;
	$encrypt_method = "AES-256-CBC";
	$secret_key = 'ini adalah my secret key';
	$secret_iv = 'ini adalah my secret iv';
	$key = hash('sha256', $secret_key);
	$iv = substr(hash('sha256', $secret_iv), 0, 16);
	$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
	return base64_encode($output);
}
function md6_open($string = null){
	$output = false;
	$encrypt_method = "AES-256-CBC";
	$secret_key = 'ini adalah my secret key';
	$secret_iv = 'ini adalah my secret iv';
	
	$key = hash('sha256', $secret_key);
	$iv = substr(hash('sha256', $secret_iv), 0, 16);
	return openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
}


function url($data = null)
{
	$ci = get_instance();
	if ($data == 1) {
		$url = $ci->uri->segment(1);
	} else if ($data == 2) {
		$url = $ci->uri->segment(1) . '/' . $ci->uri->segment(2);
	} else if ($data == 3) {
		$url = $ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/' . $ci->uri->segment(3);
	} else if ($data == 'first') {
		$url = $ci->uri->segment(1);
	} else if ($data == 'middle') {
		$url = $ci->uri->segment(2);
	} else if ($data == 'last') {
		$url = $ci->uri->segment(3);
	} else {
		if(!empty($ci->uri->segment(3))){

			 $query = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
			 if(!empty($query)){
			 	$url = $ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/' . $ci->uri->segment(3).'?'.$query;
			 }else{
			 	$url = $ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/' . $ci->uri->segment(3);
			 }
			
		}else{
			 $query = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
			 if(!empty($query)){
			 	$url = $ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/'.'?'.$query;
			 }else{
			 	$url = $ci->uri->segment(1) . '/' . $ci->uri->segment(2) ;
			 }
		}

		
	}
	return $url;
}

function urlById($data = null)
{
	$ci = get_instance();
	if ($data == 1) {
		$url = $ci->uri->segment(1);
	} else if ($data == 2) {
		$url = $ci->uri->segment(1) . '/' . $ci->uri->segment(2);
	} else if ($data == 3) {
		$url = $ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/' . $ci->uri->segment(3);
	} else if ($data == 'first') {
		$url = $ci->uri->segment(1);
	} else if ($data == 'middle') {
		$url = $ci->uri->segment(2);
	} else if ($data == 'last') {
		$url = $ci->uri->segment(3);
	} else {
		$url = $ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/' . $ci->uri->segment(3) . '/' . $ci->uri->segment(4);
	}
	return $url;
}

function clearSessionOld()
{
	$ci = get_instance();
	$controller = $ci->session->userdata('controller');
	$page = $ci->session->userdata('page');

	// dd($ci->uri->segment(2));
	// dd($controller);

	if ($controller != $ci->uri->segment(1) ) {
		 $ci->session->unset_userdata('autoFilter');
			$ci->session->unset_userdata('urutan');
			$ci->session->unset_userdata('sortBy');
			$ci->session->unset_userdata('keyword');
			$ci->session->unset_userdata('bulan');
			$ci->session->unset_userdata('tahun');
			// $ci->session->unset_userdata('tanggal_awal');
			// $ci->session->unset_userdata('tanggal_akhir');
			$ci->session->unset_userdata('tahun');
			$ci->session->unset_userdata('show');
		
	}else{
		if( $page != $ci->uri->segment(2)){
			$ci->session->unset_userdata('autoFilter');
			$ci->session->unset_userdata('urutan');
			$ci->session->unset_userdata('sortBy');
			$ci->session->unset_userdata('keyword');
			$ci->session->unset_userdata('bulan');
			$ci->session->unset_userdata('tahun');
			// $ci->session->unset_userdata('tanggal_awal');
			// $ci->session->unset_userdata('tanggal_akhir');
			$ci->session->unset_userdata('tahun');
			// $ci->session->unset_userdata('show');
		}
	}
}

function clearSessionSp()
{
	$ci = get_instance();
	$controller = $ci->session->userdata('controller');
	$page = $ci->session->userdata('page');

	// dd($ci->uri->segment(2));
	// dd($controller);
	$segment1 = $ci->uri->segment(1);
	$segment2 =  $ci->uri->segment(2);


	if ($controller != $segment1 ) {
		 $ci->session->unset_userdata('autoFilter');
			$ci->session->unset_userdata('urutan');
			$ci->session->unset_userdata('sortBy');
			$ci->session->unset_userdata('keyword');
			$ci->session->unset_userdata('bulan');
			$ci->session->unset_userdata('tahun');
			$ci->session->unset_userdata('tanggal_awal');
			$ci->session->unset_userdata('tanggal_akhir');
			$ci->session->unset_userdata('tahun');
			$ci->session->unset_userdata('show');
		
	}else{

		if( $page != $segment2 ){

			if($segment2 == "listProduksi" || $segment2 == "listAllProduksi" || $segment2 == "listProduksiOfficeAndBantul"){
			$ci->session->unset_userdata('autoFilter');
			$ci->session->unset_userdata('urutan');
			$ci->session->unset_userdata('sortBy');
			$ci->session->unset_userdata('keyword');
			$ci->session->unset_userdata('bulan');
			$ci->session->unset_userdata('tahun');
			$ci->session->unset_userdata('tanggal_awal');
			$ci->session->unset_userdata('tanggal_akhir');
			$ci->session->unset_userdata('tahun');
			// $ci->session->unset_userdata('show');
			}
		}
	}
}



function clearSession()
{
	$ci = get_instance();
	$controller = $ci->session->userdata('controller');
	$page = $ci->session->userdata('page');

	
	$segment1 = $ci->uri->segment(1);
	$segment2 =  $ci->uri->segment(2);


	if ($controller != $segment1 ) {
		 $ci->session->unset_userdata('autoFilter');
			$ci->session->unset_userdata('urutan');
			$ci->session->unset_userdata('sortBy');
			$ci->session->unset_userdata('keyword');
			$ci->session->unset_userdata('bulan');
			$ci->session->unset_userdata('tahun');
			$ci->session->unset_userdata('tanggal_awal');
			$ci->session->unset_userdata('tanggal_akhir');
			$ci->session->unset_userdata('tahun');
			$ci->session->unset_userdata('show');
		
	}else{

			if($page != $segment2 ){

			if(preg_match('/create/', strtolower($segment2))){

			}else if(preg_match('/edit/', strtolower($segment2))){

			}else if(preg_match('/delete/', strtolower($segment2))){
			}else if(preg_match('/hapus/', strtolower($segment2))){
			}else if(preg_match('/tambah/', strtolower($segment2))){
			}else if(preg_match('/ubah/', strtolower($segment2))){
			}else if(preg_match('/update/', strtolower($segment2))){
			
			

			}else{
				$ci->session->unset_userdata('autoFilter');
				$ci->session->unset_userdata('urutan');
				$ci->session->unset_userdata('sortBy');
				$ci->session->unset_userdata('keyword');
				$ci->session->unset_userdata('bulan');
				$ci->session->unset_userdata('tahun');
				$ci->session->unset_userdata('tanggal_awal');
				$ci->session->unset_userdata('tanggal_akhir');
				$ci->session->unset_userdata('show');
			}
				


			
		}
	}
}







function notification_succes($data =  null, $more = null)
{

	$ci = get_instance();
	if ($more == null) {
		$koma = '';
	} else {
		$koma = ',';
	}
	$ci->session->set_flashdata('message', '<div class=" alert alert-success alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>Data <strong> Berhasil</strong> ' . $data . '!' . $koma . ' ' . $more . '</div>  ');
}
function notification_fail($data =  null, $more = null)
{

	$ci = get_instance();
	if ($more == null) {
		$koma = '';
	} else {
		$koma = ',';
	}
	$ci->session->set_flashdata('message', '<div class=" alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>Data <strong>Gagal </strong>' . $data . '!' . $koma . ' ' . $more . '</div>  ');
}

function notification_fail_image($data =  null, $error = null)
{

	$ci = get_instance();
	if ($error == -1) {
		$more = 'File gambar harus di isi';
	} else if ($error == -2) {
		$more = 'Ektensi file harus jpg/jpeg/png';
	} else if ($error == -3) {
		$more = 'Ukuran file terlalu besar';
	} else if ($error == -4) {
		$more = 'Ukuran file terlalu kecil';
	}

	$ci->session->set_flashdata('message', '<div class=" alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>Data <strong>Gagal </strong>' . $data . '!, ' . $more . '</div>  ');
}
function notification_warning($data =  null, $more = null)
{

	$ci = get_instance();
	if ($more == null) {
		$koma = '';
	} else {
		$koma = ',';
	}
	$ci->session->set_flashdata('message', '<div class=" alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>Data <strong>Gagal </strong>' . $data . '!' . $koma . ' ' . $more . '</div>  ');
}



function money($data)
{
	return number_format($data, 0, '.', '.');
}


function uploadImage($data = null)
{
	$namaFile = $_FILES[$data['name']]['name'];
	$ukuranFile = $_FILES[$data['name']]['size'];
	$error = $_FILES[$data['name']]['error'];
	$tmpName = $_FILES[$data['name']]['tmp_name'];

	$ekstensiGambarValid = $data['ekstensi'];
	$ekstensiGambar = explode('.', $namaFile);
	$ekstensiGambar = strtolower(end($ekstensiGambar));


	// cek file 
	$emptyFile = $data['empty'];
	if ($emptyFile == TRUE) {
		$error = $_FILES[$data['name']]['error'];
		if ($error == 4) {
			return 'default.jpg';
		}
	} else {
		if ($error == 4) {
			return -1;
		}
	}

	//cek ekstensi
	if ($ekstensiGambarValid == NULL) {
		$ekstensiDefault = ['jpg', 'jpeg', 'png'];
		if (!in_array($ekstensiGambar, $ekstensiDefault)) {

			return -2;
		}
	} else {
		if (!in_array($ekstensiGambar, $ekstensiGambarValid)) {
			return -2;
		}
	}

	// cek ukuran
	$maxSize =  $data['max_size'];
	if ($maxSize != NULL) {
		$ukuranValidMax = $maxSize * 1000000;
		$sizeKb = $maxSize * 1000;

		if ($ukuranFile > $ukuranValidMax) {
			return -3;
		}
	}

	$minSize =  $data['min_size'];
	if ($minSize != NULL) {
		$ukuranValidMin = $minSize * 1000000;
		$sizeKb = $minSize * 1000;

		if ($ukuranFile < $ukuranValidMin) {
			return -4;
		}
	}

	$firstName = $data['first_name'];
	if ($firstName != NULL) {
		$firstName = $firstName . '_';
	} else {
		$firstName = '';
	}


	$lastName  = $data['last_name'];
	if ($lastName != NULL) {
		$lastName = $lastName . '_';
	} else {
		$lastName = '';
	}


	$namaFileBaru = strtoupper($firstName . $lastName);
	$namaFileBaru .= uniqid();
	$namaFileBaru .= '.';
	$namaFileBaru .= $ekstensiGambar;

	$location  = $data['location'];
	move_uploaded_file($tmpName, '.' . $location . $namaFileBaru);
	return $namaFileBaru;
}

function drop_table($data)
{
	$ci = get_instance();
	$namaTabel = $data;
	$query = "DROP VIEW IF EXISTS `$data`";
	$ci->db->query($query);
}

function modal_delete($id = null, $method = null)
{
	if (empty($method)) {
		$method = 'delete';
	} else {
		$method = $method;
	}

	$data = '<div class="modal fade" id="delete' . $id . '">
		<div class="modal-dialog modal-md">
			<form role="form" action=' . base_url(url(1) . "/$method") . ' method="post">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Delete</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="text-align: center;">
				<img src="' . base_url("assets/icon/icon-delete.png") . '" alt="">
				<h4>Apakah anda ingin menghapus data ini ?</h4>
			</div>
			<div class="modal-footer justify-content-between">
				<input type="hidden" name="url" value=' . url() . '>
		<input type="hidden" name="id" value=' . $id . '>
		<button type="button" class="btn btn-primary" data-dismiss="modal" style="width: 70px;">Tidak</button>
		<button type="submit" class="btn btn-danger" style="width: 70px;">Iya</button>
		</div>

		</div>
		</form>
		</div>
		</div> ';
	return $data;
}

function upload_resize($path = null, $width = null, $height = null, $quality = null)
{


	$ci = get_instance();
	if ($path == null) {
		$path = './assets/img/';
	} else {
		$path = './'.$path;
	}
	if ($width == null) {
		$width = 800;
	} else {
		$width = $width;
	}

	if ($height == null) {
		$height = 800;
	} else {
		$height = $height;
	}
	if ($quality == null) {
		$quality = 95;
	} else {
		$quality = $quality;
	}
	
	$config['upload_path'] = $path; //path folder
	$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|webp'; //type yang dapat diakses bisa anda sesuaikan
	$config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

	$ci->upload->initialize($config);

	if (!empty($_FILES['image']['name'])) {

		if ($ci->upload->do_upload('image')) {
			$gbr = $ci->upload->data();

			//Compress Image
			$config['image_library'] = 'gd2';
			$config['source_image'] = $path . $gbr['file_name'];
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = FALSE;
			$config['quality'] = $quality . '%';
			$config['width'] = $width;
			$config['height'] = $height;
			$config['new_image'] = $path . $gbr['file_name'];
			$ci->load->library('image_lib', $config);
			$ci->image_lib->resize();

			$gambar = $gbr['file_name'];
			return $gambar;
		}
	} else {
		return FALSE;
	}
}

function upload_resize_produk()
{
	$ci = get_instance();


	$size = $ci->db->get_where('set_resize_image', ['id_set_resize_image' => 1])->row_array(); //id = 1, untuk resize foto di menu photografer
	$width = $size['width'];
	$height = $size['height'];
	$quality = $size['quality'];
	$path = './assets/img/foto_produk/';

	$config['upload_path'] = $path; //path folder
	$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|webp'; //type yang dapat diakses bisa anda sesuaikan
	$config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

	$ci->upload->initialize($config);

	if ($ci->upload->do_upload('image_paket')) {
		$gbr = $ci->upload->data();
		// var_dump($gbr);
		// // //Compress Image
		$config['image_library'] = 'gd2';
		$config['source_image'] = $path . $gbr['file_name'];
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = FALSE;
		$config['quality'] = $quality . '%';
		$config['width'] = $width;
		$config['height'] = $height;
		$config['new_image'] = $path . $gbr['file_name'];
		$ci->load->library('image_lib', $config);

		$ci->image_lib->resize();
		// var_dump($config);

		$gambar = $gbr['file_name'];

		return $gambar;
	}
}




function upload_resize_item()
{
	$ci = get_instance();


	$size = $ci->db->get_where('set_resize_image', ['id_set_resize_image' => 1])->row_array(); //id = 1, untuk resize foto di menu photografer
	$width = $size['width'];
	$height = $size['height'];
	$quality = $size['quality'];
	$path = './assets/img/foto_produk/';


	$config['upload_path'] = $path; //path folder
	$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|webp'; //type yang dapat diakses bisa anda sesuaikan
	$config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

	$ci->upload->initialize($config);

	if ($ci->upload->do_upload('image_item')) {
		$gbr = $ci->upload->data();
		//   var_dump($gbr);
		// //Compress Image
		$config['image_library'] = 'gd2';
		$config['source_image'] = $path . $gbr['file_name'];
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = FALSE;
		$config['quality'] = $quality . '%';
		$config['width'] = $width;
		$config['height'] = $height;
		$config['new_image'] = $path . $gbr['file_name'];
		$ci->load->library('image_lib', $config);

		$ci->image_lib->resize();
		// var_dump($config);

		$gambar = $gbr['file_name'];

		return $gambar;
	}
}

function uploadGambar($path = null)
{


	$ci = get_instance();
	if ($path == null) {
		$path = './assets/img/';
	} else {
		$path = './'.$path;
	}
	
	
	$config['upload_path'] = $path; //path folder
	$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|webp'; //type yang dapat diakses bisa anda sesuaikan
	$config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

	$ci->upload->initialize($config);

	if (!empty($_FILES['image']['name'])) {

		if ($ci->upload->do_upload('image')) {
			$gbr = $ci->upload->data();

			//Compress Image
			$config['image_library'] = 'gd2';
			$config['source_image'] = $path . $gbr['file_name'];
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = FALSE;
			
			
			$config['new_image'] = $path . $gbr['file_name'];
			$ci->load->library('image_lib', $config);
			$ci->image_lib->resize();

			$gambar = $gbr['file_name'];
			return $gambar;
		}
	} else {
		return FALSE;
	}
}

//Presensi
function jamNumber($jam = null)
{
	$cacah = explode(':', $jam);
	$result = ($cacah[0] . $cacah[1]) * 1;
	return  $result;
}

function formatTanggal($date = null)
{
	if($date  == null){
		$tanggal =  "-";
	}else{
		$tanggal = date('d-m-Y', strtotime($date));
	}
	return $tanggal;
	
}

function formatTanggalWaktu($date = null)
{
	if($date  == null){
		$tanggal =  "-";
	}else{
		$tanggal = date('d-m-Y', strtotime($date));
		$time =  date('H:i:s', strtotime($date));
		$tanggal = $tanggal.' '.$time;
	}	
	return $tanggal;
	
}

function bulanString()
{
	$bulan = [
		'01' => 'Januari',
		'02' => 'Februari',
		'03' => 'Maret',
		'04' => 'April',
		'05' => 'Mei',
		'06' => 'Juni',
		'07' => 'Juli',
		'08' => 'Agustus',
		'09' => 'September',
		'10' => 'Oktober',
		'11' => 'November',
		'12' => 'Desember',

	];
	return $bulan;
}

function bulanStringArray($bulan = null)
{

	$dataBulan = [
		'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember',

	];
	$bulan = $bulan-1;
	return $dataBulan[$bulan];
}

function formatTanggalString($date = null)
{
	if ($date == null) {
		$date = date('Y-m-d');
	} else {
		$date = $date;
	}

	$cacah = explode('-', $date);

	$tahun = $cacah[0];
	$tanggal = $cacah[2] * 1;
	$bulan = $cacah[1];

	$bulanString = bulanString();
	foreach ($bulanString as $key => $value) {
		if ($key == $bulan) {
			$bulan = $value;
		}
	}

	return $tanggal . ' ' . $bulan . ' ' . $tahun;
}

function nomorPenawaran()
{
}

function dd($variabel = null){
	var_dump($variabel);
	die();
}



function vd($variabel = null){
	var_dump($variabel);
	
}




function quote()
{
	$row = db_get_num('quote');
			$idRandom = rand(1,$row);
			$result = db_get_row('quote' ,$idRandom);
			if ($result != null) {
				$quote = $result['quote'];
			return $quote;
			} else{
				return "Semangat Bertumbuh dam Menumbuhkan";
			}	
}

function formatTime($time = '0:0'){
	$timeArray = explode(':', $time);
		$hour =  $timeArray[0];
		$lenghtHour = strlen($hour);
		if($lenghtHour == 1){
			$hour = '0'.$hour;
		}else{
			$hour = $hour;
		}


		$minute = $timeArray[1];
		$lenghtMinute = strlen($minute);
	

		if($lenghtMinute == 1){
			$minute = '0'.$minute;
		}else{
			$minute = $minute;
		}
		return $hour.':'.$minute;
}

function session($index){
	$ci = get_instance();
	return	$ci->session->userdata($index);
}

function set_session($index){
	$ci = get_instance();
	return	$ci->session->set_userdata($index);
}

function jumlahJam($array,$valuName){

	$count = count($array);
	for ($i=0; $i < $count; $i++) { 

		$cacah = explode(':',  $array[$i][$valuName]);
		$jam = $cacah[0];
		$menit = $cacah[1];
		$jamTotal[] = $jam;
		$menitTotal[] = $menit;
	}
	$totalJam = array_sum($jamTotal) + round(array_sum($menitTotal)/60);
	return $totalJam;
}

function uploadFile($path = null)
{
    $ci = get_instance();
    if ($path == null) {
        $path = './assets/document/';
    } else {
        $path = './'.$path;
    }
    
    $config['upload_path'] = $path; //path folder
    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|webp|pdf|docx|xlsx|doc|xls|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
    $config['encrypt_name'] = FALSE; //Enkripsi nama yang terupload
   
    $ci->upload->initialize($config);

    if (!empty($_FILES['file']['name'])) {
        if ($ci->upload->do_upload('file')) {
            $gbr = $ci->upload->data();
            $file = $gbr['file_name'];
            return $file;
        }
    } else {
        return FALSE;
    }
}

 function download($path = null, $file = null)
 {
        force_download($path.  $file,NULL);
 }

 function deadline($tanggal = null){
 	 $date1 = explode('-', date('Y-m-d'));
    $y = $date1[0];
    $m = $date1[1];
    $d = $date1[2];
    $tanggal = $tanggal;
    $date2 = explode('-', $tanggal);
    $y2 = $date2[0];
    $m2 = $date2[1];
    $d2 = $date2[2];
    $tgl_sekarang = gregoriantojd($m, $d, $y);
    $tgl_kirim = gregoriantojd($m2, $d2, $y2);
    $hari = "hari";
    $StatusOrder = 1;
    if ($tgl_kirim > $tgl_sekarang) {
        $selisih = $tgl_kirim - $tgl_sekarang;
        if ($StatusOrder == 0) {
            $color = 'success';
        } else if ($selisih == 1) {
            $color = 'danger';
        } else if ($selisih == 2) {
            $color = 'warning';
        } else if ($selisih == 3) {
            $color = 'success';
        } else {
            $color = 'info';
        }
        echo '<span class="m-badge  m-badge--' . $color . ' m-badge--wide"  style="width:35px;color:white">' . $selisih . '</span>';
    } else {
        $selisih = '-';

          echo '<span class="m-badge  m-badge--secondary m-badge--wide"  style="width:35px">' . $selisih . '</span>';
    }
 }

 function countup($tanggal = null){
 	$tgl1 = new DateTime($tanggal);
	$tgl2 = new DateTime();
	$jarak = $tgl2->diff($tgl1);
	return $jarak->y.'-'.$jarak->m .'-'.$jarak->d;
	
 }

 function dayByDate($date = null){

		$getDate = date('D', strtotime($date));
		$ambilHari = explode(',', $getDate);
		if ($ambilHari[0] == 'Mon') {
			$hari = "Senin";
		} else if ($ambilHari[0] == 'Tue') {
			$hari = "Selasa";
		} else if ($ambilHari[0] == 'Wed') {
			$hari = "Rabu";
		} else if ($ambilHari[0] == 'Thu') {
			$hari = "Kamis";
		} else if ($ambilHari[0] == 'Fri') {
			$hari = "Jum'at";
		} else if ($ambilHari[0] == 'Sat') {
			$hari = "Sabtu";
		} else if ($ambilHari[0] == 'Sun') {
			$hari = "Minggu";
		}else{
			$hari = "-";
		}
		return $hari;
 }
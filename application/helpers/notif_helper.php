<?php

function activeSidebar()
{
    $ci = get_instance();
    $data = [
        'notif' => 1
    ];
    $id = [4, 52, 362, 369, 328, 357, 302, 355, 325, 365, 322];
    db_where_in('id_user_sub_menu', $id);
    $ci->db->update('user_sub_menu', $data);
}


function notifInSidebar()
{

    $menuSubMenu = [
        'notif_value' => totalSubmenu()
    ];
    $idSbMenuSubMenu  = 4;
    update('user_sub_menu', $menuSubMenu, ['id_user_sub_menu' => $idSbMenuSubMenu]);


    // total cutting 

    $cuttingListCutting = [
        'notif_value' => totalListCutting()
    ];

    $idSbCuttingListCutting = 52;
    update('user_sub_menu', $cuttingListCutting, ['id_user_sub_menu' => $idSbCuttingListCutting]);


    //total cutting marketing project

    $cuttinglistCuttingMarketingProject = [
        'notif_value' => totalListCuttingMp()
    ];

    $idSbCuttingListCuttingMP = 362;
    update('user_sub_menu', $cuttinglistCuttingMarketingProject, ['id_user_sub_menu' => $idSbCuttingListCuttingMP]);

    //total cutting Spv Produksi

    $cuttinglistCuttingSpvProduksi = [
        'notif_value' => totalListCuttingProduksi()
    ];

    $idSbCuttingListCuttingProduksi = 369;
    update('user_sub_menu', $cuttinglistCuttingSpvProduksi, ['id_user_sub_menu' => $idSbCuttingListCuttingProduksi]);



    $kepalaPabriklistBelanja = [
        'notif_value' => totalListAprroveBelanja()
    ];

    $idSbkepalaPabriklistBelanja = 322;
    update('user_sub_menu', $kepalaPabriklistBelanja, ['id_user_sub_menu' => $idSbkepalaPabriklistBelanja]);


    $kepalaPabriklistBelanjaMP = [
        'notif_value' => totalListAprroveBelanjaMP()
    ];

    $idSbkepalaPabriklistBelanjaMP = 357;
    update('user_sub_menu', $kepalaPabriklistBelanjaMP, ['id_user_sub_menu' => $idSbkepalaPabriklistBelanjaMP]);


    $kepalaPabriklistProduksi = [
        'notif_value' => totalListProduksi()
    ];

    $idSbkepalaPabriklistProduksi = 302;
    update('user_sub_menu', $kepalaPabriklistProduksi, ['id_user_sub_menu' => $idSbkepalaPabriklistProduksi]);

    $kepalaPabriklistProduksiMP = [
        'notif_value' => totalListProduksiMP()
    ];

    $idSbkepalaPabriklistProduksiMP = 355;
    update('user_sub_menu', $kepalaPabriklistProduksiMP, ['id_user_sub_menu' => $idSbkepalaPabriklistProduksiMP]);


    $qcPabrikListJahitan = [
        'notif_value' => totalListQcPabrik()
    ];

    $idSbqcPabrik = 325;
    update('user_sub_menu', $qcPabrikListJahitan, ['id_user_sub_menu' => $idSbqcPabrik]);


    $qcPabrikListJahitanMP = [
        'notif_value' => totalListQcPabrikMP()
    ];

    $idSbqcPabrikMP = 365;
    update('user_sub_menu', $qcPabrikListJahitanMP, ['id_user_sub_menu' => $idSbqcPabrikMP]);


    $staffPurchasinglistBelanjaNew = [
        'notif_value' => totalListBelanjaActive()
    ];



    $idSbBelanjaNew = 138;
    update('user_sub_menu', $staffPurchasinglistBelanjaNew, ['id_user_sub_menu' => $idSbBelanjaNew]);




    $cuttinglistHitungKebutuhan = [
        'notif_value' => totalListHitungBahan()
    ];

    $idHitungBahan = 326;
    update('user_sub_menu', $cuttinglistHitungKebutuhan, ['id_user_sub_menu' => $idHitungBahan]);


    $cuttinglistHitungKebutuhanMp = [
        'notif_value' => totalListHitungBahanMp()
    ];

    $idHitungBahanMp = 356;
    update('user_sub_menu', $cuttinglistHitungKebutuhanMp, ['id_user_sub_menu' => $idHitungBahanMp]);

    
    $cuttinglistRequestCuttingSpv = [
        'notif_value' => totalListRequestCutting()
    ];

    $idRequestCuttingSpv = 377;
    update('user_sub_menu', $cuttinglistRequestCuttingSpv, ['id_user_sub_menu' => $idRequestCuttingSpv]);

    
    $cuttinglistRequestCuttingTeam = [
        'notif_value' => totalListRequestCuttingTeam()
    ];

    $idRequestCuttingTeam = 378;
    update('user_sub_menu', $cuttinglistRequestCuttingTeam, ['id_user_sub_menu' => $idRequestCuttingTeam]);

    
    
    $pengajuanCutiDev= [
        'notif_value' => totalListPengajuanDev()
    ];

    $idpengajuanCutiDev = 388 ;
    update('user_sub_menu', $pengajuanCutiDev, ['id_user_sub_menu' => $idpengajuanCutiDev]);
    
    

    
    $pengajuanCutiPro= [
        'notif_value' => totalListPengajuanPro()
    ];

    $idpengajuanCutiPro = 389;
    update('user_sub_menu', $pengajuanCutiPro, ['id_user_sub_menu' => $idpengajuanCutiPro]);
    
    $pengajuanCutiMar = [
        'notif_value' => totalListPengajuanMar()
    ];

    $idpengajuanCutiMar = 387;
    update('user_sub_menu', $pengajuanCutiMar, ['id_user_sub_menu' => $idpengajuanCutiMar]);
    
    
    $pengajuanCutiFin = [
        'notif_value' => totalListPengajuanFin()
    ];

    $idpengajuanCutiFin = 390;
    update('user_sub_menu', $pengajuanCutiFin, ['id_user_sub_menu' => $idpengajuanCutiFin]);
    
    $pengajuanCutiAll = [
        'notif_value' => totalListPengajuanAll()
    ];

    $idpengajuanCutiAll = 391;
    update('user_sub_menu', $pengajuanCutiAll, ['id_user_sub_menu' => $idpengajuanCutiAll]);
}





















function totalSubmenu()
{
    return db_get_num('user_menu');
}

function totalListCutting()
{
    $query = "SELECT COUNT(*) as jumlah_cutting FROM `order` WHERE tempat_produksi = 2 AND status in (20, 21, 24, 29)";
    $result = db_query_row($query);
    return $result['jumlah_cutting'];
}

function totalListCuttingMp()
{
    $query = "SELECT COUNT(*) as jumlah_cutting_mp FROM `marketing_project` WHERE tempat_produksi = 2  AND status = 6";
    $result = db_query_row($query);
    return $result['jumlah_cutting_mp'];
}


function totalListCuttingProduksi()
{
    $query = "SELECT COUNT(*) as jumlah_cutting_produksi FROM `request_cutting_produksi`";
    $result = db_query_row($query);
    return $result['jumlah_cutting_produksi'];
}

function totalListAprroveBelanja()
{
    $query = "SELECT COUNT(*) as jumlah_approve FROM belanja WHERE status = 0 AND status_approval IS NULL";
    $result = db_query_row($query);
    return $result['jumlah_approve'];
}

function totalListAprroveBelanjaMP()
{
    $query = "SELECT COUNT(*) as jumlah_approve_mp FROM belanja WHERE status = 0 AND kategori_belanja =  2 AND status_approval is null";
    $result = db_query_row($query);
    return $result['jumlah_approve_mp'];
}

function totalListProduksi()
{
    $query = "SELECT COUNT(*) as total_produksi FROM `order` WHERE `status` IN (5, 16, 18, 19, 20, 21, 22, 23, 24, 25, 26, 29, 30, 31, 32, 33, 34, 35, 37)
              AND tempat_produksi IN (2,3)";
    $result = db_query_row($query);
    return $result['total_produksi'];
}

function totalListProduksiMP()
{
    $query = "SELECT COUNT(*) as total_produksi_mp FROM `marketing_project` WHERE `status` NOT IN (9,12)
            AND tempat_produksi  = 2";
    $result = db_query_row($query);
    return $result['total_produksi_mp'];
}

function totalListQcPabrik()
{
    $query = "SELECT COUNT(*) as jumlah_qc FROM `order` WHERE tempat_produksi = 2 AND `status` IN (26,25)";
    $result = db_query_row($query);
    return $result['jumlah_qc'];
}

function totalListQcPabrikMP()
{
    $query = "SELECT COUNT(*) as jumlah_qc_mp FROM `marketing_project` WHERE tempat_produksi = 2 AND status IN  (16,17)";
    $result = db_query_row($query);
    return $result['jumlah_qc_mp'];
}


function totalListBelanjaActive()
{
    $query = "SELECT COUNT(*) as jumlah_belanja FROM belanja WHERE  `status` IN (0,1)";
    $result = db_query_row($query);
    return $result['jumlah_belanja'];
}

function totalListQcSubcon()
{
    $query = "SELECT COUNT(*) as jumlah_qc_subcon FROM qc_subcon WHERE  `status_qc`  IN (1)";
    $result = db_query_row($query);
    return $result['jumlah_qc_subcon'];
}
function totalListHitungBahan()
{
    $query = "SELECT COUNT(*) as total_order_bahan FROM `order` WHERE `status`  IN (33,34,29,24)  AND tempat_produksi  = 2";
    $result = db_query_row($query);
    return $result['total_order_bahan'];
}

function totalListHitungBahanMp()
{
    $query = "SELECT COUNT(*) as total_order_bahan_mp FROM `marketing_project` WHERE `status`  IN (2,6,13)  AND tempat_produksi  = 2";
    $result = db_query_row($query);
    return $result['total_order_bahan_mp'];
}
function totalListRequestCutting()
{
    $query = "SELECT COUNT(*) as total_request_cutting FROM request_cutting_produksi WHERE `status`  IN (1,2) ";
    $result = db_query_row($query);
    return $result['total_request_cutting'];
}
function totalListRequestCuttingTeam()
{
    $query = "SELECT COUNT(*) as total_request_cutting_team FROM request_cutting_produksi WHERE `status`  IN (1,2)  ";
    $result = db_query_row($query);
    return $result['total_request_cutting_team'];


    
}
function totalListPengajuanDev()
{
    $query = "SELECT COUNT(*) as total_cuti_divisi_dev FROM request_cuti WHERE is_divisi  IN (3)  AND `status`  = 1 ";
    $result = db_query_row($query);
    return $result['total_cuti_divisi_dev'];
}
function totalListPengajuanPro()
{
    $query = "SELECT COUNT(*) as total_cuti_divisi_pro FROM request_cuti WHERE is_divisi  IN (1) AND `status`  = 1 ";
    $result = db_query_row($query);
    return $result['total_cuti_divisi_pro'];
}
function totalListPengajuanMar()
{
    $query = "SELECT COUNT(*) as total_cuti_divisi_mar FROM request_cuti WHERE is_divisi  IN (2)  AND `status`  = 1 ";
    $result = db_query_row($query);
    return $result['total_cuti_divisi_mar'];
}
function totalListPengajuanFin()
{   
    $query = "SELECT COUNT(*) as total_cuti_divisi_fin FROM request_cuti WHERE is_divisi  IN (4)  AND `status`  = 1 ";
    $result = db_query_row($query);
    return $result['total_cuti_divisi_fin'];
}


function totalListPengajuanAll()
{   
    $query = "SELECT COUNT(*) as total_cuti_divisi_all FROM request_cuti WHERE `status` NOT IN  (2)";
    $result = db_query_row($query);
    return $result['total_cuti_divisi_all'];
}
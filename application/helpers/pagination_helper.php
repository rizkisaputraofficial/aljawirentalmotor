<?php


function pagination($url = null, $page = null, $row = null, $queryString = null)
{


    $ci = get_instance();
    $config['base_url'] = base_url() . $url;
    $config['total_rows'] = $row;
    $config['per_page'] = $page;
    $config['num_links'] = 2;

    if (!empty($queryString)) {
        $config['enable_query_strings'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['query_string_segment'] = 'page';
        $config['reuse_query_string'] = FALSE;
    }



    $config['full_tag_open'] = '<div class="dataTables_paginate paging_simple_numbers" >
                <ul class="pagination">';
    $config['full_tag_close'] = '</ul></div> ';
    $config['first_link'] = 'First';
    $config['first_tag_open'] = '  <li class="paginate_button page-item ">';
    $config['first_tag_close'] = '</li >';
    $config['last_link'] = 'Last';
    $config['last_tag_open'] = '  <li class="paginate_button page-item next" id="m_table_3_next">';
    $config['last_tag_close'] = '</li >';
    $config['next_link'] = '&raquo';
    $config['next_tag_open'] = '   <li id="m_table_3_next" class="paginate_button page-item ">';
    $config['next_tag_close'] = '</li >';
    $config['prev_link'] = '&laquo';
    $config['prev_tag_open'] = '<li id="previous" class="paginate_button page-item ">';
    $config['prev_tag_close'] = '</li >';

    $config['cur_tag_open'] = '<li class="paginate_button page-item active"><a href="#" aria-controls="m_table_3" data-dt-idx="3" tabindex="0" class="page-link">';
    $config['cur_tag_close'] = '</a></li >';

    $config['num_tag_open'] = '<li class="paginate_button page-item ">';
    $config['num_tag_close'] = '</li >';

    $config['attributes'] = array('class' => 'page-link');


    return  $ci->pagination->initialize($config);
}

function pagination_new($url, $page, $row)
{


    $ci = get_instance();
    $config['base_url'] = base_url() . $url;
    $config['total_rows'] = $row;
    $config['per_page'] = $page;
    $config['num_links'] = 2;
    $config['enable_query_strings'] = TRUE;
    $config['page_query_string'] = TRUE;
    $config['use_page_numbers'] = TRUE;
    $config['query_string_segment'] = 'page';
    $config['reuse_query_string'] = FALSE;
    $config['full_tag_open'] = '<div class="dataTables_paginate paging_simple_numbers" >
                    <ul class="pagination">';
    $config['full_tag_close'] = '</ul></div> ';
    $config['first_link'] = 'First';
    $config['first_tag_open'] = '  <li class="paginate_button page-item ">';
    $config['first_tag_close'] = '</li >';
    $config['last_link'] = 'Last';
    $config['last_tag_open'] = '  <li class="paginate_button page-item next" id="m_table_3_next">';
    $config['last_tag_close'] = '</li >';
    $config['next_link'] = '&raquo';
    $config['next_tag_open'] = '   <li id="m_table_3_next" class="paginate_button page-item ">';
    $config['next_tag_close'] = '</li >';
    $config['prev_link'] = '&laquo';
    $config['prev_tag_open'] = '<li id="previous" class="paginate_button page-item ">';
    $config['prev_tag_close'] = '</li >';

    $config['cur_tag_open'] = '<li class="paginate_button page-item active"><a href="#" aria-controls="m_table_3" data-dt-idx="3" tabindex="0" class="page-link">';
    $config['cur_tag_close'] = '</a></li >';

    $config['num_tag_open'] = '<li class="paginate_button page-item ">';
    $config['num_tag_close'] = '</li >';
    $config['attributes']['rel'] = FALSE;
    $config['attributes'] = array('class' => 'page-link');


    return  $ci->pagination->initialize($config);
}


function url_pagination()
{
    $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri_segments = explode('/', $uri_path);
    $url = $uri_segments[2] . '/' . $uri_segments[3] . '/';
    return $url;
}

function url_page_ById()
{
    $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri_segments = explode('/', $uri_path);
    $url = $uri_segments[2] . '/' . $uri_segments[3];
    return $url;
}

function url_page()
{
    // $ci = get_instance();
    // $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    // $uri_segments = explode('/', $uri_path);

    // if(empty($id)){
    //     $segment2 = $ci->uri->segment(2);

    //     if(!empty($segment2)){
    //         $ci->session->set_userdata(['page-id' => false]);
    //         $url = $uri_segments[2] . '/' . $uri_segments[3] ;
    //     }else{
    //           $ci->session->set_userdata(['page-id' => false]);
    //           $url = $uri_segments[2];
    //     }


    // }else{
    //     $ci->session->set_userdata(['page-id' => true]);
    //     $url = $uri_segments[2] . '/' . $uri_segments[3] . '/'. $uri_segments[4];
    // }


    $ci = get_instance();
    $segment3 =  $ci->uri->segment(3);
    if (empty($segment3)) {
        $url = $ci->uri->segment(1) . '/' . $ci->uri->segment(2);
    } else {
        $url = $ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/' . $ci->uri->segment(3);
    }
    return $url;
}

// function url_page_new()
// {      
//    $ci = get_instance();
//    $segment3 =  $ci->uri->segment(3);
//    if(empty($segment3)){
//      $url = $ci->uri->segment(1) . '/' .$ci->uri->segment(2) ;
//    }else{
//       $url =$ci->uri->segment(1) . '/' . $ci->uri->segment(2) . '/'. $ci->uri->segment(3);

//    }


//     return $url;


// }
// function url_page_id()
// {
//     $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
//     $uri_segments = explode('/', $uri_path);
//     $url = $uri_segments[2] . '/' . $uri_segments[3] . '/'. $uri_segments[4] .  '/';
//     return $url;
// }

// function url_pagination_byId()
// {
//     $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
//     $uri_segments = explode('/', $uri_path);
//     $url = $uri_segments[2] . '/' . $uri_segments[3] . '/'. $uri_segments[4] .  '/';
//     return $url;
// }
// function page_pagination()
// {
//     $ci = get_instance();
//     $search = $ci->input->post('keyword');
//     if ($search == null) {
//         $page = 10;
//     } else {
//         $page = 500;
//     }
//     return $page;
// }

function page()
{
    return session('limit');
}

function limit($limit = null)
{
    $ci = get_instance();
    if (!empty($limit)) {
        $limit = $limit;
    } else {
        $show = $ci->session->userdata('show');
        if (!empty($show)) {
            $limit =  $show;
        } else {
            $limit = 10;
        }
    }
    $ci->session->set_userdata(['limit' => $limit]);
    return $limit;
}

function offset()
{
    $ci = get_instance();
    $offset = html_escape($ci->input->get('page'));
    if (!empty($offset)) {
        $offset = ($offset - 1) * limit();
    } else {
        $offset = 0;
    }

    return $offset;
}


function row_pagination($table)
{
    $ci = get_instance();
    $limit = null;
    $start = null;
    $ci->db->select('*');
    $ci->db->from($table);
    $ci->db->limit($limit, $start);
    return $ci->db->get()->num_rows();
}


function start_pagination()
{
    $ci = get_instance();
    $ci->session->unset_userdata('limit');
    return $ci->uri->segment(3);
}


function start()
{
    $ci = get_instance();
    if (session('page-id') == true) {
        return $ci->uri->segment(4);
    } else {
        return $ci->uri->segment(3);
    }
}
// function start_new()
// {
//     $ci = get_instance();

//   $offset = html_escape($ci->input->get('page'));
//     if(!empty($offset)){
//         $start = ($offset-1) * page() ;
//     }else{

//         $start = 0;
//     }

//     return $start;
// }
// function start_id()
// {
//     $ci = get_instance();
//     return $ci->uri->segment(4);
// }

// function start_pagination_byId()
// {
//     $ci = get_instance();
//     return $ci->uri->segment(4);
// }

function begin()
{

    $ci = get_instance();


    $limit = session('limit');
    if (!empty($limit)) {
        $data = html_escape($ci->input->get('page'));
        if (!empty($data)) {
            $data =  (($data - 1) * page()) + 1;
        } else {
            if (empty(session('row-result'))) {
                $data = 0;
            } else {
                $data = 1;
            }
        }
    } else {
        if (session('page-id') == true) {
            $data = $ci->uri->segment(2);
            if (empty($data)) {
                $data = 1;
            } else {
                $data = $data + 1;
            }
        } else {
            $data = $ci->uri->segment(3);
            if (empty($data)) {
                $data = 1;
            } else {
                $data = $data + 1;
            }
        }
    }








    return $data;
}

function beginEnd()
{
    if (!empty(session('row-result'))) {
        $data = begin() + session('row-result') - 1;
    } else {
        $data = 0;
    }

    return $data;
}

function row()
{

    return session('row-data');
}

// function begin_new()
// {

//     $ci = get_instance();

//     $data = html_escape($ci->input->get('page'));
//     if(!empty($data)){
//           $data =  (($data-1) * page())+1;
//     }else{
//         $data = 1;    
//     } 

//     return $data;
// }

// function begin_id()
// {
//     $ci = get_instance();
//     $data = $ci->uri->segment(4);
//     if (empty($data)) {
//         $data = 1;
//     } else {
//         $data = $data+1;
//     }
//     return $data;
// }



function beginById()
{
    $ci = get_instance();
    $data = $ci->uri->segment(4);
    if (empty($data)) {
        $data = 1;
    } else {
        $data = $data + 1;
    }
    return $data;
}

function filterOrder($limit, $start, $type)
{
    $ci = get_instance();
    $tahun = $ci->input->post('tahun');
    $bulan = $ci->input->post('bulan');
    $sortBy = $ci->input->post('shortBy');
    $urutan = $ci->input->post('urutan');
    $keyword = $ci->input->post('keyword');
    if ($tahun == null) {
        if (empty($ci->session->userdata('tahun'))) {
            $tahun =  date('Y');
        } else {
            $tahun = $ci->session->userdata('tahun');
        }
    } else {
        $tahun = $tahun;
        $ci->session->set_userdata(['tahun' => $tahun]);
    }
    if ($bulan == null) {
        if (empty($ci->session->userdata('bulan'))) {
            $bulan =  date('m');
        } else {
            $bulan = $ci->session->userdata('bulan');
        }
    } else {
        $bulan = $bulan;
        $ci->session->set_userdata(['bulan' => $bulan]);
    }
    if ($sortBy == null && $keyword == null) {
        if (empty($ci->session->userdata('sortBy'))) {
            $sortBy =  "tgl_order";
        } else {
            $sortBy = $ci->session->userdata('sortBy');
        }
    } else if ($sortBy == null && $keyword != null) {
        if (empty($ci->session->userdata('sortBy'))) {
            $sortBy =  "nama_customer";
        } else {
            $sortBy = $ci->session->userdata('sortBy');
        }
    } else {
        $sortBy = $sortBy;
        $ci->session->set_userdata(['sortBy' => $sortBy]);
    }


    if ($urutan == null) {
        if (empty($ci->session->userdata('urutan'))) {
            $urut = "DESC";
        } else {
            $urut = $ci->session->userdata('urutan');
        }
    } else {
        $urut = $urutan;
        $ci->session->set_userdata(['urutan' => $urut]);
    }

    if ($keyword == null) {
        // if (empty($ci->session->userdata('keyword'))) {
        $keyword = "";
        // } else {
        //     $keyword = $ci->session->userdata('keyword');
        // }
    } else {

        $keyword = $keyword;
        // $ci->session->set_userdata(['keyword' => $keyword]);
    }

    $tgl = $tahun . '-' . $bulan . '-';

    $data = array(
        'tanggal' => $tgl,
        'urut' => $urut,
        'keyword' => $keyword,
        'sortBy' => $sortBy,
    );

    $ci->db->like('tgl_order', $data['tanggal']);
    $ci->db->like($data['sortBy'], $data['keyword']);
    $ci->db->order_by($data['sortBy'], $data['urut']);
    $ci->db->order_by('id_order', 'desc');
    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}

function yearBy($yearBy)
{
    $ci = get_instance();
    $tahun = $ci->input->post('tahun');
    $bulan = $ci->input->post('bulan');
    if ($tahun == null) {
        if (empty($ci->session->userdata('tahun'))) {
            $tahun =  date('Y');
        } else {
            $tahun = $ci->session->userdata('tahun');
        }
    } else {
        $tahun = $tahun;
        $ci->session->set_userdata(['tahun' => $tahun]);
    }
    if ($bulan == null) {
        if (empty($ci->session->userdata('bulan'))) {
            $bulan =  date('m');
        } else {
            $bulan = $ci->session->userdata('bulan');
        }
    } else {
        $bulan = $bulan;
        $ci->session->set_userdata(['bulan' => $bulan]);
    }
    $tanggal = $tahun . '-' . $bulan . '-';
    return $tanggal;
}

function sortBy($sortBy)
{
    $ci = get_instance();
    $urutan = $ci->input->post('urutan');

    if ($urutan == null) {
        if (empty($ci->session->userdata('urutan'))) {
            $urut = "DESC";
        } else {
            $urut = $ci->session->userdata('urutan');
        }
    } else {
        $urut = $urutan;
        $ci->session->set_userdata(['urutan' => $urut]);
    }
    $ci->db->order_by($sortBy, $urut);
}

function searchBy($searchBy)
{
    $ci = get_instance();
    $keyword = $ci->input->post('keyword');

    if ($keyword == null) {

        $keyword = "";
    } else {
        $keyword = $keyword;
    }
    $ci->db->like($searchBy, $keyword);
}
function searchByOr($searchBy)
{
    $ci = get_instance();
    $keyword = $ci->input->post('keyword');

    if ($keyword == null) {

        $keyword = "";
    } else {
        $keyword = $keyword;
    }
    $ci->db->or_like($searchBy, $keyword);
}


function getListTahunFromOrder()
{
    $ci = get_instance();
    $query = "SELECT left (tgl_order, 4)  as tahun FROM `order` group by tahun  ";
    return $ci->db->query($query)->result_array();
}

function countAllRowTable($table = NULL)
{

    $ci = get_instance();
    return $ci->db->get($table)->num_rows();
}

function clearSessionSearch($url = null)
{
    $ci = get_instance();
    $data = $ci->input->post('submit');
    if ($data == "clear") {
        $ci->session->unset_userdata('autoFilter');
        $ci->session->unset_userdata('urutan');
        $ci->session->unset_userdata('sortBy');
        $ci->session->unset_userdata('keyword');
        $ci->session->unset_userdata('bulan');
        $ci->session->unset_userdata('tahun');
        $ci->session->unset_userdata('show');
        $ci->session->unset_userdata('tanggal_awal');
        $ci->session->unset_userdata('tanggal_akhir');
        $ci->session->unset_userdata('account_debit');
        $ci->session->unset_userdata('account_kredit');

        redirect($url);
    }
}

function clearSessionNew()
{
    $ci = get_instance();
    $controller = $ci->session->userdata('controller');
    $page = $ci->session->userdata('page');


    $segment1 = $ci->uri->segment(1);
    $segment2 =  $ci->uri->segment(2);

    $data = $ci->session->userdata();
    var_dump($data);
    if (empty(session('sortBy'))) {
        echo "kosong";
    } else {
        echo "ada isi";
    }
    var_dump(session('sortBy'));
    if ($controller != $segment1) {
        $ci->session->unset_userdata('autoFilter');
        $ci->session->unset_userdata('urutan');
        $ci->session->unset_userdata('sortBy');
        $ci->session->unset_userdata('keyword');
        $ci->session->unset_userdata('bulan');
        $ci->session->unset_userdata('tahun');
        $ci->session->unset_userdata('tanggal_awal');
        $ci->session->unset_userdata('tanggal_akhir');
        $ci->session->unset_userdata('tahun');
        $ci->session->unset_userdata('show');
    } else {

        if ($page != $segment2) {

            if (preg_match('/create/', strtolower($segment2))) {
            } else if (preg_match('/edit/', strtolower($segment2))) {
            } else if (preg_match('/delete/', strtolower($segment2))) {
            } else if (preg_match('/hapus/', strtolower($segment2))) {
            } else if (preg_match('/tambah/', strtolower($segment2))) {
            } else if (preg_match('/ubah/', strtolower($segment2))) {
            } else if (preg_match('/update/', strtolower($segment2))) {
            } else {
                $ci->session->unset_userdata('autoFilter');
                $ci->session->unset_userdata('urutan');
                $ci->session->unset_userdata('sortBy');
                $ci->session->unset_userdata('keyword');
                $ci->session->unset_userdata('bulan');
                $ci->session->unset_userdata('tahun');
                $ci->session->unset_userdata('tanggal_awal');
                $ci->session->unset_userdata('tanggal_akhir');
                $ci->session->unset_userdata('show');
            }
        }
    }
}






function filterSortDesc()
{
    $ci = get_instance();
    $urutan = $ci->input->post('urutan');

    if ($urutan == null) {
        if (empty($ci->session->userdata('urutan'))) {
            $urut = "DESC";
        } else {
            $urut = $ci->session->userdata('urutan');
        }
    } else {
        $urut = $urutan;
        $ci->session->set_userdata(['urutan' => $urut]);
    }
    return $urut;
}

function filterSort()
{
    $ci = get_instance();
    $urutan = $ci->input->post('urutan');
    if ($urutan == null) {
        if (empty($ci->session->userdata('urutan'))) {
            $urut = "ASC";
        } else {
            $urut = $ci->session->userdata('urutan');
        }
    } else {
        $urut = $urutan;
        $ci->session->set_userdata(['urutan' => $urut]);
    }
    return $urut;
}
function filterSortAsc()
{
    $ci = get_instance();
    $urutan = $ci->input->post('urutan');

    if ($urutan == null) {
        if (empty($ci->session->userdata('urutan'))) {
            $urut = "ASC";
        } else {
            $urut = $ci->session->userdata('urutan');
        }
    } else {
        $urut = $urutan;
        $ci->session->set_userdata(['urutan' => $urut]);
    }
    return $urut;
}


// function filterKeyword(){
//     $ci = get_instance();

//     $keyword = $ci->input->post('keyword');
//     if ($keyword == null) {
//         if (empty($ci->session->userdata('keyword'))) {
//             $keyword = "";
//         } else {
//             $keyword = $ci->session->userdata('keyword');
//         }
//     } else {
//         $keyword = $keyword;
//         $ci->session->set_userdata(['keyword' => $keyword]);
//     }
//     return $keyword;
// }


function getDataTahunFromOrder()
{
    $ci = get_instance();
    $query = "SELECT YEAR(tgl_order)  as tahun FROM `order` group by tahun";
    return $ci->db->query($query)->result_array();
}
// filter for purchasing
function getTahunFromOrder()
{
    $ci = get_instance();
    $query = "SELECT left (tanggal, 4)  as tahun FROM purchase_order group by tahun  ";
    return $ci->db->query($query)->result_array();
}
function getDataTahunFromKunjungan()
{
    $ci = get_instance();
    $query = "SELECT left (tanggal_kunjungan, 4)  as tahun FROM customer_kunjungan group by tahun  ";
    return $ci->db->query($query)->result_array();
}
function getDataTahunFromOthersPermasalahan()
{
    $ci = get_instance();
    $query = "SELECT YEAR (tanggal)  as tahun FROM others_permasalahan group by tahun  ";
    return $ci->db->query($query)->result_array();
}

function getDataTahunFromInventaris()
{
    $ci = get_instance();
    $query = "SELECT YEAR (tanggal_beli)  as tahun FROM inventaris group by tahun";
    return $ci->db->query($query)->result_array();
}

function getDataTahunFromInventarisPeminjaman()
{
    $ci = get_instance();
    $query = "SELECT YEAR (tanggal_pinjam)  as tahun FROM inventaris_peminjaman group by tahun";
    return $ci->db->query($query)->result_array();
}

function getDataTahunFromListJobContentCreator()
{
    $ci = get_instance();
    $query = "SELECT YEAR (created_at)  as tahun FROM joblist_content_creator group by tahun";
    return $ci->db->query($query)->result_array();
}

function getDataTahunFromBelanja()
{
    $ci = get_instance();
    $query = "SELECT YEAR (created_at)  as tahun FROM belanja group by tahun";
    return $ci->db->query($query)->result_array();
}

function getDataTahunFromJahitan()
{
    $ci = get_instance();
    $query = "SELECT YEAR (tgl_jahit)  as tahun FROM jahitan group by tahun";
    return $ci->db->query($query)->result_array();
}

function getDataTahunFromPresensi()
{
    $ci = get_instance();
    $query = "SELECT YEAR (tanggal)  as tahun FROM user_detail_presensi group by tahun";
    return $ci->db->query($query)->result_array();
}
function getDataTahunFromJurnalKegiatan()
{
    $ci = get_instance();
    $query = "SELECT YEAR (tanggal)  as tahun FROM jk_content_creator group by tahun";
    return $ci->db->query($query)->result_array();
}
function getTahunFromPenawaran()
{
    $ci = get_instance();
    $query = "SELECT YEAR (tanggal_penawaran)  as tahun FROM penawaran_customer group by tahun";
    return $ci->db->query($query)->result_array();
}

function getTahunFromAnggaran()
{
    $ci = get_instance();
    $query = "SELECT YEAR (tanggal)  as tahun FROM anggaran group by tahun";
    return $ci->db->query($query)->result_array();
}






function filterSearchPurchase()
{


    $ci = get_instance();
    $sortBy = $ci->input->post('shortBy');

    $keyword = $ci->input->post('keyword');
    if ($sortBy == null && $keyword == null) {
        if (empty($ci->session->userdata('sortBy'))) {
            $sortBy =  "tanggal";
        } else {
            $sortBy = $ci->session->userdata('sortBy');
        }
    } else if ($sortBy == null && $keyword != null) {
        if (empty($ci->session->userdata('sortBy'))) {
            $sortBy =  "item.item";
        } else {
            $sortBy = $ci->session->userdata('sortBy');
        }
    } else {
        $sortBy = $sortBy;
        $ci->session->set_userdata(['sortBy' => $sortBy]);
    }

    return $sortBy;
}

function filterPurchase($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    $data = array(
        'tanggal' => filterDate(),
        'sortBy' => filterSort(),
        'keyword' => filterKeyword(),
        'searchBy' => filterSearchPurchase(),
    );
    $ci->db->like('tanggal', $data['tanggal']);
    $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}
function filterMenu($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    $data = array(

        'sortBy' => filterSort(),
        'keyword' => filterKeyword(),
        'searchBy' => filterSearch(),
    );

    $ci->db->like($data['searchBy'], $data['keyword']);
    $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}

function filterKeyword()
{
    $ci = get_instance();

    $keyword = $ci->input->post('keyword');
    if ($keyword == null) {
        if (empty($ci->session->userdata('keyword'))) {
            $keyword = "";
        } else {
            $keyword = $ci->session->userdata('keyword');
        }
    } else {
        $keyword = $keyword;
        $ci->session->set_userdata(['keyword' => $keyword]);
    }
    return $keyword;
}



function filterSearch()
{
    $ci = get_instance();
    $sortBy = $ci->input->post('shortBy');
    $keyword = $ci->input->post('keyword');

    if ($sortBy == null && $keyword == null) {
        if (empty($ci->session->userdata('sortBy'))) {
            $sortBy =  $ci->session->userdata('autoFilter');
        } else {
            $sortBy = $ci->session->userdata('sortBy');
        }
    } else if ($sortBy == null && $keyword != null) {
        if (empty($ci->session->userdata('sortBy'))) {
            $sortBy = $ci->session->userdata('autoFilter');
        } else {
            $sortBy = $ci->session->userdata('sortBy');
        }
    } else {
        $sortBy = $sortBy;
        $ci->session->set_userdata(['sortBy' => $sortBy]);
    }
    return $sortBy;
}

function filter($limit = null, $offset = null, $type = null, $date = null)
{

    $ci = get_instance();
    $keyword = inpos('keyword');
    $keywordSession = $ci->session->userdata('keyword');
    if (empty($keywordSession)) {
        $keyword = inpos('keyword');
        $keywordSession = $ci->session->set_userdata(['keyword' => $keyword]);
    }

    if (!empty($date)) {

        // var_dump($date);
        $keywordSession = $ci->session->userdata('keyword');

        if (!empty($keywordSession)) {

            if (empty($date)) {

                $data = array(

                    'sortBy' => filterSort(),
                    'keyword' => filterKeyword(),
                    'searchBy' => filterSearch(),
                );
                $ci->db->like($data['searchBy'], $data['keyword']);
                $ci->db->order_by($data['searchBy'], $data['sortBy']);
            } else {

                $data = array(
                    'tanggal' => filterDate(),
                    'sortBy' => filterSort(),
                    'keyword' => filterKeyword(),
                    'searchBy' => filterSearch(),
                );
                $ci->db->like($date, $data['tanggal']);
                $ci->db->like($data['searchBy'], $data['keyword']);
                $ci->db->order_by($date, 'DESC');
                $ci->db->order_by($data['searchBy'], $data['sortBy']);
            }
        } else {



            if (!empty($date)) {
                // var_dump('oke');

                $sessionSortBy = session('sortBy');

                if ($sessionSortBy   != '') {
                    $data = array(
                        'tanggal' => filterDate(),
                        'sortBy' => filterSort(),
                        'keyword' => filterKeyword(),
                        'searchBy' => filterSearch(),
                    );

                    $ci->db->like($date, $data['tanggal']);
                    $ci->db->order_by($sessionSortBy, $data['sortBy']);
                } else {

                    $data = array(
                        'tanggal' => filterDate(),
                        'sortBy' => filterSort(),
                        'keyword' => filterKeyword(),
                        'searchBy' => filterSearch(),
                    );

                    $ci->db->like($date, $data['tanggal']);
                    $ci->db->order_by($date, 'DESC');
                }
            }
        }
    } else {
        $keywordSession = session('keyword');
        $sortBySession = session('sortBy');
        $sortByAutoFillter = session('autoFilter');

        if (empty($sortBySession) &&  empty($sortByAutoFillter)) {
            $ci->session->unset_userdata('keyword');
        }

        if (!empty($keywordSession) &&  !empty($sortByAutoFillter)) {
            $data = array(
                'sortBy'    => filterSort(),
                'keyword'   => filterKeyword(),
                'searchBy'  => filterSearch(),
            );
            $ci->db->like($data['searchBy'], $data['keyword']);
            $ci->db->order_by($data['searchBy'], $data['sortBy']);
        } else if (!empty($keywordSession) &&  !empty($sortBySession)) {
            $data = array(
                'sortBy'    => filterSort(),
                'keyword'   => filterKeyword(),
                'searchBy'  => filterSearch(),
            );
            $ci->db->like($data['searchBy'], $data['keyword']);
            $ci->db->order_by($data['searchBy'], $data['sortBy']);
        }
    }

    if ($type == "get") {
        $ci->db->limit($limit, $offset);
    } else if ($type == "data") {
        $ci->db->limit($limit, $offset);
    }
}

// function filterByDate($limit = null, $start = null, $type = null, $tanggal = null)
// {

//     // $ci = get_instance();
//     // $keyword = inpos('keyword');
//     // $keywordSession = $ci->session->userdata('keyword');
//     // if (empty($keywordSession)) {
//     //     $keyword = inpos('keyword');
//     //     $keywordSession = $ci->session->set_userdata(['keyword' => $keyword]);
//     // }

//     $keywordSession = $ci->session->userdata('keyword');
//     if (!empty($keywordSession)) {

//         if (empty($tanggal)) {
//             $data = array(

//                 'sortBy' => filterSort(),
//                 'keyword' => filterKeyword(),
//                 'searchBy' => filterSearch(),
//             );
//             $ci->db->like($data['searchBy'], $data['keyword']);
//             $ci->db->order_by($data['searchBy'], $data['sortBy']);
//         } else {
//             $data = array(
//                 'tanggal' => filterDate(),
//                 'sortBy' => filterSort(),
//                 'keyword' => filterKeyword(),
//                 'searchBy' => filterSearch(),
//             );
//             $ci->db->like($tanggal, $data['tanggal']);
//             $ci->db->like($data['searchBy'], $data['keyword']);
//             $ci->db->order_by($tanggal, 'DESC');
//             $ci->db->order_by($data['searchBy'], $data['sortBy']);
//         }
//     }else{
//         if (!empty($tanggal)) {
//             $shortBy = inpos('shortBy');   
//             if($shortBy   != ''){
//                 $data = array(
//                         'tanggal' => filterDate(),
//                         'sortBy' => filterSort(),
//                         'keyword' => filterKeyword(),
//                         'searchBy' => filterSearch(),
//                     );
//                 $ci->db->like($tanggal, $data['tanggal']);
//                 $ci->db->order_by($shortBy, $data['sortBy']);
//             }else{
//                 $data = array(
//                         'tanggal' => filterDate(),
//                         'sortBy' => filterSort(),
//                         'keyword' => filterKeyword(),
//                         'searchBy' => filterSearch(),
//                     );
//                 $ci->db->like($tanggal, $data['tanggal']);
//                 $ci->db->order_by($tanggal, 'DESC');   
//             }
//         }
//     }

//     if ($type == "get") {
//         $ci->db->limit($limit, $start);
//     }
// }



function filterNew($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    $keyword = inpos('keyword');
    $keywordSession = $ci->session->userdata('keyword');

    if (empty($keywordSession)) {
        $keyword = inpos('keyword');
        $keywordSession = $ci->session->set_userdata(['keyword' => $keyword]);
    }
    $keywordSession = $ci->session->userdata('keyword');

    if (!empty($keywordSession)) {
        $data = array(
            'sortBy' => filterSort(),
            'keyword' => filterKeyword(),
            'searchBy' => filterSearch(),
        );
        $ci->db->like($data['searchBy'], $data['keyword']);
        $ci->db->order_by($data['searchBy'], $data['sortBy']);
    }
    if ($type == 'data') {
        $ci->db->limit($limit, $start);
    }
}

function filterBySerach($limit = null, $start = null, $type = null)
{

    $ci = get_instance();
    $keyword = inpos('keyword');
    $keywordSession = $ci->session->userdata('keyword');

    if (empty($keywordSession)) {
        $keyword = inpos('keyword');
        $keywordSession = $ci->session->set_userdata(['keyword' => $keyword]);
    }
    $keywordSession = $ci->session->userdata('keyword');

    if (!empty($keywordSession)) {
        $data = array(
            'sortBy' => filterSort(),
            'keyword' => filterKeyword(),
            'searchBy' => filterSearch(),
        );
        $ci->db->like($data['searchBy'], $data['keyword']);
        $ci->db->order_by($data['searchBy'], $data['sortBy']);
    }

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}

// filter
function filterDate()
{
    $ci = get_instance();
    $tahun = $ci->input->post('tahun');
    $bulan = $ci->input->post('bulan');
    if ($tahun == null) {
        if (empty($ci->session->userdata('tahun'))) {
            $tahun =  date('Y');
        } else {
            $tahun = $ci->session->userdata('tahun');
        }
    } else {
        $tahun = $tahun;
        $ci->session->set_userdata(['tahun' => $tahun]);
    }
    if ($bulan == null) {
        if (empty($ci->session->userdata('bulan'))) {
            $bulan =  date('m');
        } else {
            $bulan = $ci->session->userdata('bulan');
        }
    } else {
        $bulan = $bulan;
        $ci->session->set_userdata(['bulan' => $bulan]);
    }
    $tanggal = $tahun . '-' . $bulan . '-';
    return $tanggal;
}


function filterDateByRange($range = null)
{

    $ci = get_instance();
    $tahun = $ci->input->post('tahun');
    $bulan = $ci->input->post('bulan');
    $tanggalAwal = $ci->input->post('tanggal_awal');
    $tanggalAkhir = $ci->input->post('tanggal_akhir');




    if ($tahun == null) {
        if (empty($ci->session->userdata('tahun'))) {
            $tahun =  date('Y');
        } else {
            $tahun = $ci->session->userdata('tahun');
        }
    } else {
        $tahun = $tahun;
        $ci->session->set_userdata(['tahun' => $tahun]);
    }


    if ($bulan == null) {
        if (empty($ci->session->userdata('bulan'))) {
            $bulan =  date('m');
        } else {
            $bulan = $ci->session->userdata('bulan');
        }
    } else {
        $bulan = $bulan;
        $ci->session->set_userdata(['bulan' => $bulan]);
    }


    if ($range == 'tanggal_awal') {

        if ($tanggalAwal == null) {
            if (empty($ci->session->userdata('tanggal_awal'))) {
                $range =  '01';
            } else {
                $range = $ci->session->userdata('tanggal_awal');
            }
        } else {
            $range = $tanggalAwal;
            $ci->session->set_userdata(['tanggal_awal' => $tanggalAwal]);
        }
    } else if ($range = 'tanggal_akhir') {
        if ($range == null) {
            if (empty($ci->session->userdata('tanggal_akhir'))) {
                $range = '31';
            } else {
                $range = $ci->session->userdata('tanggal_akhir');
            }
        } else {
            $range = $tanggalAkhir;
            $ci->session->set_userdata(['tanggal_akhir' => $tanggalAkhir]);
        }
    }
    $tanggal = $tahun . '-' . $bulan . '-' . $range;
    return $tanggal;
}

function filterDateByYear()
{
    $ci = get_instance();
    $tahun = $ci->input->post('tahun');
    if ($tahun == null) {
        if (empty($ci->session->userdata('tahun'))) {
            $tahun =  date('Y');
        } else {
            $tahun = $ci->session->userdata('tahun');
        }
    } else {
        $tahun = $tahun;
        $ci->session->set_userdata(['tahun' => $tahun]);
    }


    return $tahun;
}


function filterDateByMonth()
{
    $ci = get_instance();
    $bulan = $ci->input->post('bulan');
    if ($bulan == null) {
        if (empty($ci->session->userdata('bulan'))) {
            $bulan =  date('m');
        } else {
            $bulan = $ci->session->userdata('bulan');
        }
    } else {
        $bulan = $bulan;
        $ci->session->set_userdata(['bulan' => $bulan]);
    }
    return $bulan;
}



function filterByDate($limit = null, $start = null, $type = null, $tanggal = null)
{





    $ci = get_instance();


    $keyword = inpos('keyword');
    $keywordSession = $ci->session->userdata('keyword');

    if (empty($keywordSession)) {
        $keyword = inpos('keyword');
        $keywordSession = $ci->session->set_userdata(['keyword' => $keyword]);
    }
    $keywordSession = $ci->session->userdata('keyword');






    if (!empty($keywordSession)) {

        if (empty($tanggal)) {
            $data = array(

                'sortBy' => filterSort(),
                'keyword' => filterKeyword(),
                'searchBy' => filterSearch(),
            );

            $ci->db->like($data['searchBy'], $data['keyword']);
            $ci->db->order_by($data['searchBy'], $data['sortBy']);
        } else {


            $data = array(
                'tanggal' => filterDate(),
                'sortBy' => filterSort(),
                'keyword' => filterKeyword(),
                'searchBy' => filterSearch(),
            );

            $ci->db->like($tanggal, $data['tanggal']);
            $ci->db->like($data['searchBy'], $data['keyword']);
            $ci->db->order_by($tanggal, 'DESC');
            $ci->db->order_by($data['searchBy'], $data['sortBy']);
        }
    } else {
        if (!empty($tanggal)) {


            $shortBy = inpos('shortBy');

            if ($shortBy   != '') {
                $data = array(
                    'tanggal' => filterDate(),
                    'sortBy' => filterSort(),
                    'keyword' => filterKeyword(),
                    'searchBy' => filterSearch(),
                );

                $ci->db->like($tanggal, $data['tanggal']);
                $ci->db->order_by($shortBy, $data['sortBy']);
            } else {
                $data = array(
                    'tanggal' => filterDate(),
                    'sortBy' => filterSort(),
                    'keyword' => filterKeyword(),
                    'searchBy' => filterSearch(),
                );
                $ci->db->like($tanggal, $data['tanggal']);
                $ci->db->order_by($tanggal, 'DESC');
            }
        }
    }

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}

function filterPenawaran($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    // $data = array(
    //     'tanggal' => filterDate(),
    //     'sortBy' => filterSort(),
    //     'keyword' => filterKeyword(),
    //     'searchBy' => filterSearchPurchase(),
    // );
    // $ci->db->like('tanggal', $data['tanggal']);
    // $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}
function filterRequest($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    // $data = array(
    //     'tanggal' => filterDate(),
    //     'sortBy' => filterSort(),
    //     'keyword' => filterKeyword(),
    //     'searchBy' => filterSearchPurchase(),
    // );
    // $ci->db->like('tanggal', $data['tanggal']);
    // $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}
function filterProduk($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    // $data = array(
    //     'tanggal' => filterDate(),
    //     'sortBy' => filterSort(),
    //     'keyword' => filterKeyword(),
    //     'searchBy' => filterSearchPurchase(),
    // );
    // $ci->db->like('tanggal', $data['tanggal']);
    // $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}
function filterPenjahit($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    // $data = array(
    //     'tanggal' => filterDate(),
    //     'sortBy' => filterSort(),
    //     'keyword' => filterKeyword(),
    //     'searchBy' => filterSearchPurchase(),
    // );
    // $ci->db->like('tanggal', $data['tanggal']);
    // $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}
function filterWarnaBahanBaku($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    // $data = array(
    //     'tanggal' => filterDate(),
    //     'sortBy' => filterSort(),
    //     'keyword' => filterKeyword(),
    //     'searchBy' => filterSearchPurchase(),
    // );
    // $ci->db->like('tanggal', $data['tanggal']);
    // $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}
function filterUser($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    // $data = array(
    //     'tanggal' => filterDate(),
    //     'sortBy' => filterSort(),
    //     'keyword' => filterKeyword(),
    //     'searchBy' => filterSearchPurchase(),
    // );
    // $ci->db->like('tanggal', $data['tanggal']);
    // $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}
function filterRole($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    // $data = array(
    //     'tanggal' => filterDate(),
    //     'sortBy' => filterSort(),
    //     'keyword' => filterKeyword(),
    //     'searchBy' => filterSearchPurchase(),
    // );
    // $ci->db->like('tanggal', $data['tanggal']);
    // $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}
function filterVersion($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    // $data = array(
    //     'tanggal' => filterDate(),
    //     'sortBy' => filterSort(),
    //     'keyword' => filterKeyword(),
    //     'searchBy' => filterSearchPurchase(),
    // );
    // $ci->db->like('tanggal', $data['tanggal']);
    // $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}
function filterDepartemen($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    // $data = array(
    //     'tanggal' => filterDate(),
    //     'sortBy' => filterSort(),
    //     'keyword' => filterKeyword(),
    //     'searchBy' => filterSearchPurchase(),
    // );
    // $ci->db->like('tanggal', $data['tanggal']);
    // $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}
function filterJabatan($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    // $data = array(
    //     'tanggal' => filterDate(),
    //     'sortBy' => filterSort(),
    //     'keyword' => filterKeyword(),
    //     'searchBy' => filterSearchPurchase(),
    // );
    // $ci->db->like('tanggal', $data['tanggal']);
    // $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}
function filterPekerjaan($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    // $data = array(
    //     'tanggal' => filterDate(),
    //     'sortBy' => filterSort(),
    //     'keyword' => filterKeyword(),
    //     'searchBy' => filterSearchPurchase(),
    // );
    // $ci->db->like('tanggal', $data['tanggal']);
    // $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}
function filterKaryawan($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    // $data = array(
    //     'tanggal' => filterDate(),
    //     'sortBy' => filterSort(),
    //     'keyword' => filterKeyword(),
    //     'searchBy' => filterSearchPurchase(),
    // );
    // $ci->db->like('tanggal', $data['tanggal']);
    // $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}
function filterStatusOrder($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    // $data = array(
    //     'tanggal' => filterDate(),
    //     'sortBy' => filterSort(),
    //     'keyword' => filterKeyword(),
    //     'searchBy' => filterSearchPurchase(),
    // );
    // $ci->db->like('tanggal', $data['tanggal']);
    // $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}
function filterProduksi($limit = null, $start = null, $type = null)
{
    $ci = get_instance();
    // $data = array(
    //     'tanggal' => filterDate(),
    //     'sortBy' => filterSort(),
    //     'keyword' => filterKeyword(),
    //     'searchBy' => filterSearchPurchase(),
    // );
    // $ci->db->like('tanggal', $data['tanggal']);
    // $ci->db->like($data['searchBy'], $data['keyword']);
    // $ci->db->order_by($data['searchBy'], $data['sortBy']);

    if ($type == "get") {
        $ci->db->limit($limit, $start);
    }
}

//end filter for purchasing

function getMonth()
{
    $ci = get_instance();

    return $ci->db->get('get_month')->result_array();
}




function bulanNama($bulan = null)
{

    if ($bulan == null) {
        $bulan = date('m');
    } else {
        $bulan = $bulan;
    }
    if ($bulan == '01') {
        $tanggal = 'Januari';
    } else if ($bulan == '02') {
        $tanggal = 'Februari';
    } else if ($bulan == '03') {
        $tanggal = 'Maret';
    } else if ($bulan == '04') {
        $tanggal = 'April';
    } else if ($bulan == '05') {
        $tanggal = 'Mei';
    } else if ($bulan == '06') {
        $tanggal = 'Juni';
    } else if ($bulan == '07') {
        $tanggal = 'Juli';
    } else if ($bulan == '08') {
        $tanggal = 'Agustus';
    } else if ($bulan == '09') {
        $tanggal = 'September';
    } else if ($bulan == '10') {
        $tanggal = 'Oktober';
    } else if ($bulan == '11') {
        $tanggal = 'November';
    } else if ($bulan == '12') {
        $tanggal = 'Desember';
    }
    return $tanggal;
}
function show()
{
    $ci = get_instance();
    $data = $ci->session->userdata('show');
    if (!empty($data)) {
        return $data;
    } else {
        return 10;
    }
}

function showData()
{
    $show = show();
    if ($show == 10) {
        echo '<div class="dataTables_length">
            <select name="data" aria-controls="m_table_3"  id="show-data" class="custom-select custom-select-sm form-control form-control-sm show-data">
                
                <option value="10" selected>10</option>
                <option value="25" >25</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
        </div>';
    } else if ($show == 25) {
        echo '<div class="dataTables_length">
            <select name="data" aria-controls="m_table_3"  id="show-data" class="custom-select custom-select-sm form-control form-control-sm show-data">
                
                <option value="10" >10</option>
                <option value="25" selected>25</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
        </div>';
    } else if ($show == 50) {
        echo '<div class="dataTables_length">
            <select name="data" aria-controls="m_table_3"  id="show-data" class="custom-select custom-select-sm form-control form-control-sm show-data">
                
                <option value="10" >10</option>
                <option value="25" >25</option>
                <option value="50" selected>50</option>
                <option value="100">100</option>
            </select>
        </div>';
    } else if ($show == 100) {
        echo '<div class="dataTables_length">
            <select name="data" aria-controls="m_table_3"  id="show-data" class="custom-select custom-select-sm form-control form-control-sm show-data">
                
                <option value="10" >10</option>
                <option value="25" >25</option>
                <option value="50" >50</option>
                <option value="100" selected>100</option>
            </select>
        </div>';
    } else {
        echo '<div class="dataTables_length">
            <select name="data" aria-controls="m_table_3"  id="show-data" class="custom-select custom-select-sm form-control form-control-sm show-data">
                
                <option value="10" selected>10</option>
                <option value="25" >25</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
        </div>';
    }
}


function filterAccountDebit()
{
    $ci = get_instance();

    $accountDebit = $ci->input->post('account_debit');
    if ($accountDebit == null) {
        if (empty($ci->session->userdata('account_debit'))) {
            $accountDebit = "";
        } else {
            $accountDebit = $ci->session->userdata('account_debit');
        }
    } else {
        $accountDebit = $accountDebit;
        $ci->session->set_userdata(['account_debit' => $accountDebit]);
    }
    return $accountDebit;
}

function filterAccountKredit()
{
    $ci = get_instance();

    $accountKredit = $ci->input->post('account_kredit');
    if ($accountKredit == null) {
        if (empty($ci->session->userdata('account_kredit'))) {
            $accountKredit = "";
        } else {
            $accountKredit = $ci->session->userdata('account_kredit');
        }
    } else {
        $accountKredit = $accountKredit;
        $ci->session->set_userdata(['account_kredit' => $accountKredit]);
    }
    return $accountKredit;





    function bulanStringArray($bulan = null)
    {

        $dataBulan = [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember',

        ];
        $bulan = $bulan - 1;
        return $dataBulan[$bulan];
    }

    function formatTanggalString($date = null)
    {
        if ($date == null) {
            $date = date('Y-m-d');
        } else {
            $date = $date;
        }

        $cacah = explode('-', $date);

        $tahun = $cacah[0];
        $tanggal = $cacah[2] * 1;
        $bulan = $cacah[1];

        $bulanString = bulanString();
        foreach ($bulanString as $key => $value) {
            if ($key == $bulan) {
                $bulan = $value;
            }
        }

        return $tanggal . ' ' . $bulan . ' ' . $tahun;
    }
}

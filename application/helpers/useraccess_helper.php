<?php

function  user_access_gaji()
{

    $id = userId();
    $userPermit = [1, 2, 3, 14, 27, 51, 55, 63];
    //1 Dewi
    //2 Riko
    //3 Nurmi
    //14 Detri 
    //27 Titik
    //51 Nita
    //55 Rani
    //63 Abel

    if (!in_array($id,   $userPermit)) {
        redirect('auth/AccessDenied');
    };
}

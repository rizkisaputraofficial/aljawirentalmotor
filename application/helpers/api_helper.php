<?php

function cloudflareAuth()
{
    return  db_get_row('api_cloudflare', 1);
}

function cfStatus()
{

    $auth = cloudflareAuth();
    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL => "https://api.cloudflare.com/client/v4/accounts/" . $auth['account_id'] . "/images/v1/stats",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => [
            "Authorization: Bearer undefined",
            'X-Auth-Email: ' . $auth['email'],
            'X-Auth-Key: ' . $auth['key']
        ],
    ]);

    $response = curl_exec($curl);
    $response = json_decode($response);

    return $response->success;
}
function cloudflareStatus()
{

    $auth = cloudflareAuth();
    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL => "https://api.cloudflare.com/client/v4/accounts/9b1085c803fb8cc9b7ad3bdc6d767eed/images/v1/stats",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => [
            "Authorization: Bearer undefined",
            "Content-Type: application/json",
            'X-Auth-Email: ' . $auth['email'],
            'X-Auth-Key: ' . $auth['key']
        ],
    ]);
    $response = curl_exec($curl);
    $response = json_decode($response);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        $status =  $response->success;
        if ($status == true) {
            $res = [
                'status' => 200,
                'message' => 'Cloudflare connected'
            ];
        } else {
            $res = [
                'status' =>  $response->errors[0]->code,
                'message' => $response->errors[0]->message
            ];
        }
    }
    return $res;
}


function uploadImageToCloudFlare($path = null, $type = null, $name = null, $size = null)
{

    $auth = cloudflareAuth();
    $curl = curl_init();
    $args =  new \CURLFile($path, $type, $name);
    $data = ['file' => $args];
    curl_setopt_array($curl, [
        CURLOPT_URL => "https://api.cloudflare.com/client/v4/accounts/" . $auth['account_id'] . "/images/v1",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => [
            "Authorization: Bearer undefined",
            'Content-Type: multipart/form-data',
            'boundary=---011000010111000001101001',
            'X-Auth-Email: ' . $auth['email'],
            'X-Auth-Key: ' . $auth['key']
        ],
    ]);
    $response = curl_exec($curl);
    $response = json_decode($response);

    $err = curl_error($curl);
    curl_close($curl);

    if (!empty($size)) {
        $size = $size;
    } else {
        $size = 'public';
    }



    $urlImage = 'https://imagedelivery.net/' .  $auth['account_hash'] . '/' . $response->result->id . '/' . $size;
    $data = [
        'id_image_cf' => $response->result->id,
        'name_image_cf' =>  $response->result->filename,
        'url_image_cf' =>  $urlImage,
    ];

    if ($err) {
        $res = [
            'status' => 'error',
            'message' => "cURL Error #: " . $err,
            'code' => 403,
        ];
    } else {
        $status =  $response->success;
        if ($status == true) {
            $res = [
                'status' => 'success',

                'message' => 'Upload image success',
                'data' => $data,
                'code' => 200,
            ];
        } else {
            $res = [
                'status' => 'error',
                'message' => $response->errors[0]->message,
                'code' => $response->errors[0]->code,
            ];
        }
    }



    return $res;
}


function createImageCDN($alias = null)
{

    $path = $_FILES['image']['tmp_name'];
    $type = $_FILES['image']['type'];
    $name =   name_image($_FILES['image']['name'],  $alias);
    $auth = cloudflareAuth();
    $curl = curl_init();
    $args =  new \CURLFile($path, $type, $name);
    $data = ['file' => $args];
    curl_setopt_array($curl, [
        CURLOPT_URL => "https://api.cloudflare.com/client/v4/accounts/" . $auth['account_id'] . "/images/v1",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => [
            "Authorization: Bearer undefined",
            'Content-Type: multipart/form-data',
            'boundary=---011000010111000001101001',
            'X-Auth-Email: ' . $auth['email'],
            'X-Auth-Key: ' . $auth['key']
        ],
    ]);
    $response = curl_exec($curl);
    $response = json_decode($response);
    $err = curl_error($curl);
    curl_close($curl);

    $urlImage = 'https://imagedelivery.net/' .  $auth['account_hash'] . '/' . $response->result->id . '/public';
    $data = [
        'id_image_cf' => $response->result->id,
        'name_image_cf' =>  $response->result->filename,
        'url_image_cf' =>  $urlImage,
    ];

    if ($err) {
        $res = [
            'status' => 'error',
            'message' => "cURL Error #: " . $err,
            'code' => 403,
        ];
    } else {
        $status =  $response->success;
        if ($status == true) {
            $res = [
                'status' => 'success',

                'message' => 'Update image success',
                'data' => $data,
                'code' => 200,
            ];
        } else {
            $res = [
                'status' => 'error',
                'message' => $response->errors[0]->message,
                'code' => $response->errors[0]->code,
            ];
        }
    }

    return $res;
}



function createImageCDNForOrder($alias = null)
{

    $path = $_FILES['image']['tmp_name'];
    $type = $_FILES['image']['type'];
    $name =   name_image($_FILES['image']['name'],  $alias);
    $auth = cloudflareAuth();
    $curl = curl_init();
    $args =  new \CURLFile($path, $type, $name);
    $data = ['file' => $args];
    curl_setopt_array($curl, [
        CURLOPT_URL => "https://api.cloudflare.com/client/v4/accounts/" . $auth['account_id'] . "/images/v1",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => [
            "Authorization: Bearer undefined",
            'Content-Type: multipart/form-data',
            'boundary=---011000010111000001101001',
            'X-Auth-Email: ' . $auth['email'],
            'X-Auth-Key: ' . $auth['key']
        ],
    ]);
    $response = curl_exec($curl);
    $response = json_decode($response);
    $err = curl_error($curl);
    curl_close($curl);

    $urlImage = 'https://imagedelivery.net/' .  $auth['account_hash'] . '/' . $response->result->id . '/mockup';
    $data = [
        'id_image_cf' => $response->result->id,
        'name_image_cf' =>  $response->result->filename,
        'url_image_cf' =>  $urlImage,
    ];

    if ($err) {
        $res = [
            'status' => 'error',
            'message' => "cURL Error #: " . $err,
            'code' => 403,
        ];
    } else {
        $status =  $response->success;
        if ($status == true) {
            $res = [
                'status' => 'success',

                'message' => 'Update image success',
                'data' => $data,
                'code' => 200,
            ];
        } else {
            $res = [
                'status' => 'error',
                'message' => $response->errors[0]->message,
                'code' => $response->errors[0]->code,
            ];
        }
    }

    return $res;
}

function deleteImageCDN($idImage = null)
{

    if ($idImage != null) {
        $idImage = $idImage;
    } else {
        // var_dump('aaaa');
        $idImage = inpos('id_image_cf');

        if ($idImage != null) {
            $idImage =  $idImage;
        } else {
            $idImage = inget('id_image_cf');
            if ($idImage != null) {
                $idImage =  $idImage;
            } else {
                return false;
            }
        }
    }
    $auth = cloudflareAuth();
    $curl = curl_init();

    $url = "https://api.cloudflare.com/client/v4/accounts/" . $auth['account_id'] . "/images/v1/" . $idImage;

    curl_setopt_array($curl, [
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "DELETE",

        CURLOPT_HTTPHEADER => [
            "Authorization: Bearer undefined",
            "Content-Type: application/json",
            'X-Auth-Email: ' . $auth['email'],
            'X-Auth-Key: ' . $auth['key']
        ],
    ]);
    $response = curl_exec($curl);
    $response = json_decode($response);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
        $res = [
            'status' => 'error',
            'message' => "cURL Error #: " . $err,
            'code' => 403,
        ];
    } else {
        $status =  $response->success;
        if ($status == true) {
            $res = [
                'status' => 'success',

                'message' => 'Delete image success',

                'code' => 200,
            ];
        } else {
            $res = [
                'status' => 'error',
                'message' => $response->errors[0]->message,
                'code' => $response->errors[0]->code,
            ];
        }
    }



    return $res;
}

function name_image($fileName = null, $nama = null)
{
    if ($nama == null) {
        $nama = 'IMAGE';
    } else {
        $nama = $nama;
    }

    $tanggalOrder = inpos('tanggal_order');
    if (!empty($tanggalOrder)) {
        $tanggal = substr($tanggalOrder, '2');
    } else {
        $tanggal = date('y-m-d');
    }
    $tanggal = preg_replace("/-/", "", $tanggal);
    $ektension = pathinfo($fileName, PATHINFO_EXTENSION);

    return strtoupper($nama) . '_' . $tanggal  . '_' . strtoupper(substr(uniqid(), 3)) . '.' . strtolower($ektension);
}



function uploadImageQrCodeInventaris($path = null, $type = null, $name = null, $size = null)
{

    $auth = cloudflareAuth();
    $curl = curl_init();
    $args =  new \CURLFile($path, $type, $name);
    $data = ['file' => $args];
    curl_setopt_array($curl, [
        CURLOPT_URL => "https://api.cloudflare.com/client/v4/accounts/" . $auth['account_id'] . "/images/v1",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => [
            "Authorization: Bearer undefined",
            'Content-Type: multipart/form-data',
            'boundary=---011000010111000001101001',
            'X-Auth-Email: ' . $auth['email'],
            'X-Auth-Key: ' . $auth['key']
        ],
    ]);
    $response = curl_exec($curl);
    $response = json_decode($response);

    $err = curl_error($curl);
    curl_close($curl);

    if (!empty($size)) {
        $size = $size;
    } else {
        $size = 'public';
    }



    $urlImage = 'https://imagedelivery.net/' .  $auth['account_hash'] . '/' . $response->result->id . '/' . $size;
    $data = [
        'id_qrcode_cf' => $response->result->id,
        'name_qrcode_cf' =>  $response->result->filename,
        'url_qrcode_cf' =>  $urlImage,
    ];

    if ($err) {
        $res = [
            'status' => 'error',
            'message' => "cURL Error #: " . $err,
            'code' => 403,
        ];
    } else {
        $status =  $response->success;
        if ($status == true) {
            $res = [
                'status' => 'success',

                'message' => 'Upload image success',
                'data' => $data,
                'code' => 200,
            ];
        } else {
            $res = [
                'status' => 'error',
                'message' => $response->errors[0]->message,
                'code' => $response->errors[0]->code,
            ];
        }
    }



    return $res;
}

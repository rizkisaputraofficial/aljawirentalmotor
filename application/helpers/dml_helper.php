<?php

use PhpOffice\PhpSpreadsheet\Calculation\DateTimeExcel\Month;

function statusOrderProduksiOffice()
{
	$ci = get_instance();
	$query = "SELECT id_order_status, nama_status, color FROM order_status WHERE id_order_status NOT IN (38,39) AND produksi_office IS NOT NULL
		ORDER BY produksi_office ASC";
	return $ci->db->query($query)->result_array();
}


function statusOrderProduksiOfficeById($status)
{
	$ci = get_instance();
	$query = "SELECT id_order_status, nama_status, color FROM order_status WHERE id_order_status = $status";
	return $ci->db->query($query)->row_array();
}
function statusOrderById($status)
{
	$ci = get_instance();
	$query = "SELECT id_order_status, nama_status, color FROM order_status WHERE id_order_status = $status";
	return $ci->db->query($query)->row_array();
}

function statusOrderByIdMarketingProject($status = null)
{
	$ci = get_instance();
	$query = "SELECT id_marketing_project_status, status, color FROM marketing_project_status WHERE id_marketing_project_status = $status";
	return $ci->db->query($query)->row_array();
}


// Menu My Order


function totalOmset()
{
	$ci = get_instance();
	$tanggal = date('Y-m');
	$idCustomerService = session('id');
	$query = "SELECT  SUM(jumlah * harga) AS total_omset FROM `order`
	WHERE tgl_order LIKE '%$tanggal%' AND `status` != 10 ";
	$result =  $ci->db->query($query)->row_array();
	return $result['total_omset'];
}

function totalPengunjung()
{
	$ci = get_instance();
	$tanggal = date('Y-m');
	$query = "SELECT  COUNT(DISTINCT(id_pengunjung_fk)) AS total  FROM pengunjung_kegiatan WHERE tanggal_kunjungan LIKE '%$tanggal%'";
	$result =  $ci->db->query($query)->row_array();
	return $result['total'];
}

function totalKegiatan()
{
	$ci = get_instance();
	$tanggal = date('Y-m');
	$query = "SELECT  COUNT(kegiatan) AS jumlah  FROM pengunjung_kegiatan WHERE tanggal_kunjungan LIKE '%$tanggal%'";
	$result =  $ci->db->query($query)->row_array();
	return $result['jumlah'];
}
function totalKegiatanHariIni()
{

	$ci = get_instance();
	$tanggal = date('Y-m-d');
	$query = "SELECT  COUNT(kegiatan) AS jumlah  FROM pengunjung_kegiatan WHERE tanggal_kunjungan LIKE '%$tanggal%'";
	$result =  $ci->db->query($query)->row_array();
	return $result['jumlah'];
}

function totalFollowUp()
{
	$ci = get_instance();
	$tanggal = date('Y-m');
	$query = "SELECT COUNT(follow_up) AS jumlah  FROM pengunjung WHERE follow_up = 1 ";
	$result =  $ci->db->query($query)->row_array();
	return $result['jumlah'];
}

function totalPengunjungBaru()
{
	$ci = get_instance();
	$tanggal = date('Y-m');
	$query = "SELECT
IF ((SELECT CONCAT(YEAR(created_at),MONTH(created_at)) FROM pengunjung WHERE id_pengunjung = id_pengunjung_fk
) = CONCAT(YEAR(tanggal_kunjungan),MONTH(tanggal_kunjungan)), 'Baru', 'Lama') AS status_kunjungan ,
COUNT(DISTINCT(id_pengunjung_fk)) AS jumlah
FROM pengunjung_kegiatan
WHERE tanggal_kunjungan LIKE '%$tanggal%'
GROUP BY status_kunjungan, MONTH(tanggal_kunjungan)
HAVING status_kunjungan = 'Baru'";
	$result =  $ci->db->query($query)->row_array();

	if (!empty($result['jumlah'])) {
		return $result['jumlah'];
	} else {
		return 0;
	}
}

function totalPengunjungLama()
{
	$ci = get_instance();
	$tanggal = date('Y-m');
	$query = "SELECT
IF ((SELECT CONCAT(YEAR(created_at),MONTH(created_at)) FROM pengunjung WHERE id_pengunjung = id_pengunjung_fk
) = CONCAT(YEAR(tanggal_kunjungan),MONTH(tanggal_kunjungan)), 'Baru', 'Lama') AS status_kunjungan ,
COUNT(DISTINCT(id_pengunjung_fk)) AS jumlah
FROM pengunjung_kegiatan
WHERE tanggal_kunjungan LIKE '%$tanggal%'
GROUP BY status_kunjungan, MONTH(tanggal_kunjungan)
HAVING status_kunjungan = 'Lama'";
	$result =  $ci->db->query($query)->row_array();

	if (!empty($result['jumlah'])) {
		return $result['jumlah'];
	} else {
		return 0;
	}
}



















function totalMyOmset()
{
	$ci = get_instance();
	$tanggal = date('Y-m');
	$idCustomerService = session('id');
	$query = "SELECT  SUM(jumlah * harga) AS total_omset FROM `order`
	WHERE tgl_order LIKE '%$tanggal%' AND `status` != 10 AND order_id_customer_service = $idCustomerService";
	$result =  $ci->db->query($query)->row_array();
	return $result['total_omset'];
}


function totalOrderHariIni()
{
	db_like('tgl_order', date('Y-m-d'));
	db_where_not_in('status', 10);
	db_group('order_id_order_invoice');
	return db_get_num('order');
}
function totalOrderBulanIni()
{
	db_like('tgl_order', date('Y-m'));
	db_where_not_in('status', 10);
	db_group('order_id_order_invoice');
	return db_get_num('order');
}

function totalMyOrderBulanIni()
{
	db_like('tgl_order', date('Y-m'));
	db_where_not_in('status', 10);
	db_where('order_id_customer_service', session('id'));
	db_group('order_id_order_invoice');
	return db_get_num('order');
}

function myOrderBulanIni()
{
	$idCustomerService = session("id");
	$tanggal = date('Y-m');
	$result = db_query_array(
		"SELECT * , (SELECT COUNT(*) FROM `order` p WHERE p.order_id_order_pembayaran = q.order_id_order_pembayaran  AND tgl_order LIKE '%$tanggal%' ) AS row_merge
		 FROM `order` q JOIN  customers
		 ON id_customer = order_id_customer JOIN customer_kategori
		 ON id_customer_kategori = id_customer_kategori_fk
		 WHERE tgl_order LIKE '%$tanggal%'   AND `status` != 10 and order_id_customer_service = '$idCustomerService'
		 ORDER BY tgl_order, id_order DESC
		"
	);

	return $result;
}
function orderBulanIni()
{

	$tanggal = date('Y-m');
	$result = db_query_array(
		"SELECT * , (SELECT COUNT(*) FROM `order` p WHERE p.order_id_order_pembayaran = q.order_id_order_pembayaran AND tgl_order LIKE '%$tanggal%' ) AS row_merge
		 FROM `order` q JOIN  customers
		 ON id_customer = order_id_customer JOIN customer_kategori
		 ON id_customer_kategori = id_customer_kategori_fk
		 WHERE tgl_order LIKE '%$tanggal%'   AND `status` != 10
		 ORDER BY tgl_order, id_order DESC
		"
	);

	return $result;
}



function totalDeadlineHariIni()
{

	$data = "	SELECT COUNT(*) deadline_today
		FROM `order` WHERE  DATEDIFF(tgl_kirim,CURDATE()) <= 0 AND `status` NOT IN(9,10)
 		";
	$result = db_query_row($data);

	if ($result == null) {
		$result = 0;
	} else {
		$result = $result['deadline_today'];
	}
	return $result;
}

function totalMyDeadlineHariIni()
{

	$idCustomerService = session('id');
	$data = "	SELECT COUNT(*) deadline_today
		FROM `order` WHERE  DATEDIFF(tgl_kirim,CURDATE()) <= 0 AND `status` NOT IN(9,10) AND order_id_customer_service =$idCustomerService 		";
	$result = db_query_row($data);

	if ($result == null) {
		$result = 0;
	} else {
		$result = $result['deadline_today'];
	}
	return $result;
}




function deadlineHariIni()
{

	$query = "SELECT * FROM `order` JOIN  customers
		 ON id_customer = order_id_customer JOIN customer_kategori
		 ON id_customer_kategori = id_customer_kategori_fk
		 WHERE  DATEDIFF(tgl_kirim,CURDATE()) <= 0 AND `status` NOT IN(9,10)
		 ORDER BY tgl_order, id_order DESC";

	return db_query_array($query);
}


function deadlineBesok()
{

	$query = "SELECT * FROM `order` JOIN  customers
		 ON id_customer = order_id_customer JOIN customer_kategori
		 ON id_customer_kategori = id_customer_kategori_fk
		 WHERE  DATEDIFF(tgl_kirim,CURDATE()) = 1 AND `status` NOT IN(9,10)
		 ORDER BY tgl_order, id_order DESC";

	return db_query_array($query);
}

function deadlinelusa()
{

	$query = "SELECT * FROM `order` JOIN  customers
		 ON id_customer = order_id_customer JOIN customer_kategori
		 ON id_customer_kategori = id_customer_kategori_fk
		 WHERE  DATEDIFF(tgl_kirim,CURDATE()) = 2 AND `status` NOT IN(9,10)
		 ORDER BY tgl_order, id_order DESC";

	return db_query_array($query);
}
function allDeadline()
{

	$query = "SELECT * FROM `order` JOIN  customers
		 ON id_customer = order_id_customer JOIN customer_kategori
		 ON id_customer_kategori = id_customer_kategori_fk
		 WHERE  DATEDIFF(tgl_kirim,CURDATE()) = 2 AND `status` NOT IN(9,10)
		 ORDER BY tgl_order, id_order DESC";

	return db_query_array($query);
}


function myDeadlineHariIni()
{
	$idCustomerService = session('id');
	$query = "SELECT *FROM `order` JOIN  customers
		 ON id_customer = order_id_customer JOIN customer_kategori
		 ON id_customer_kategori = id_customer_kategori_fk
		 WHERE  DATEDIFF(tgl_kirim,CURDATE()) <= 0 AND `status` NOT IN(9,10) AND order_id_customer_service = $idCustomerService
		 ORDER BY tgl_order, id_order DESC";

	return db_query_array($query);
}

// SUB MENU MU CUSTOMER SERVICE IN MENU MANAGER MARKETING




function totalMyOmsetById($id = null)
{
	$ci = get_instance();
	$tanggal = date('Y-m');
	$idCustomerService = $id;
	$query = "SELECT  SUM(jumlah * harga) AS total_omset FROM `order`
	WHERE tgl_order LIKE '%$tanggal%' AND `status` != 10 AND order_id_customer_service = $idCustomerService";
	$result =  $ci->db->query($query)->row_array();
	return $result['total_omset'];
}

function totalMyOrderBulanIniById($id = null)
{
	db_like('tgl_order', date('Y-m'));
	db_where_not_in('status', 10);
	db_where('order_id_customer_service', $id);
	db_group('order_id_order_invoice');
	return db_get_num('order');
}



function totalMyDeadlineHariIniById($id = null)
{

	$idCustomerService = $id;
	$data = "	SELECT COUNT(*) deadline_today
		FROM `order` WHERE  DATEDIFF(tgl_kirim,CURDATE()) <= 0 AND `status` NOT IN(9,10) AND order_id_customer_service =$idCustomerService 		";
	$result = db_query_row($data);

	if ($result == null) {
		$result = 0;
	} else {
		$result = $result['deadline_today'];
	}
	return $result;
}



function myOrderBulanIniById($id = null)
{
	$idCustomerService = $id;
	$tanggal = date('Y-m');
	$result = db_query_array(
		"SELECT * , (SELECT COUNT(*) FROM `order` p WHERE p.order_id_order_pembayaran = q.order_id_order_pembayaran  AND tgl_order LIKE '%$tanggal%' ) AS row_merge
		 FROM `order` q JOIN  customers
		 ON id_customer = order_id_customer JOIN customer_kategori
		 ON id_customer_kategori = id_customer_kategori_fk
		 WHERE tgl_order LIKE '%$tanggal%'   AND `status` != 10 and order_id_customer_service = '$idCustomerService'
		 ORDER BY tgl_order, id_order DESC
		"
	);

	return $result;
}



function myDeadlineHariIniById($id = null)
{
	$idCustomerService = $id;
	$query = "SELECT *FROM `order` JOIN  customers
		 ON id_customer = order_id_customer JOIN customer_kategori
		 ON id_customer_kategori = id_customer_kategori_fk
		 WHERE  DATEDIFF(tgl_kirim,CURDATE()) <= 0 AND `status` NOT IN(9,10) AND order_id_customer_service = $idCustomerService
		 ORDER BY tgl_order, id_order DESC";

	return db_query_array($query);
}


function totalDeadlineBesok()
{

	$data = "	SELECT COUNT(*) deadline_tomorrow
		FROM `order` WHERE  DATEDIFF(tgl_kirim,CURDATE()) = 1 AND `status` NOT IN(9,10)
 		";
	$result = db_query_row($data);

	if ($result == null) {
		$result = 0;
	} else {
		$result = $result['deadline_tomorrow'];
	}
	return $result;
}




function totalDeadlineLusa()
{

	$data = "	SELECT COUNT(*) deadline_after_tomorrow
		FROM `order` WHERE  DATEDIFF(tgl_kirim,CURDATE()) = 2 AND `status` NOT IN(9,10)
 		";
	$result = db_query_row($data);

	if ($result == null) {
		$result = 0;
	} else {
		$result = $result['deadline_after_tomorrow'];
	}
	return $result;
}

function totalSemuaDeadline()
{
	$query = "SELECT SUM(total_count) AS total
	FROM (
		SELECT COUNT(*) AS total_count
		FROM `order`
		WHERE DATEDIFF(tgl_kirim, CURDATE()) <= 0 AND `status` NOT IN (9, 10)

		UNION ALL

		SELECT COUNT(*) AS total_count
		FROM `order`
		WHERE DATEDIFF(tgl_kirim, CURDATE()) = 1 AND `status` NOT IN (9, 10)

		UNION ALL

		SELECT COUNT(*) AS total_count
		FROM `order`
		WHERE DATEDIFF(tgl_kirim, CURDATE()) = 2 AND `status` NOT IN (9, 10)
	) AS HASIL;";
	$result = db_query_row($query);
	return $result['total'];
}



function jumlahPenjahit()
{
	return db_get_num('penjahit');
}

function jumlahPenjahitAktif()
{
	db_where('is_active', 1);
	return db_get_num('penjahit');
}

function jumlahPenjahitTidakAktif()
{
	db_where('is_active', 0);
	return db_get_num('penjahit');
}

function PenjahitTidakAktif()
{
	db_where('is_active', 0);
	return db_get_array('penjahit');
}

function kodePenjahitNew()
{
	$ci = get_instance();
	$ci->db->select('kode_penjahit');
	$ci->db->from('penjahit');

	$result =  $ci->db->get()->last_row();
	if ($result != null) {
		$kode =  $result->kode_penjahit;
		if ($kode == NULL) {
			$kode = 3001;
		} else {
			$kode = $kode + 1;
		}
		return $kode;
	} else {
		return '3001';
	}
}


function jumlahKaryawanAktif()
{
	db_where('is_active', 1);
	return db_get_num('user');
}

function jumlahKaryawanTidakAktif()
{
	db_where('is_active', 0);
	return db_get_num('user');
}

function jumlahKaryawanOffice()
{
	db_where('is_active', 1);
	db_where('office', 1);
	return db_get_num('user');
}


function jumlahKaryawanWorkshop()
{
	db_where('is_active', 1);
	db_where('office', 3);
	return db_get_num('user');
}


function jumlahKaryawanRumahProduksi()
{
	db_where('is_active', 1);
	db_where('office', 2);
	return db_get_num('user');
}


// Halaman Dashboard


function jumlahHadir()
{
	db_where('kode_karyawan_user', session('kode_karyawan'));
	db_where('presensi', 1);
	db_like('tanggal', date('Y-m'));
	return db_get_num('user_detail_presensi');
}

function jumlahIzin()
{
	db_where('kode_karyawan_user', session('kode_karyawan'));
	db_where('presensi', 2);
	db_like('tanggal', date('Y-m'));
	return db_get_num('user_detail_presensi');
}

function jumlahSakit()
{
	db_where('kode_karyawan_user', session('kode_karyawan'));
	db_where('presensi', 3);
	db_like('tanggal', date('Y-m'));
	return db_get_num('user_detail_presensi');
}
function jumlahCuti()
{
	db_where('kode_karyawan_user', session('kode_karyawan'));
	db_where('presensi', 4);
	db_like('tanggal', date('Y-m'));
	return db_get_num('user_detail_presensi');
}

function jumlahTerlambat()
{

	db_where('kode_karyawan_user', session('kode_karyawan'));
	db_where('presensi', 1);
	db_where_not_in('late', '0:0');
	db_like('tanggal', date('Y-m'));
	return db_get_num('user_detail_presensi');


	db_where('kode_karyawan_user', session('kode_karyawan'));
	db_where('tanggal >', '2024-02-16');
	db_where('late_status', 1);
	return db_get_num('user_detail_presensi');
}

function jumlahSeluruhTerlambat()
{



	db_where('kode_karyawan_user', session('kode_karyawan'));
	db_where('tanggal >', '2024-02-16');
	db_where('late_status', 1);
	return db_get_num('user_detail_presensi');
}

function jumlahJamTerlambat()
{

	db_where('kode_karyawan_user', session('kode_karyawan'));
	db_where('presensi', 1);
	db_like('tanggal', date('Y-m'));
	$result = db_get_array('user_detail_presensi');
	if ($result == NULL) {
		$hasil = '0:0';
	} else {


		foreach ($result as $value) {
			if ($value['late'] != NULL) {
				$jamTelat = explode(':', $value['late']);
				$jam[] = $jamTelat[0];
				$menit[] =  $jamTelat[1];
			} else {
				$jam[] = 0;
				$menit[] = 0;
			}
		}
		$jumlahJam = (((array_sum($jam) * 60) + array_sum($menit)) / 60);
		$hasilJam = floor((((array_sum($jam) * 60) + array_sum($menit)) / 60));
		$sisa =  $jumlahJam - $hasilJam;
		$jumlahMenit = floor($sisa * 60);
		$hasil = $hasilJam . ':' . $jumlahMenit;
	}

	return $hasil;
}

// public function peringkatTerlambat(){

// }

function jumlahLembur()
{

	db_where('kode_karyawan_user', session('kode_karyawan'));
	db_where('presensi', 1);
	db_where_not_in('lembur', '0:0');
	db_where_not_in('lembur', '');
	db_like('tanggal', date('Y-m'));
	return db_get_num('user_detail_presensi');
}

function jumlahJamLembur()
{

	db_where('kode_karyawan_user', session('kode_karyawan'));
	db_where('presensi', 1);
	db_like('tanggal', date('Y-m'));
	$result = db_get_array('user_detail_presensi');
	if ($result == NULL) {
		$hasil = '0:0';
	} else {


		foreach ($result as $value) {
			if ($value['lembur'] != NULL) {
				$jamTelat = explode(':', $value['lembur']);
				$jam[] = $jamTelat[0];
				$menit[] =  $jamTelat[1];
			} else {
				$jam[] = 0;
				$menit[] = 0;
			}
		}
		$jumlahJam = (((array_sum($jam) * 60) + array_sum($menit)) / 60);
		$hasilJam = floor((((array_sum($jam) * 60) + array_sum($menit)) / 60));
		$sisa =  $jumlahJam - $hasilJam;
		$jumlahMenit = floor($sisa * 60);
		$hasil = $hasilJam . ':' . $jumlahMenit;
	}

	return $hasil;
}

// Informtion Box Customer

function totalCustomer()
{
	return db_query_num("SELECT * FROM customers");
}

function totalCustomerTahunIni()
{
	db_join('customers', 'id_customer = order_id_customer');
	db_like('tgl_order', date('Y'));
	db_group('order_id_customer');
	return db_get_num('order');
}


function totalNewCustomerTahunIni()
{
	$date = date('Y');
	// $date = '2022';
	$query = "SELECT id_customer,  nama_customer, keterangan_kategori, tgl_order, order_id_order_pembayaran,
		(SELECT COUNT(DISTINCT(order_id_order_pembayaran)) FROM `order` a JOIN `customers` b
		ON a.order_id_customer = b.id_customer
		WHERE b.id_customer = k.id_customer and  `status` != 10
		) AS repeat_year ,
		(SELECT COUNT(DISTINCT(order_id_order_pembayaran)) FROM `order` a JOIN `customers` b
		ON a.order_id_customer = b.id_customer
		WHERE b.id_customer = k.id_customer  AND YEAR(tgl_order) < $date
		) AS repeat_last_year
		FROM `order`  JOIN customers k
		ON order_id_customer = id_customer
		WHERE tgl_order LIKE '%$date%'
		GROUP BY  order_id_customer
		HAVING repeat_last_year = 0 and repeat_year  != 0
		ORDER BY order_id_customer

		";
	return db_query_num($query);
}

function totalNewCustomerTahunKemarin()
{
	$date = date('Y') - 1;
	// $date = '2022';
	$query = "SELECT id_customer,  nama_customer, keterangan_kategori, tgl_order, order_id_order_pembayaran,
		(SELECT COUNT(DISTINCT(order_id_order_pembayaran)) FROM `order` a JOIN `customers` b
		ON a.order_id_customer = b.id_customer
		WHERE b.id_customer = k.id_customer and  `status` != 10
		) AS repeat_year ,
		(SELECT COUNT(DISTINCT(order_id_order_pembayaran)) FROM `order` a JOIN `customers` b
		ON a.order_id_customer = b.id_customer
		WHERE b.id_customer = k.id_customer  AND YEAR(tgl_order) < $date
		) AS repeat_last_year
		FROM `order`  JOIN customers k
		ON order_id_customer = id_customer
		WHERE tgl_order LIKE '%$date%'
		GROUP BY  order_id_customer
		HAVING repeat_last_year = 0 and repeat_year  != 0
		ORDER BY order_id_customer

		";
	return db_query_num($query);
}


function totalCustomerBulanIni()
{
	db_join('customers', 'id_customer = order_id_customer');
	db_like('tgl_order', date('Y-m'));
	db_where('status !=', 10);
	db_group('order_id_customer');
	return db_get_num('order');
}


function totalNewCustomerBulanIni()
{
	$date = date('Y-m');
	$tanggal = date('Y-m') . "-01";

	// 	$query = "SELECT id_customer,  nama_customer, keterangan_kategori, tgl_order, order_id_order_pembayaran,
	// 		(SELECT COUNT(DISTINCT(order_id_order_pembayaran)) FROM `order` a JOIN `customers` b
	// 		ON a.order_id_customer = b.id_customer
	// 		WHERE b.id_customer = k.id_customer
	// 		 ) AS repeat_order
	// 		FROM `order`  JOIN customers k
	// 		ON order_id_customer = id_customer
	// 		WHERE tgl_order LIKE '%$date%'
	// 		GROUP BY  order_id_customer
	// 		HAVING repeat_order = 1
	// 		ORDER BY order_id_customer
	// ";


	$query = "SELECT id_customer,  nama_customer, keterangan_kategori, tgl_order, order_id_order_pembayaran,
			(SELECT COUNT(DISTINCT(order_id_order_pembayaran)) FROM `order` a JOIN `customers` b
					ON a.order_id_customer = b.id_customer
					WHERE b.id_customer = k.id_customer
					 ) AS repeat_year ,
			 (SELECT COUNT(DISTINCT(order_id_order_pembayaran)) FROM `order` a JOIN `customers` b
			ON a.order_id_customer = b.id_customer
			WHERE b.id_customer = k.id_customer  AND tgl_order < '$tanggal'
			 ) AS repeat_last_month
			FROM `order`  JOIN customers k
			ON order_id_customer = id_customer
			WHERE tgl_order LIKE '%$date%'
			GROUP BY  order_id_customer

			HAVING repeat_last_month = 0
			ORDER BY order_id_customer
		";
	return db_query_num($query);
}

// public function peringkatTerlambat(){

// }


// -------------- MENU PIUTANG KARYAWAN--------------------











function jumlahHutangMitra($idPiutangMitra = null)
{

	$query = "SELECT SUM(hutang) AS jumlah_hutang FROM piutang_mitra_history
		WHERE id_piutang_mitra_fk = $idPiutangMitra";
	$result = db_query_row($query);
	return $result['jumlah_hutang'];
}
function jumlahCicilanMitra($idPiutangMitra = null)
{

	$query = "SELECT SUM(cicilan) AS jumlah_cicilan FROM piutang_mitra_history
		WHERE id_piutang_mitra_fk = $idPiutangMitra";
	$result = db_query_row($query);
	return $result['jumlah_cicilan'];
}









function totalPiutangMitra()
{
	$query = "SELECT SUM(jumlah) AS total_piutang FROM piutang_mitra";
	$result = db_query_row($query);
	return $result['total_piutang'];
}


function jumlahMitraYangBerhutang()
{
	$query = "SELECT count(*) AS jumlah_mitra FROM piutang_mitra where status = 1";
	$result = db_query_row($query);
	return $result['jumlah_mitra'];
}




//UTANG SUPPLLIER


function jumlahUtangSupplier($idUtangSupplier = null)
{

	$query = "SELECT SUM(utang) AS jumlah_utang FROM utang_vendor_history
	WHERE id_utang_vendor_fk = $idUtangSupplier";
	$result = db_query_row($query);
	return $result['jumlah_utang'];
}
function jumlahCicilanSupplier($idUtangSupplier = null)
{

	$query = "SELECT SUM(cicilan) AS jumlah_cicilan FROM utang_vendor_history
		WHERE id_utang_vendor_fk = $idUtangSupplier";
	$result = db_query_row($query);
	return $result['jumlah_cicilan'];
}


function totalUtangSupplier()
{

	$query = "SELECT perusahaan, SUM(utang) - SUM(cicilan) as jumlah_utang FROM utang_vendor_history
	join utang_vendor ON id_utang_vendor = id_utang_vendor_fk
	join vendor on id_vendor = id_vendor_fk
	where kategori_vendor = 1";
	$result = db_query_row($query);
	return $result['jumlah_utang'];
}


function jumlahSupplierYangBerhutang()
{
	$query = "SELECT  perusahaan, SUM(utang) AS utang, SUM(cicilan) AS cicilan, SUM(utang) - SUM(cicilan) AS selisih FROM `utang_vendor_history`
	join utang_vendor ON id_utang_vendor = id_utang_vendor_fk
	join vendor on id_vendor = id_vendor_fk
	WHERE kategori_vendor = 1
	GROUP BY id_utang_vendor_fk
	HAVING selisih > 0";
	$result = db_query_num($query);
	return $result;
}




// END UTANG SUPPLLIER




// Piutang Karyawan//


function jumlahPiutangKaryawan($idPiutangKaryawan = null)
{

	$query = "SELECT SUM(hutang) AS jumlah_utang FROM piutang_karyawan_history
	WHERE id_piutang_karyawan_fk = $idPiutangKaryawan";
	$result = db_query_row($query);
	return $result['jumlah_utang'];
}
function jumlahCicilanKaryawan($idPiutangKaryawan = null)
{

	$query = "SELECT SUM(cicilan) AS jumlah_cicilan FROM piutang_karyawan_history
	WHERE id_piutang_karyawan_fk = $idPiutangKaryawan";
	$result = db_query_row($query);
	return $result['jumlah_cicilan'];
}


function totalUtangKaryawanOld()
{

	$query = "SELECT SUM(hutang) - SUM(cicilan) AS jumlah_utang FROM piutang_karyawan_history_old";
	$result = db_query_row($query);
	return $result['jumlah_utang'];
}

function jumlahKaryawanOldYangBerhutang()
{
	$query = "SELECT  SUM(hutang) AS utang, SUM(cicilan) AS cicilan, SUM(hutang) - SUM(cicilan) AS selisih FROM `piutang_karyawan_history_old`
	join piutang_karyawan_old ON id_piutang_karyawan_old = id_piutang_karyawan_old_fk
	GROUP BY id_piutang_karyawan_old_fk
	HAVING selisih > 0";
	$result = db_query_num($query);
	return $result;
}



// Piutang Karyawan//

function totalUtangKaryawan()
{

	$query = "SELECT SUM(hutang) - SUM(cicilan) AS jumlah_utang FROM piutang_karyawan_history";
	$result = db_query_row($query);
	return $result['jumlah_utang'];
}


function jumlahKaryawanYangBerhutang()
{
	$query = "SELECT  SUM(hutang) AS utang, SUM(cicilan) AS cicilan, SUM(hutang) - SUM(cicilan) AS selisih FROM `piutang_karyawan_history`
	join piutang_karyawan ON id_piutang_karyawan = id_piutang_karyawan_fk
	GROUP BY id_piutang_karyawan_fk
	HAVING selisih > 0";
	$result = db_query_num($query);
	return $result;
}
// Piutang Penjahit//

function totalUtangPenjahit()
{

	$query = "SELECT SUM(hutang) - SUM(cicilan) AS jumlah_utang FROM piutang_penjahit_history";
	$result = db_query_row($query);
	return $result['jumlah_utang'];
}


function jumlahPenjahitYangBerhutang()
{
	$query = "SELECT  SUM(hutang) AS utang, SUM(cicilan) AS cicilan, SUM(hutang) - SUM(cicilan) AS selisih FROM `piutang_penjahit_history`
	join piutang_penjahit ON id_piutang_penjahit = id_piutang_penjahit_fk
	GROUP BY id_piutang_penjahit_fk
	HAVING selisih > 0";
	$result = db_query_num($query);
	return $result;
}


// Piutang Luar Usaha//

function totalPiutangLuarUsaha()
{

	$query = "SELECT SUM(piutang) - SUM(cicilan) AS jumlah_utang FROM piutang_luar_usaha_history";
	$result = db_query_row($query);
	return $result['jumlah_utang'];
}



function jumlahOrangYangBerhutang()
{
	$query = "SELECT  SUM(piutang) AS utang, SUM(cicilan) AS cicilan, SUM(piutang) - SUM(cicilan) AS selisih FROM `piutang_luar_usaha_history`
	join piutang_luar_usaha ON id_piutang_luar_usaha = id_piutang_luar_usaha_fk
	 	GROUP BY id_piutang_luar_usaha_fk
	HAVING selisih > 0";
	$result = db_query_num($query);
	return $result;
}



function jumlahUtangMitra($idUtangMitra = null)
{

	$query = "SELECT SUM(utang) AS jumlah_utang FROM utang_vendor_history
	WHERE id_utang_vendor_fk = $idUtangMitra";
	$result = db_query_row($query);
	return $result['jumlah_utang'];
}


function jumlahCicilanMitraa($idUtangMitra = null)
{
	$query = "SELECT SUM(cicilan) AS jumlah_cicilan FROM utang_vendor_history
	WHERE id_utang_vendor_fk = $idUtangMitra";
	$result = db_query_row($query);
	return $result['jumlah_cicilan'];
}









function totalUtangMitra()
{

	// $query = "SELECT SUM(utang) - SUM(cicilan) AS jumlah_utang FROM utang_vendor_history";
	$query = "SELECT perusahaan, SUM(utang) - SUM(cicilan) as jumlah_utang FROM utang_vendor_history
	join utang_vendor ON id_utang_vendor = id_utang_vendor_fk
	join vendor on id_vendor = id_vendor_fk
	where kategori_vendor = 2";
	$result = db_query_row($query);
	return $result['jumlah_utang'];
}


function jumlahMitraYangUtang()
{
	$query = "SELECT  perusahaan, SUM(utang) AS utang, SUM(cicilan) AS cicilan, SUM(utang) - SUM(cicilan) AS selisih FROM `utang_vendor_history`
	join utang_vendor ON id_utang_vendor = id_utang_vendor_fk
	join vendor on id_vendor = id_vendor_fk
	WHERE kategori_vendor = 2
	GROUP BY id_utang_vendor_fk
	HAVING selisih > 0";
	$result = db_query_num($query);
	return $result;
}







function checkTotalHarga($idOrderPembayaran = null)
{
	$query = "SELECT  SUM(order.jumlah * order.harga)- total_harga AS selisih  FROM`order`
		JOIN order_pembayaran
		ON id_order_pembayaran = order_id_order_pembayaran
		WHERE order_id_order_pembayaran = $idOrderPembayaran AND `status` != 10
		GROUP BY order_id_order_pembayaran";

	$result = db_query_row($query);
	return $result['selisih'];
}

function checkPembayaranPiutangDagang($idOrderPembayaran = null)
{

	if ($idOrderPembayaran != null) {
		$query = "SELECT  SUM(order.jumlah * order.harga)- journal_umum.harga AS selisih FROM`order`
		JOIN order_pembayaran
		ON id_order_pembayaran = order_id_order_pembayaran  JOIN journal_umum
		ON id_order_pembayaran = journal_umum.id_order_pembayaran_fk
		WHERE order_id_order_pembayaran = $idOrderPembayaran  AND `status` != 10 AND account_kredit = 22
		GROUP BY order_id_order_pembayaran
	";
		$result = db_query_row($query);

		return $result['selisih'];
	}
}

function getTotalHargaByIdOrderPembayaran($idOrderPembayaran = null)
{
	if ($idOrderPembayaran != null) {
		$query = "SELECT SUM(jumlah*harga) AS total FROM `order` WHERE order_id_order_pembayaran = $idOrderPembayaran AND `status` != 10";
		$result = db_query_row($query);

		return $result['total'];
	} else {
		return 0;
	}
}

function namaDesigner($id = null)
{

	if ($id != 0) {
		$result = db_get_row('user', ['id' => $id]);
		$namaDesigner = $result['nama'];
	} else {
		$namaDesigner = "-";
	}

	return $namaDesigner;
}


function answerSurveyForTypeCheck($answer)
{
	$answer  = explode(',', $answer);
	var_dump($answer);
	$count 	 = count($answer);
	// var_dump($answer);
	for ($i = 0; $i < $count; $i++) {
		$id = $answer[$i];
		$answerQuisioner = db_get_row('survey_question_type', ['id_survey_question_type' => $id]);

		$jawaban[] = $answerQuisioner['keterangan'];
	}

	$jawaban = implode(', ', $jawaban);

	return $jawaban;
}


function answerQusetionTypeSelect($idQusetion, $type, $answer)
{
	$query = "SELECT * FROM survey_answer JOIN survey_question
ON id_survey_question = id_survey_question_fk
WHERE `type` ='$type' AND id_survey_question_fk = $idQusetion AND answer LIKE '%$answer%'";

	$result = db_query_num($query);

	return $result;
}

function gajianInProfil($tanggal, $kode)
{
	$cacah = explode('-', $tanggal);
	$bulan = $cacah[1];
	$tahun = $cacah[0];
	$date = $tahun . '-' . $bulan;
	$gajian = db_query_row("SELECT (gaji_pokok+tunjangan_jabatan+lembur+insentif+konsumsi+tunjangan_tetap+bonus+bonus_kinerja-potongan_absen-potongan_telat-potongan_bpjs+subsidi_bpjs) AS total FROM `user`  JOIN user_gajian
                                    ON kode_karyawan = id_user
                                    WHERE id_user = $kode AND tanggal LIKE '%$date%'");
	return $gajian['total'];
}


function totalPiutangCustomerActive()
{
	$query = "SELECT
SUM(jumlah*harga) AS total
FROM `order` JOIN customers
ON id_customer = order_id_customer JOIN order_pembayaran
ON id_order_pembayaran = order_id_order_pembayaran JOIN order_pengiriman
ON id_order_pembayaran = order_pengiriman.id_order_pembayaran_fk
WHERE  status_pelunasan != 1";

	$result =	db_query_row($query);

	return $result['total'];
}

function totalCustomerPiutang()
{
	$query = "SELECT
SUM(jumlah*harga) AS total
FROM `order` JOIN customers
ON id_customer = order_id_customer JOIN order_pembayaran
ON id_order_pembayaran = order_id_order_pembayaran JOIN order_pengiriman
ON id_order_pembayaran = order_pengiriman.id_order_pembayaran_fk
WHERE  status_pelunasan != 1
GROUP BY order_id_order_pembayaran
HAVING total != 0
";

	return	db_query_num($query);


	// menu pengunjung tampil kotak //




}


function sisaUtangById($id)
{

	db_where('id_utang_vendor_fk', $id);
	$data =  db_get_array('utang_vendor_history');
	dd($data);

	foreach ($data as $value) {
		$utang[]  = $value['utang'];
		$cicilan[] = $value['cicilan'];
	}

	$selisih = array_sum($utang) - array_sum($cicilan);
	return money($selisih);
}





function sisaUtangVendorById($id)
{

	db_where('id_utang_vendor_fk', $id);
	$data =  db_get_array('utang_vendor_history');

	foreach ($data as $value) {
		$utang[]  = $value['utang'];
		$cicilan[] = $value['cicilan'];
	}

	$selisih = array_sum($utang) - array_sum($cicilan);
	return money($selisih);
}

function sisaPiutangKaryawanByIdOld($id)
{

	db_where('id_piutang_karyawan_old_fk', $id);
	$data =  db_get_array('piutang_karyawan_history_old');

	foreach ($data as $value) {
		$utang[]  = $value['hutang'];
		$cicilan[] = $value['cicilan'];
	}

	$selisih = array_sum($utang) - array_sum($cicilan);
	return money($selisih);
}
function sisaPiutangKaryawanById($id)
{

	db_where('id_piutang_karyawan_fk', $id);
	$data =  db_get_array('piutang_karyawan_history');

	foreach ($data as $value) {
		$utang[]  = $value['hutang'];
		$cicilan[] = $value['cicilan'];
	}

	$selisih = array_sum($utang) - array_sum($cicilan);
	return money($selisih);
}

function sisaPiutangPenjahitById($id)
{

	db_where('id_piutang_penjahit_fk', $id);
	$data =  db_get_array('piutang_penjahit_history');

	foreach ($data as $value) {
		$utang[]  = $value['hutang'];
		$cicilan[] = $value['cicilan'];
	}

	$selisih = array_sum($utang) - array_sum($cicilan);
	return money($selisih);
}


function sisaPiutangLuarUsahaById($id)
{

	db_where('id_piutang_luar_usaha_fk', $id);
	$data =  db_get_array('piutang_luar_usaha_history');

	foreach ($data as $value) {
		$utang[]  = $value['piutang'];
		$cicilan[] = $value['cicilan'];
	}

	$selisih = array_sum($utang) - array_sum($cicilan);
	return money($selisih);
}


function selected($data, $value)
{
	return ($data == $value ? 'selected' : '');
}


function deadlinePengirimanHariIni()
{

	$query = "SELECT * FROM `order` JOIN  customers
		 ON id_customer = order_id_customer JOIN customer_kategori
		 ON id_customer_kategori = id_customer_kategori_fk
		 WHERE  DATEDIFF(tgl_kirim,CURDATE()) <= 0 AND `status` NOT IN(9,10)
		 ORDER BY tgl_kirim, id_order DESC";

	return db_query_array($query);
}


function deadlinePengirimanBesok()
{

	$query = "SELECT * FROM `order` JOIN  customers
		 ON id_customer = order_id_customer JOIN customer_kategori
		 ON id_customer_kategori = id_customer_kategori_fk
		 WHERE  DATEDIFF(tgl_kirim,CURDATE()) = 1 AND `status` NOT IN(9,10)
		 ORDER BY tgl_kirim, id_order DESC";

	return db_query_array($query);
}



function jumlahAsetOfice()
{
	$ci = get_instance();

	$query = "SELECT COUNT(barang) AS jumlah_aset_kantor FROM inventaris WHERE tempat = 1";
	$result =  $ci->db->query($query)->row_array();
	return $result['jumlah_aset_kantor'];
}
function jumlahAsetPabrik()
{
	$ci = get_instance();

	$query = "SELECT COUNT(barang) AS jumlah_aset_pabrik FROM inventaris WHERE tempat = 2";
	$result =  $ci->db->query($query)->row_array();
	return $result['jumlah_aset_pabrik'];
}

function jumlahAsetWorkshop()
{
	$ci = get_instance();

	$query = "SELECT COUNT(barang) AS jumlah_aset_pabrik FROM inventaris WHERE tempat = 3";
	$result =  $ci->db->query($query)->row_array();
	return $result['jumlah_aset_pabrik'];
}


function totalAsetDml()

{
	$ci = get_instance();

	$query = "SELECT SUM(harga) AS total_aset FROM inventaris";
	$result =  $ci->db->query($query)->row_array();
	return $result['total_aset'];
}

function dataAsetOffice()
{
	$result = db_query_array(
		"SELECT * FROM inventaris
		JOIN inventaris_kategori ON  id_inventaris_kategori = id_inventaris_kategori_fk
		WHERE tempat = 1"
	);

	return $result;
}
function dataAsetPabrik()
{
	$result = db_query_array(
		"SELECT * FROM inventaris
		JOIN inventaris_kategori ON  id_inventaris_kategori = id_inventaris_kategori_fk
		WHERE tempat = 2"
	);

	return $result;
}
function dataAsetWorksop()
{
	$result = db_query_array(
		"SELECT * FROM inventaris
		JOIN inventaris_kategori ON  id_inventaris_kategori = id_inventaris_kategori_fk
		WHERE tempat = 3"
	);

	return $result;
}

function jumlahTahun()
{

	$ci = get_instance();


	$idCustomerService = session('id');

	$query = "SELECT nama, tanggal_masuk, CURDATE() AS tanggal_sekarang, FLOOR( DATEDIFF(CURDATE(),tanggal_masuk )/360) AS tahun,
	 FLOOR((FLOOR(((DATEDIFF(CURDATE(),tanggal_masuk )/360) - FLOOR( DATEDIFF(CURDATE(),tanggal_masuk )/360))*360))/30) bulan,
	  FLOOR(((FLOOR(((DATEDIFF(CURDATE(),tanggal_masuk )/360) - FLOOR( DATEDIFF(CURDATE(),tanggal_masuk )/360))*360))/30 -
	  FLOOR((FLOOR(((DATEDIFF(CURDATE(),tanggal_masuk )/360) - FLOOR( DATEDIFF(CURDATE(),tanggal_masuk )/360))*360))/30))*30) Hari FROM `user`
WHERE is_active = 1  AND id = $idCustomerService  ORDER BY tahun desc, bulan desc , hari desc";
	$result =  $ci->db->query($query)->row_array();


	return  $result['tahun'];
}


function jumlahBulan()
{

	$ci = get_instance();


	$idCustomerService = session('id');

	$query = "SELECT nama, tanggal_masuk, CURDATE() AS tanggal_sekarang, FLOOR( DATEDIFF(CURDATE(),tanggal_masuk )/360) AS tahun,
	 FLOOR((FLOOR(((DATEDIFF(CURDATE(),tanggal_masuk )/360) - FLOOR( DATEDIFF(CURDATE(),tanggal_masuk )/360))*360))/30) bulan,
	  FLOOR(((FLOOR(((DATEDIFF(CURDATE(),tanggal_masuk )/360) - FLOOR( DATEDIFF(CURDATE(),tanggal_masuk )/360))*360))/30 -
	  FLOOR((FLOOR(((DATEDIFF(CURDATE(),tanggal_masuk )/360) - FLOOR( DATEDIFF(CURDATE(),tanggal_masuk )/360))*360))/30))*30) Hari FROM `user`
WHERE is_active = 1  AND id = $idCustomerService  ORDER BY tahun desc, bulan desc , hari desc";
	$result =  $ci->db->query($query)->row_array();


	return  $result['bulan'];
}
function jumlahHari()
{

	$ci = get_instance();


	$idKaryawan = session('id');

	$query = "SELECT nama, tanggal_masuk, CURDATE() AS tanggal_sekarang, FLOOR( DATEDIFF(CURDATE(),tanggal_masuk )/360) AS tahun,
	 FLOOR((FLOOR(((DATEDIFF(CURDATE(),tanggal_masuk )/360) - FLOOR( DATEDIFF(CURDATE(),tanggal_masuk )/360))*360))/30) bulan,
	  FLOOR(((FLOOR(((DATEDIFF(CURDATE(),tanggal_masuk )/360) - FLOOR( DATEDIFF(CURDATE(),tanggal_masuk )/360))*360))/30 -
	  FLOOR((FLOOR(((DATEDIFF(CURDATE(),tanggal_masuk )/360) - FLOOR( DATEDIFF(CURDATE(),tanggal_masuk )/360))*360))/30))*30) Hari FROM `user`
WHERE is_active = 1  AND id = $idKaryawan  ORDER BY tahun desc, bulan desc , hari desc";
	$result =  $ci->db->query($query)->row_array();


	return  $result['Hari'];
}


function jumlahKesalahan()
{

	$ci = get_instance();
	$tanggal = date('Y');

	$idKaryawan = session('id');

	$query = "SELECT tanggal, nama, count(*) as total, sum(`point`) as Poin FROM pelanggaran_karyawan
				JOIN `user` on id_user_fk = id
				JOIN pelanggaran on id_pelanggaran = id_pelanggaran_fk WHERE tanggal like '%$tanggal%' AND id = $idKaryawan
				GROUP BY nama
				ORDER BY tanggal desc";
	$result =  $ci->db->query($query)->row_array();



	return  $result['total'];
}
function jumlahPoint()
{

	$ci = get_instance();
	$tanggal = date('Y');

	$idKaryawan = session('id');

	$query = "SELECT tanggal, nama, count(*) as total, sum(`point`) as Poin FROM pelanggaran_karyawan
				JOIN `user` on id_user_fk = id
				JOIN pelanggaran on id_pelanggaran = id_pelanggaran_fk WHERE tanggal like '%$tanggal%' AND id = $idKaryawan
				GROUP BY nama
				ORDER BY tanggal desc";
	$result =  $ci->db->query($query)->row_array();



	return  $result['Poin'];
}

function detailPeforma()
{

	$ci = get_instance();
	$tanggal = date('Y-m');

	$idKaryawan = session('kode_karyawan');

	$query = "SELECT nama_user, peforma, kesalahan, peforma_upgrade FROM user_gajian
  WHERE tanggal like '%2024-11%' and id_user = $idKaryawan";
	$result =  $ci->db->query($query)->row_array();



	return  $result['peforma'];
}
function detailPeformaUpgrade()
{

	$ci = get_instance();
	$tanggal = date('Y-m');

	$idKaryawan = session('kode_karyawan');

	$query = "SELECT nama_user, peforma, kesalahan, peforma_upgrade FROM user_gajian
WHERE tanggal like '%2024-11%' and id_user = $idKaryawan";
	$result =  $ci->db->query($query)->row_array();



	return  $result['peforma_upgrade'];
}
function detailKesalahan()
{

	$ci = get_instance();
	$tanggal = date('Y-m');

	$idKaryawan = session('kode_karyawan');

	$query = "SELECT nama_user, peforma, kesalahan, peforma_upgrade FROM user_gajian
WHERE tanggal like '%2024-11%' and id_user = $idKaryawan";
	$result =  $ci->db->query($query)->row_array();



	return  $result['kesalahan'];
}

function Statusfaktur($id)
{
	$ci = get_instance();

	$query = "SELECT * FROM order_faktur JOIN order_pembayaran
	ON id_order_pembayaran = id_order_pembayaran_fk
	WHERE id_order_pembayaran_fk = $id";
	$result =  $ci->db->query($query)->row_array();

	return  $result;
}

function getCountItemRequestDesign($id)
{
	$data = db_get_array('request_design_produk', ['id_request_design_fk' => $id]);

	return count($data);
}



function find_highest_and_lowest($data, $key)
{
	$max = null;
	$min = null;

	$max_bulan = $min_bulan = null;
	$i = 0;

	if (filterGetYear() == date('Y')) {

		$month = date('m');
		$arrayMonth = [];
		for ($j = $month; $j <= 12; $j++) {
			$arrayMonth[] = $j;
		}
	} else {
		$arrayMonth[] = 13;
	}



	foreach ($data as $value) {
		if ($i++ != 0) {

			if (in_array($value['bulan'], $arrayMonth)) {
				$max = $max;
				$max_bulan = $max_bulan;
			} else {
				if ($max === null || $value[$key] > $max) {
					$max = $value[$key];
					$max_bulan = $value['bulan'];
				}
			}


			if (in_array($value['bulan'], $arrayMonth)) {
				$min = $min;
				$min_bulan = $min_bulan;
			} else {
				if ($min === null || $value[$key] < $min) {
					$min = $value[$key];
					$min_bulan = $value['bulan'];
				}
			}
		}
	}

	return ['max' => $max, 'min' => $min, 'max_bulan' => $max_bulan, 'min_bulan' => $min_bulan];
}


function autoCloseOmset()
{

	db_select('MONTH(tanggal) as bulan,SUM(qty* harga) AS pendapatan_penjualan');
	db_where('account_kredit', 22);   // 22 - Pendapatan Penjualan;
	db_where('YEAR(tanggal)', date('Y'));
	db_where('MONTH(tanggal)', date('m'));
	db_group('MONTH(tanggal)');
	$result =  db_get_row('journal_umum');
	if (!empty($result)) {
		$revenue = db_get_row('accounting_revenue', ['bulan' => date('m'), 'tahun' => date('Y')]);
		if (!empty($revenue)) {
			$data = [
				'bulan' => date('m'),
				'tahun' => date('Y'),
				'revenue' => $result['pendapatan_penjualan'],
				'created_at' => timestamp()
			];
			update('accounting_revenue', $data, ['id_accounting_revenue' => $revenue['id_accounting_revenue']]);
		} else {
			$data = [
				'bulan' => date('m'),
				'tahun' => date('Y'),
				'revenue' => $result['pendapatan_penjualan'],
				'created_at' => timestamp()
			];
			create('accounting_revenue', $data);
		}
	} else {
		$data = [
			'bulan' => date('m'),
			'tahun' => date('Y'),
			'revenue' => 0,
			'created_at' => timestamp()
		];
		create('accounting_revenue', $data);
	}
}

function autoComplatePenjahitInternalAndSubcon()
{
	$query = "SELECT * FROM jahitan_penjahit_qc
	JOIN jahitan_penjahit ON id_jahitan_penjahit = id_jahitan_penjahit_fk
	WHERE id_penjahit_fk IN (43,53) AND index_gaji_fk IS NULL";

	$result = db_query_array($query);
	foreach ($result as $value) {

		$data = [
			'status_gaji_qc' => 'paid',
			'index_gaji_fk_qc' => 'complate',

		];

		UPDATE('jahitan_penjahit_qc', $data, ['id_jahitan_penjahit_fk' => $value['id_jahitan_penjahit']]);
	}


	$query = "SELECT * FROM jahitan_penjahit WHERE id_penjahit_fk IN (43,53) AND index_gaji_fk IS NULL";

	$result = db_query_array($query);
	foreach ($result as $value) {

		$data = [
			'status_gaji_jahitan' => 'done',
			'index_gaji_fk' => 'complate',
		];

		UPDATE('jahitan_penjahit', $data, ['id_jahitan_penjahit' => $value['id_jahitan_penjahit']]);
	}
}

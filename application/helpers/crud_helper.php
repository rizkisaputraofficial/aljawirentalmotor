<?php



function inpos($data)
{
    $ci = get_instance();
    return $ci->input->post($data);
}


function create($table = null, $data = null)
{
    $ci = get_instance();
    $ci->db->insert($table, $data);
    return $ci->db->affected_rows();
}


function update($table = null, $data = null, $id = null)
{
    $ci = get_instance();
    // if ($id != null) {
    //     $id = $id;
    // } else {
    //     $id = inpos('id');
    // }
    // $idTable = 'id_' . $table; //id_admin
    // $ci->db->update($table, $data, [$idTable => $id]);

    if (is_array($id)) {
        $ci->db->update($table, $data, $id);
    } else {

        if ($id != null) {
            $id = $id;
        } else {
            $id = inpos('id');
        }
        $idTable = 'id_' . $table; //id_admin
        $ci->db->update($table, $data, [$idTable => $id]);
    }
    return $ci->db->affected_rows();
}

function delete($table = null, $id = null)
{
    $ci = get_instance();
    if ($id != null) {
        $id = $id;
    } else {
        $id = inpos('id');
    }
    $idTable = 'id_' . $table; //id_admin
    $ci->db->delete($table, [$idTable => $id]);
    return $ci->db->affected_rows();
}

// function load_view($view = null, $data = null, $type = null)
// {

//     $ci = get_instance();
//     clearSessionSearch(url_page());
//     if ($type == 'die') {
//         die();
//     } else if ($type == 'pagination') {
//         pagination(url_page(), page(), row(), 'pagination');
//     }

//     $ci->load->view('template/header', $data);
//     $ci->load->view('template/sidebarNew');
//     $ci->load->view($view, $data);
//     $ci->load->view('template/footer2023');
// }

function load_view_2023($view = null, $data = null, $type = null)
{

    $ci = get_instance();
    clearSessionSearch(url_page());
    if ($type == 'die') {
        die();
    } else if ($type == 'pagination') {
        pagination(url_page(), page(), row(), 'pagination');
    }

    $ci->load->view('template/header', $data);
    $ci->load->view('template/sidebarNew');
    $ci->load->view($view, $data);
    $ci->load->view('template/footer2023');
}



function load_view_new($view = null, $data = null, $id = null)
{


    $ci = get_instance();

    if (!empty($data['row'])) {
        $ci->session->set_userdata(['row-data' => $data['row']]);
    } else {
        $ci->session->set_userdata(['row-data' => 0]);
    }

    pagination_new(url_page_new(), page(), row());
    clearSessionSearch(url_page());
    $ci->load->view('template/header', $data);
    $ci->load->view('template/sidebar');
    $ci->load->view($view, $data);
    $ci->load->view('template/footer');
}


function load_view_karyawan($view = null, $data = null)
{

    $ci = get_instance();
    $ci->load->view('template/header', $data);
    $ci->load->view('template/sidebar');
    $ci->load->view('karyawan/seederKaryawan');
    $ci->load->view($view, $data);
    $ci->load->view('template/footer');
}

// function redirect_back($data = null, $notif = null, $url = null, $notif2 = null)
// {

//     $ci = get_instance();
//     if (!empty($url)) {
//         $url = $url;
//     } else {
//         $url = $ci->input->post('url');
//     }

//     if ($data > 0) {
//         notification_succes($notif, $notif2);
//     } else {
//         notification_fail($notif, $notif2);
//     }
//     redirect($url);
// }

function checkUserAccess($akses = null)
{
    // $ci = get_instance();
    // $url = $ci->input->post('url');

    // if ($akses == 'create') {
    //     $user_access = user_access($akses);
    //     if ($user_access != 1) {
    //         $url = $ci->input->post('url');
    //         notification_fail('Ditambah', 'Akses Dilarang');
    //         redirect($url);
    //     }
    // } else if ($akses == 'update') {
    //     $user_access = user_access($akses);
    //     if ($user_access != 1) {
    //         notification_fail('Diubah', 'Akses Dilarang');
    //         redirect($url);
    //     }
    // } else if ($akses == 'delete') {
    //     $user_access = user_access($akses);
    //     if ($user_access != 1) {
    //         notification_fail('Dihapus', 'Akses Dilarang');

    //         redirect($url);
    //     }
    // } else if ($akses == 'read') {
    //     $user_access = user_access($akses);
    //     if ($user_access !=1) {
    //         notification_fail('Diakses', 'Akses Dilarang');
    //         redirect('auth/AccessDenied');
    //     }
    // }
}

function redirect_create($data = null)
{



    redirect_back($data, 'Ditambah');
}

function redirect_update($data = null)
{
    $ci = get_instance();
    $url = $ci->input->post('url');
    $user_access = user_access('update');
    if ($user_access != 'success') {
        $ci->session->set_flashdata('message', '<div class=" alert alert-warning alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>Data <strong>Gagal Diubah!</strong>, Akses Dilarang</div>');
        redirect($url);
    }

    redirect_back($data, 'Diubah');
}

function redirect_delete($data = null)
{
    $ci = get_instance();
    $url = $ci->input->post('url');
    $user_access = user_access('delete');
    if ($user_access != 'success') {
        $ci->session->set_flashdata('message', '<div class=" alert alert-warning alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>Data <strong>Gagal Dihapus!</strong>, Akses Dilarang</div>');
        redirect($url);
    }

    redirect_back($data, 'Delete');
}


// function timestamp()
// {
//     return date('Y-m-d G:i:s');
// }


function db_join($table =  null,  $id = null)
{
    $ci = get_instance();
    if ($id == null) {
        $primaryKey = 'id_' . $table;
        $foreignKey = 'id_' . $table . '_fk';
        $key = $primaryKey . ' = ' . $foreignKey;
    } else {

        $key = $id;
    }
    $ci->db->join($table,  $key);
}

function db_join_left($table =  null,  $id = null)
{
    $ci = get_instance();
    if ($id == null) {
        $primaryKey = 'id_' . $table;
        $foreignKey = 'id_' . $table . '_fk';
        $key = $primaryKey . ' = ' . $foreignKey;
    } else {

        $key = $id;
    }
    $ci->db->join($table,  $key, 'left');
}
function db_like($field = null, $content = null)
{
    $ci = get_instance();
    $ci->db->like($field, $content);
}

function db_limit($limit = null, $start = null)
{
    $ci = get_instance();
    $ci->db->limit($limit, $start);
}
function db_where($fielt = null, $content = null)
{
    $ci = get_instance();
    $ci->db->where($fielt, $content);
}
function db_or_where($fielt = null, $content = null)
{
    $ci = get_instance();
    $ci->db->or_where($fielt, $content);
}

function db_where_in($fielt = null, $content = null)
{
    $ci = get_instance();
    $ci->db->where_in($fielt, $content);
}
function db_where_not_in($fielt = null, $content = null)
{
    $ci = get_instance();
    $ci->db->where_not_in($fielt, $content);
}

function db_or_like($field = null, $content = null)
{
    $ci = get_instance();
    $ci->db->or_like($field, $content);
}

function db_order($field = null, $content = null)
{
    $ci = get_instance();
    $ci->db->order_by($field, $content);
}



function db_group($field = null)
{
    $ci = get_instance();
    $ci->db->group_by($field);
}

function db_select($field = null)
{
    $ci = get_instance();
    $ci->db->select($field);
}

function db_sum($field = null)
{
    $ci = get_instance();
    $ci->db->select_sum($field);
}


function db_from($field = null)
{
    $ci = get_instance();
    $ci->db->from($field);
}

function db_get($table = null, $type = null, $id = null)
{
    $ci = get_instance();
    if ($type == 'data') {
        $result =  db_get_array($table, $id);
        $count = count($result);
        if (!empty($result)) {
            $ci->session->set_userdata(['row-result' => $count]);
        } else {
            $ci->session->set_userdata(['row-result' => 0]);
        }
    } else if ($type == 'row') {
        $result =  db_get_num($table, $id);
        if (!empty($result)) {
            $ci->session->set_userdata(['row-data' => $result]);
        } else {
            $ci->session->set_userdata(['row-data' => 0]);
        }
    } else {
        $result = null;
    }
    return $result;
}

function db_get_array($table = null, $id = null)
{

    $ci = get_instance();
    if (empty($id)) {
        return $ci->db->get($table)->result_array();
    } else {
        if (is_array($id)) {
            return $ci->db->get_where($table, $id)->result_array();
        } else {
            $idTable = 'id_' . $table;
            return $ci->db->get_where($table, [$idTable => $id])->result_array();
        }
    }
}

function db_get_row($table = null, $id = null)
{

    $ci = get_instance();

    if (empty($id)) {
        return $ci->db->get($table)->row_array();
    } else {
        if (is_array($id)) {
            return $ci->db->get_where($table, $id)->row_array();
        } else {
            $idTable = 'id_' . $table;
            return $ci->db->get_where($table, [$idTable => $id])->row_array();
        }
    }
}

function db_get_num($table = null, $id = null)
{

    $ci = get_instance();
    if (empty($id)) {
        return $ci->db->get($table)->num_rows();
    } else {
        if (is_array($id)) {
            return $ci->db->get_where($table, $id)->num_rows();
        } else {
            $idTable = 'id_' . $table;
            return $ci->db->get_where($table, [$idTable => $id])->num_rows();
        }
    }
}

function db_get_last($table = null)
{

    $ci = get_instance();

    return $ci->db->get($table)->last_row();
}



function db_query_array($query = null)
{

    $ci = get_instance();
    return $ci->db->query($query)->result_array();
}


function db_query_row($query = null)
{

    $ci = get_instance();
    return $ci->db->query($query)->row_array();
}

function db_query_num($query = null)
{

    $ci = get_instance();
    return $ci->db->query($query)->num_rows();
}

function trans_start()
{

    $ci = get_instance();
    $ci->db->trans_start();
}

function trans_end()
{

    $ci = get_instance();
    $ci->db->trans_complete();
    if ($ci->db->trans_status() === FALSE) {
        return 0;
    } else {
        return 1;
    }
}

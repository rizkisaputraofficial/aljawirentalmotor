<?php
class Customer_model extends CI_Model
{

    public function getAllDataCustomer()
    {
        return $this->db->get('customer')->result_array();
    }
    public function getDataById($id)
    {
        return $this->db->get_where('customer', ['id_customer' => $id])->row_array();
    }

    public function getDataTransaksiById($id)
    {
        $this->db->join('motor', 'id_motor = id_motor_fk');
        return $this->db->get_where('transaksi', ['id_customer_fk' => $id])->result_array();
    }


    public function tambah()
    {
        // $image = upload();
        $data = array(
            'nama_customer' => $this->input->post('nama_customer'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'gender' => $this->input->post('gender'),
            'telpon' => $this->input->post('telpon'),
            'alamat' => $this->input->post('alamat'),
            // 'foto_ktp' => $image

        );

        $this->db->insert('customer', $data);
        return $this->db->affected_rows();
    }

    public function edit()
    {

        $data = array(
            'nama_customer' => $this->input->post('nama_customer'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'gender' => $this->input->post('gender'),
            'telpon' => $this->input->post('telpon'),
            'alamat' => $this->input->post('alamat'),

        );

        $id = $this->input->post('id');
        $this->db->update('customer', $data, array('id_customer' => $id));
        return $this->db->affected_rows();
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->db->delete('customer', array('id_customer' => $id));
        return $this->db->affected_rows();
    }

    public function uploadKTP()
    {

        $data = array(
            'foto_ktp'  => upload(),
        );

        $id = $this->input->post('id');
        $this->db->update('customer', $data, array('id_customer' => $id));
        return $this->db->affected_rows();
    }





	public function jumlahCustomerPerbulan()
    {
        $tahun =  $this->input->post('tahun');
        $bulan =  $this->input->post('bulan');
        if ($tahun == null) {
            $tahun = date('Y');
        } else {
            $tahun = $tahun;
        }

        if ($bulan == null) {
            $bulan = date('m');
        } else {
            $bulan = $bulan;
        }
        $tanggal = $tahun . '-' . $bulan;
        $query = "SELECT MONTH(created_at) AS label, COUNT(DISTINCT(id_customer)) 
					AS Y FROM customer 
					WHERE created_at LIKE '%$tanggal%' AND created_at >= '2023-01-01'
					GROUP BY YEAR(created_at),  MONTH(created_at)";
           		  $result = $this->db->query($query)->result_array();
		  		  foreach ($result as $value) {
          		  $data[] = array(
                'label' => ($value['label']),
             
          		  );
        }
        // var_dump($data);
        return $data;
    }

}
<?php
class User_model extends CI_Model
{

    public function getAllData()
    {
        return $this->db->get('user')->result_array();
    }

    public function tambah()
    {
        $data = array(
            'nama' => $this->input->post('nama'),
            'telpon' => $this->input->post('telpon'),
            'username' => $this->input->post('username'),
            // 'level' => $this->input->post('level'),
            'password' => $this->input->post('password'),
        );
        $this->db->insert('user', $data);
        return $this->db->affected_rows();
    }

    public function edit()
    {
        $data = array(
            'nama' => $this->input->post('nama'),
            'telpon' => $this->input->post('telpon'),
            'username' => $this->input->post('username'),
            // 'level' => $this->input->post('level'),
            'password' => $this->input->post('password'),
        );
        $id = $this->input->post('id');
        $this->db->update('user', $data, array('id_user' => $id));
        return $this->db->affected_rows();
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->db->delete('user', array('id_user' => $id));
        return $this->db->affected_rows();
    }
}
<?php
class Denda_model extends CI_Model
{

    public function getAllData()
    {
        return $this->db->get('denda')->result_array();
    }

   

    public function edit()
    {
        $data = array(
            'denda' => $this->input->post('denda'),
        );
        $id = $this->input->post('id');
        $this->db->update('denda', $data, array('id_denda' => $id));
        return $this->db->affected_rows();
    }

 
}
<?php
class Motor_model extends CI_Model
{

    public function getAllData()
    {
     
        $this->db->order_by('id_motor','desc');
        return $this->db->get('motor')->result_array();
    }
    public function getdataMotor()
    {
      
        return $this->db->get('motor')->result_array();
    }
    public function getDataById($id)
    {
        
        return $this->db->get_where('motor', ['id_motor' => $id])->row_array();
    }

    public function getDataTransaksiById($id)
    {
        $this->db->join('motor', 'id_motor = id_motor_fk');
        return $this->db->get_where('transaksi', ['id_motor_fk' => $id])->result_array();
    }

    public function getDataJenisTarifByIdMotor($id)
    {

        $motor = $this->db->get_where('tarif', ['id_motor_fk' => $id])->result_array();
        if ($motor == null) {
            $idJenis = null;
        } else {
            foreach ($motor as $value) :
                $idJenis[] = $value['id_jenis_tarif_fk'];
            endforeach;
        }


        $this->db->where_not_in('id_jenis_tarif', $idJenis);
        return $this->db->get('jenis_tarif')->result_array();
    }
    public function getDataTarifByIdMotor($id)
    {
        $this->db->join('jenis_tarif',  'id_jenis_tarif = id_jenis_tarif_fk');
        $this->db->order_by('id_jenis_tarif', 'asc');
        return $this->db->get_where('tarif', ['id_motor_fk' => $id])->result_array();
    }


    public function tambah()
    {

        $data = array(
          
            'merek' => $this->input->post('merek'),
            'no_plat' => $this->input->post('no_plat'),
            'warna' => $this->input->post('warna'),
            'tahun' => $this->input->post('tahun'),
            'bahan_bakar' => $this->input->post('bahan_bakar'),
            'status' => 1,
            'gambar' => upload(),

        );

        $this->db->insert('motor', $data);
        return $this->db->affected_rows();
    }
    public function tambahTarif()
    {
        $jenis =  $this->input->post('id_jenis_tarif');
        $harga =  $this->input->post('harga_tarif');
        $idmotor = $this->input->post('id_motor');
        //    var_dump($jenis);
        $count = count($jenis);
        for ($i = 0; $i < $count; $i++) {

            if ($harga[$i] != 0) {
                $data = array(
                    'id_jenis_tarif_fk' => $jenis[$i],
                    'id_motor_fk' => $idmotor,
                    'harga_tarif' =>  $harga[$i],
                );
                $this->db->insert('tarif', $data);
            }
        }
        return $this->db->affected_rows();
    }

    public function edit()
    {

       
        $data = array(
          
            'merek' => $this->input->post('merek'),
            'no_plat' => $this->input->post('no_plat'),
            'warna' => $this->input->post('warna'),
            'tahun' => $this->input->post('tahun'),
            'status' => $this->input->post('status'),
            'harga' => $this->input->post('harga'),
            'denda' => $this->input->post('denda'),
            'bahan_bakar' => $this->input->post('bahan_bakar'),
            'gambar' => upload(),

        );
       
        $id = $this->input->post('id');
        $this->db->update('motor', $data, array('id_motor' => $id));
        return $this->db->affected_rows();
    }
    public function editTarif()
    {
        $data = array(
            'harga_tarif' => $this->input->post('harga_tarif'),
        );

        $id = $this->input->post('id');
        $this->db->update('tarif', $data, array('id_tarif' => $id));
        return $this->db->affected_rows();
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->db->delete('motor', array('id_motor' => $id));
        return $this->db->affected_rows();
    }

    public function deleteTarif()
    {
        $id = $this->input->post('id');
        $this->db->delete('tarif', array('id_tarif' => $id));
        return $this->db->affected_rows();
    }
}
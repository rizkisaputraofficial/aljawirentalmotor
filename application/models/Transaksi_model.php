<?php
class Transaksi_model extends CI_Model
{

    // public function getAllData()
    // {
    //     $this->db->join('customer', 'id_customer = id_customer_fk', 'left');
    //     $this->db->join('motor', 'id_motor = id_motor_fk', 'left');
    //     $this->db->order_by('tanggal_rental', 'DESC');

    //     return $this->db->get('transaksi')->result_array();
    // }


    public function getAllData($limit, $start)
    {
        filter($limit, $start, 'get');
        // $this->db->join('user_role', 'role_id = id_user_role');
        // $this->db->order_by('id', 'ASC');
        // return $this->db->get('user')->result_array();

        $this->db->join('customer', 'id_customer = id_customer_fk', 'left');
        $this->db->join('motor', 'id_motor = id_motor_fk', 'left');
        $this->db->order_by('tanggal_rental', 'DESC');

        return $this->db->get('transaksi')->result_array();
    }

    public function countAllData()
    {
        filter($limit = null, $start = null, 'row');
        // $this->db->join('user_role', 'role_id = id_user_role');
        // $this->db->order_by('id', 'ASC');
        // return $this->db->get('user')->num_rows();

        $this->db->join('customer', 'id_customer = id_customer_fk', 'left');
        $this->db->join('motor', 'id_motor = id_motor_fk', 'left');
        $this->db->order_by('tanggal_rental', 'DESC');

        return $this->db->get('transaksi')->num_rows();
    }


    public function getDataById($id)
    {
        $data = $this->db->get_where('transaksi', ['id_transaksi' => $id])->row_array();
        $idCustomer = $data['id_customer_fk'];
        return $this->db->get_where('customer', ['id_customer' => $idCustomer])->row_array();
    }

    public function getDataTransaksiById($id)
    {

        return $this->db->get_where('transaksi', ['id_transaksi' => $id])->row_array();
    }

    public function getDataMotorById()
    {
        $id = $this->input->post('id_motor');
        $this->db->join('jenis_tarif', 'id_jenis_tarif = id_jenis_tarif_fk');
        $this->db->join('tarif', 'id_motor = id_motor_fk');
        return $this->db->get_where('motor', ['id_motor' => $id])->row_array();
    }


    public function getDataTransaksiByIdTransaksi()
    {
        $id = $this->input->post('id_transaksi');
        return $this->db->get_where('transaksi', ['id_transaksi' => $id])->row_array();
    }

    public function getDataTarifByIdMotor()
    {
        $id = $this->input->post('id_motor');
        $this->db->join('jenis_tarif', 'id_jenis_tarif = id_jenis_tarif_fk');

        return $this->db->get_where('tarif', ['id_motor_fk' => $id])->result_array();
    }

    public function getDataTarifByIdTarif()
    {
        $id = $this->input->post('id');
        $this->db->join('jenis_tarif', 'id_jenis_tarif = id_jenis_tarif_fk');
        return $this->db->get_where('tarif', ['id_tarif' => $id])->row_array();
    }


    public function tambah()
    {
        $dataCustomer = array(
            'kode_transaksi' => uniqid(),
            'nama_customer' => $this->input->post('nama_customer'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'gender' => $this->input->post('gender'),
            'telpon' => $this->input->post('telpon'),
            'alamat' => $this->input->post('alamat'),



        );



        $this->db->insert('customer', $dataCustomer);

        $customer = $this->db->get('customer')->last_row();
        $idCustomer = $customer->id_customer;
        $idTarif =  $this->input->post('id_tarif');
        $tarif =  $this->db->get_where('tarif', ['id_tarif' => $idTarif])->row_array();
        $harga = $tarif['harga_tarif'];

        $dataTransaksi = array(
            'id_customer_fk' => $idCustomer,
            'id_motor_fk' => $this->input->post('id_motor'),
            'id_tarif_fk' => $this->input->post('id_tarif'),
            'tanggal_rental' => $this->tanggalRental(),
            'tanggal_kembali' => $this->tanggalKembali(),
            'tanggal_pengembalian' => '0000-00-00 00:00:00',
            'total_harga' => $harga,
            'status_transaksi' => 1,
        );

        $this->db->insert('transaksi', $dataTransaksi);
        return $this->db->affected_rows();
    }




    public function tambahTransaksi()
    {



        $idCustomer = $this->session->userdata('id_customer');
        $idmotor = $this->input->post('id_motor');

        $idTarif =  $this->input->post('id_tarif');
        $tarif =  $this->db->get_where('tarif', ['id_tarif' => $idTarif])->row_array();
        $harga = $tarif['harga_tarif'];

        $dataTransaksi = array(

            'id_customer_fk' => $idCustomer,
            'id_motor_fk' =>   $idmotor,
            'id_tarif_fk' => $idTarif,
            'tanggal_rental' => $this->tanggalRental(),
            'tanggal_kembali' => $this->tanggalKembali(),
            'tanggal_pengembalian' => '0000-00-00 00:00:00',
            'total_harga' => $harga,
            'status_transaksi' => 1,

        );





        $this->db->insert('transaksi', $dataTransaksi);
        return $this->db->affected_rows();
    }

    public function edit()
    {


        $data = array(
            'nama_customer' => $this->input->post('nama_customer'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'gender' => $this->input->post('gender'),
            'telpon' => $this->input->post('telpon'),

            'alamat' => $this->input->post('alamat'),
        );

        $id = $this->input->post('id');
        $this->db->update('customer', $data, array('id_customer' => $id));
        return $this->db->affected_rows();
    }

    public function editTransaksi()
    {
        $data = array(

            'id_motor_fk' => $this->input->post('id_motor'),
            'id_tarif_fk' => $this->input->post('id_tarif'),
            'tanggal_rental' =>  $this->tanggalRental(),
            'tanggal_kembali' => $this->tanggalKembali(),
            'tanggal_pengembalian' =>  $this->tanggalPengembalian(),
            'total_harga' => $this->input->post('total_harga'),
            'total_denda' =>  $this->totalDenda(),
            'status_transaksi' => $this->input->post('status_transaksi'),
            'jam_terlambat' => $this->jamTerlambat(),
        );


        $id = $this->input->post('id');
        // var_dump($data);
        $this->db->update('transaksi', $data, array('id_transaksi' => $id));
        return $this->db->affected_rows();
    }

    public function tanggalRental()
    {
        $tanggalRental =  $this->input->post('tanggal_rental');
        $jamRental = $this->input->post('jam_rental');
        return $tanggalRental = $tanggalRental . ' ' . $jamRental;
    }

    public function tanggalKembali()
    {

        $tanggalKembali =  $this->input->post('tanggal_kembali');
        $jamKembali = $this->input->post('jam_kembali');
        return $tanggalKembali = $tanggalKembali . ' ' . $jamKembali;
    }

    public function tanggalPengembalian()
    {
        $tanggalPengembalian =  $this->input->post('tanggal_pengembalian');
        $jamPengembalian = $this->input->post('jam_pengembalian');
        return $tanggalPengembalian = $tanggalPengembalian . ' ' . $jamPengembalian;
    }
    public function jamTerlambat()
    {

        $tanggalPengembalian =  $this->input->post('tanggal_pengembalian');

        if ($tanggalPengembalian == '') {
            return 0;
        } else {
            $tanggalKembali = $this->tanggalKembali();
            $tanggalPengembalian =  $this->input->post('tanggal_pengembalian');
            $jamPengembalian = $this->input->post('jam_pengembalian');
            $tanggalPengembalian = $tanggalPengembalian . ' ' . $jamPengembalian;

            if ($tanggalPengembalian <= $tanggalKembali) {
                $totalJam = 0;
            } else {
                $awal  = date_create($tanggalKembali);
                $akhir = date_create($tanggalPengembalian); // waktu sekarang



                $diff  = date_diff($awal, $akhir);
                $hari = $diff->d;
                $jam = $diff->h;
                $menit = $diff->i;
                $totalJam = ($hari * 24) + $jam;
            }




            return $totalJam;
        }
    }

    public function totalDenda()
    {
        $terlambat = $this->jamTerlambat();

        return $terlambat * 10000;
    }



    public function delete()
    {
        $id = $this->input->post('id');
        $this->db->delete('transaksi', array('id_transaksi' => $id));
        return $this->db->affected_rows();
    }

    public function bulan($date)
    {

        $cacah = explode('-', $date);

        $tahun = $cacah[0];
        $bulan = $cacah[1];

        if ($tahun == NULL) {

            $tahun = date('Y');
        } else {
            $tahun = $tahun;
        }
        if ($bulan == NULL) {
            $bulan = date('m');
        } else {
            $bulan = $bulan;
        }
        if ($bulan == 1) {

            $bulanSkrng = $bulan;
            $tahunSkrng = $tahun;
            $bulanKemarin = 12;
            $tahunKemarin = $tahun - 1;
        } else {
            $bulanSkrng = $bulan;
            $tahunSkrng = $tahun;
            $bulanKemarin =  $bulan - 1;

            if (strlen($bulanKemarin) == 1) {
                $bulanKemarin = '0' . $bulanKemarin;
            } else {
                $bulanKemarin = $bulanKemarin;
            }
            $tahunKemarin = $tahun;
        }

        $getMonth = $this->db->get('get_month')->result_array();
        foreach ($getMonth as $value) {
            if ($value['value'] == $bulan) {
                $bulan = $value['bulan'];
                // var_dump($bulan);
            }
        }

        $bulanSekarang = $tahunSkrng . '-' . $bulanSkrng;
        $bulanKemarin = $tahunKemarin . '-' . $bulanKemarin;

        $data = array(

            'bulan_sekarang' => $bulanSekarang,
            'bulan_kemarin' => $bulanKemarin,
            'bulan' => $bulan,
            'tahun' => $tahun,
        );


        return $data;
    }
}

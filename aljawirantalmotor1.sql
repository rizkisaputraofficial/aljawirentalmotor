/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.4.25-MariaDB : Database - aljawirentalmotor
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`aljawirentalmotor` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `aljawirentalmotor`;

/*Table structure for table `customer` */

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL AUTO_INCREMENT,
  `nama_customer` varchar(20) DEFAULT NULL,
  `tempat_lahir` varchar(20) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `gender` enum('L','P') DEFAULT NULL,
  `telpon` varchar(20) DEFAULT NULL,
  `foto_ktp` varchar(50) DEFAULT NULL,
  `image` varchar(20) DEFAULT NULL,
  `no_ktp` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `kode_transaksi` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=latin1;

/*Data for the table `customer` */

insert  into `customer`(`id_customer`,`nama_customer`,`tempat_lahir`,`tanggal_lahir`,`alamat`,`gender`,`telpon`,`foto_ktp`,`image`,`no_ktp`,`created_at`,`kode_transaksi`) values 
(98,'Randi','sungai binjai muara ','2023-12-11','fefefe','L','082328334199','65772b4285d08.jpeg',NULL,NULL,NULL,'65772a59b7fe9'),
(99,'aminah','sungai binjai muara ','2023-12-11','Jl. Ki Ageng Pemanahan No.35B, Sorosutan, Kec. Umb','P','6546546',NULL,NULL,NULL,NULL,'65773fc80e858'),
(100,'irfan','bungo','2023-07-11','Jl. Ki Ageng Pemanahan No.35B, Sorosutan, Kec. Umb','L','082328334199',NULL,NULL,NULL,NULL,'65773ff3b797c'),
(101,'mariko','ambon','2023-12-11','Jl. Ki Ageng Pemanahan No.35B, Sorosutan, Kec. Umb','L','082328334199',NULL,NULL,NULL,NULL,'657740715d474'),
(102,'devi','sungai binjai muara ','2023-12-27','Jl. Ki Ageng Pemanahan No.35B, Sorosutan, Kec. Umb','P','082328334199',NULL,NULL,NULL,NULL,'657740c8a6c66'),
(103,'Rizki','Tiroau','2023-07-12','Jl. Ki Ageng Pemanahan No.35B, Sorosutan, Kec. Umb','L','082328334199',NULL,NULL,NULL,NULL,'6577410c8d34b'),
(104,'kiki','sungai binjai muara ','2023-11-20','Jl. Ki Ageng Pemanahan No.35B, Sorosutan, Kec. Umb','P','082328334199',NULL,NULL,NULL,NULL,'65774161ccc8c'),
(105,'Reza','sungai binjai muara ','2023-12-19','Jl. Ki Ageng Pemanahan No.35B, Sorosutan, Kec. Umb','P','082328334199',NULL,NULL,NULL,NULL,'657741905b8d6'),
(106,'Sindi','sungai binjai muara ','2023-12-12','Jl. Ki Ageng Pemanahan No.35B, Sorosutan, Kec. Umb','P','082328334199',NULL,NULL,NULL,NULL,'657741df0436a'),
(107,'Agus','sungai binjai muara ','2023-11-23','Jl. Ki Ageng Pemanahan No.35B, Sorosutan, Kec. Umb','L','082328334199',NULL,NULL,NULL,NULL,'6577422841022'),
(108,'Ilham','sungai binjai muara ','2023-12-13','Jl. Ki Ageng Pemanahan No.35B, Sorosutan, Kec. Umb','L','082328334199',NULL,NULL,NULL,NULL,'6577425d34fc2'),
(109,'Andi','sungai binjai muara ','2023-12-18','Jl. Ki Ageng Pemanahan No.35B, Sorosutan, Kec. Umb','L','082328334199',NULL,NULL,NULL,NULL,'6577428991b59'),
(110,'aminah','sungai binjai muara ','2023-12-12','Jl. Ki Ageng Pemanahan No.35B, Sorosutan, Kec. Umb','L','082328334199',NULL,NULL,NULL,NULL,'6577434650dae'),
(111,'Rizki','sungai binjai muara ','0000-00-00','Jl. Ki Ageng Pemanahan No.35B, Sorosutan, Kec. Umb','L','082328334199',NULL,NULL,NULL,NULL,'657743738bf61'),
(112,'aminah','sungai binjai muara ','2023-12-12','','P','082328334199',NULL,NULL,NULL,NULL,'6577461c2667b'),
(113,'aminah','sungai binjai muara ','2023-12-12','','P','082328334199',NULL,NULL,NULL,NULL,'6577461d468d5'),
(114,'din','sungai binjai muara ','2023-12-12','asSs','L','082328334199',NULL,NULL,NULL,NULL,'6577470934914');

/*Table structure for table `denda` */

DROP TABLE IF EXISTS `denda`;

CREATE TABLE `denda` (
  `id_denda` int(11) NOT NULL AUTO_INCREMENT,
  `denda` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_denda`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `denda` */

insert  into `denda`(`id_denda`,`denda`) values 
(1,10000);

/*Table structure for table `get_month` */

DROP TABLE IF EXISTS `get_month`;

CREATE TABLE `get_month` (
  `id_get_month` int(11) NOT NULL AUTO_INCREMENT,
  `bulan` varchar(20) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  KEY `id_get_month` (`id_get_month`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

/*Data for the table `get_month` */

insert  into `get_month`(`id_get_month`,`bulan`,`value`) values 
(1,'Januari',1),
(2,'Februari',2),
(3,'Maret',3),
(4,'April',4),
(5,'Mei',5),
(6,'Juni',6),
(7,'Juli',7),
(8,'Agustus',8),
(9,'September',9),
(10,'Oktober',10),
(11,'November',11),
(12,'Desember',12);

/*Table structure for table `jenis_tarif` */

DROP TABLE IF EXISTS `jenis_tarif`;

CREATE TABLE `jenis_tarif` (
  `id_jenis_tarif` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_tarif` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_tarif`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `jenis_tarif` */

insert  into `jenis_tarif`(`id_jenis_tarif`,`jenis_tarif`) values 
(1,'6 Jam'),
(2,'12 Jam'),
(3,'1 Hari'),
(4,'2 Hari'),
(5,'3 Hari'),
(6,'5 Hari'),
(7,'1 Minggu'),
(8,'2 Minggu'),
(9,'3 Minggu'),
(10,'1 Bulan');

/*Table structure for table `motor` */

DROP TABLE IF EXISTS `motor`;

CREATE TABLE `motor` (
  `id_motor` int(11) NOT NULL AUTO_INCREMENT,
  `merek` varchar(20) NOT NULL,
  `no_plat` varchar(20) NOT NULL,
  `warna` varchar(20) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `status` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  `denda` int(11) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `bahan_bakar` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_motor`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `motor` */

insert  into `motor`(`id_motor`,`merek`,`no_plat`,`warna`,`tahun`,`status`,`harga`,`denda`,`gambar`,`bahan_bakar`) values 
(13,'MIO 123','AB3454BBV','Merah Muda','2019','1',0,0,'625cecd7ea7b8.jpg','Bensin'),
(15,'BEAT','AB1228NCC','Merah','2018','2',0,0,'62585f3bd53db.jpg','Bensin'),
(17,'CB 150 R','AB6727DDP','Hitam','2017','1',0,0,'6572c72a787a9.jpeg','Bensin');

/*Table structure for table `tarif` */

DROP TABLE IF EXISTS `tarif`;

CREATE TABLE `tarif` (
  `id_tarif` int(11) NOT NULL AUTO_INCREMENT,
  `id_motor_fk` int(11) DEFAULT NULL,
  `id_jenis_tarif_fk` int(11) DEFAULT NULL,
  `harga_tarif` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_tarif`),
  KEY `id_mobil_fk` (`id_motor_fk`),
  KEY `id_jenis_tarif_fk` (`id_jenis_tarif_fk`),
  KEY `id_motor_fk` (`id_motor_fk`),
  KEY `id_jenis_tarif_fk_2` (`id_jenis_tarif_fk`),
  CONSTRAINT `tarif_ibfk_1` FOREIGN KEY (`id_motor_fk`) REFERENCES `motor` (`id_motor`),
  CONSTRAINT `tarif_ibfk_2` FOREIGN KEY (`id_jenis_tarif_fk`) REFERENCES `jenis_tarif` (`id_jenis_tarif`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

/*Data for the table `tarif` */

insert  into `tarif`(`id_tarif`,`id_motor_fk`,`id_jenis_tarif_fk`,`harga_tarif`) values 
(34,13,4,75000),
(36,13,1,15000),
(37,13,2,25000),
(38,17,3,45000),
(39,17,4,70000),
(40,17,7,230000),
(41,15,2,25000),
(42,15,3,45000),
(43,15,7,250000),
(44,13,3,10000),
(45,15,1,45777);

/*Table structure for table `transaksi` */

DROP TABLE IF EXISTS `transaksi`;

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer_fk` int(11) NOT NULL,
  `id_motor_fk` int(11) NOT NULL,
  `id_tarif_fk` int(11) DEFAULT NULL,
  `tanggal_rental` datetime DEFAULT NULL,
  `tanggal_kembali` datetime DEFAULT NULL,
  `total_harga` varchar(20) DEFAULT NULL,
  `total_denda` varchar(20) DEFAULT NULL,
  `tanggal_pengembalian` datetime DEFAULT NULL,
  `status_transaksi` varchar(50) DEFAULT NULL,
  `jam_terlambat` int(11) DEFAULT NULL,
  `kode_transaksi` int(12) DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`),
  KEY `id_customer_fk` (`id_customer_fk`),
  KEY `id_mobil_fk` (`id_motor_fk`),
  KEY `id_tarif_fk` (`id_tarif_fk`),
  CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_customer_fk`) REFERENCES `customer` (`id_customer`),
  CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_motor_fk`) REFERENCES `motor` (`id_motor`),
  CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`id_tarif_fk`) REFERENCES `tarif` (`id_tarif`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `transaksi` */

insert  into `transaksi`(`id_transaksi`,`id_customer_fk`,`id_motor_fk`,`id_tarif_fk`,`tanggal_rental`,`tanggal_kembali`,`total_harga`,`total_denda`,`tanggal_pengembalian`,`status_transaksi`,`jam_terlambat`,`kode_transaksi`) values 
(6,98,13,36,'2023-12-11 02:27:00','2023-12-13 01:27:00','15000','220000','2023-12-14 00:00:00','2',22,NULL),
(7,99,17,39,'2023-11-11 15:42:00','2023-11-12 02:13:00','70000','1410000','2023-11-18 00:00:00','2',141,NULL),
(8,100,17,40,'2023-10-01 23:59:00','2023-10-17 23:59:00','230000','2400000','2023-12-28 00:00:00','2',240,NULL),
(9,101,17,39,'2023-09-12 23:59:00','2023-09-14 23:59:00','70000','2190000','2023-09-24 03:00:00','2',219,NULL),
(10,102,15,45,'2023-08-12 23:59:00','2023-08-12 03:02:00','45777','4710000','2023-08-31 18:11:00','2',471,NULL),
(11,103,13,37,'2023-07-12 23:59:00','2023-07-14 02:04:00','25000','3600000','2023-07-29 03:00:00','2',360,NULL),
(12,104,13,36,'2023-06-12 01:05:00','2023-06-13 02:05:00','15000','2610000','2023-06-24 00:00:00','2',261,NULL),
(13,105,17,38,'2023-05-02 01:06:00','2023-05-09 01:06:00','45000','5020000','2023-05-30 00:00:00','2',502,NULL),
(14,106,15,41,'2023-04-12 02:07:00','2023-04-15 03:07:00','25000','3560000','2023-04-30 00:00:00','2',356,NULL),
(15,107,17,39,'2023-03-12 01:08:00','2023-03-08 02:08:00','70000','3570000','2023-03-23 00:00:00','2',357,NULL),
(16,108,17,39,'2023-02-21 02:09:00','2023-02-23 04:09:00','70000','1150000','2023-02-28 00:00:00','2',115,NULL),
(17,109,15,43,'2023-01-01 02:10:00','2023-01-07 00:12:00','250000','5750000','2023-01-31 00:00:00','2',575,NULL),
(18,110,17,38,'2023-12-11 01:13:00','2023-12-13 03:13:00','45000','2360000','2023-12-23 00:00:00','2',236,NULL),
(19,111,17,39,'2023-10-13 01:14:00','2023-12-13 02:14:00','70000','0','2023-12-12 00:00:00','2',0,NULL),
(20,112,17,40,'2023-12-12 02:25:00','2023-12-13 02:25:00','230000','1650000','2023-12-20 00:00:00','2',165,NULL),
(21,113,17,40,'2023-12-12 02:25:00','2023-12-13 02:25:00','230000','690000','2023-12-16 00:00:00','2',69,NULL),
(22,114,17,40,'2023-12-12 02:29:00','2023-12-13 02:29:00','230000','4050000','2023-12-30 00:00:00','2',405,NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `telpon` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`nama`,`username`,`password`,`telpon`) values 
(1,'Admin','admin','admin','081789000000'),
(4,'ahmad badawei','ahmad','12345','6546546');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

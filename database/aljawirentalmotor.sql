-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Jun 2022 pada 18.20
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aljawirentalmotor`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL,
  `nama_customer` varchar(120) NOT NULL,
  `tempat_lahir` varchar(120) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` varchar(120) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `telpon` varchar(20) NOT NULL,
  `foto_ktp` varchar(50) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `no_ktp` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`id_customer`, `nama_customer`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `gender`, `telpon`, `foto_ktp`, `image`, `no_ktp`) VALUES
(64, 'Dadang', 'Jakarta', '1994-02-12', 'Lenteng Agung, Jagakarsa, Jakarta Selatan', 'L', '081554000000', '625cedf477248.png', NULL, NULL),
(65, 'Nabilah', 'Surabaya', '1999-05-02', 'Ploso, Tambaksari, Surabaya', 'P', '085443000000', '62585a0cf358a.png', NULL, NULL),
(66, 'Hansu', 'Malang', '1997-01-24', 'Tasikmadu, Lowokwaru, Malang', 'L', '089189000000', '', NULL, NULL),
(67, 'Yijin', 'Semarang', '1983-04-12', 'Rowosari, Tembalang, Semarang', 'L', '081996000000', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `denda`
--

CREATE TABLE `denda` (
  `id_denda` int(11) NOT NULL,
  `denda` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `denda`
--

INSERT INTO `denda` (`id_denda`, `denda`) VALUES
(1, 10000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_tarif`
--

CREATE TABLE `jenis_tarif` (
  `id_jenis_tarif` int(11) NOT NULL,
  `jenis_tarif` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_tarif`
--

INSERT INTO `jenis_tarif` (`id_jenis_tarif`, `jenis_tarif`) VALUES
(1, '6 Jam'),
(2, '12 Jam'),
(3, '1 Hari'),
(4, '2 Hari'),
(5, '3 Hari'),
(6, '5 Hari'),
(7, '1 Minggu'),
(8, '2 Minggu'),
(9, '3 Minggu'),
(10, '1 Bulan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `motor`
--

CREATE TABLE `motor` (
  `id_motor` int(11) NOT NULL,
  `merek` varchar(120) NOT NULL,
  `no_plat` varchar(20) NOT NULL,
  `warna` varchar(20) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `status` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  `denda` int(11) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `bahan_bakar` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `motor`
--

INSERT INTO `motor` (`id_motor`, `merek`, `no_plat`, `warna`, `tahun`, `status`, `harga`, `denda`, `gambar`, `bahan_bakar`) VALUES
(13, 'MIO 123', 'AB3454BBV', 'Merah Muda', '2019', '1', 0, 0, '625cecd7ea7b8.jpg', 'Bensin'),
(15, 'BEAT', 'AB1228NCC', 'Merah', '2018', '1', 0, 0, '62585f3bd53db.jpg', 'Bensin'),
(17, 'Supra', 'AB6727DDP', 'Hijau', '2017', '1', 0, 0, '62586000de6fa.png', 'Bensin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tarif`
--

CREATE TABLE `tarif` (
  `id_tarif` int(11) NOT NULL,
  `id_motor_fk` int(11) DEFAULT NULL,
  `id_jenis_tarif_fk` int(150) DEFAULT NULL,
  `harga_tarif` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tarif`
--

INSERT INTO `tarif` (`id_tarif`, `id_motor_fk`, `id_jenis_tarif_fk`, `harga_tarif`) VALUES
(34, 13, 4, 75000),
(36, 13, 1, 15000),
(37, 13, 2, 25000),
(38, 17, 3, 45000),
(39, 17, 4, 70000),
(40, 17, 7, 230000),
(41, 15, 2, 25000),
(42, 15, 3, 45000),
(43, 15, 7, 250000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_customer_fk` int(11) NOT NULL,
  `id_motor_fk` int(11) NOT NULL,
  `id_tarif_fk` int(11) DEFAULT NULL,
  `tanggal_rental` datetime DEFAULT NULL,
  `tanggal_kembali` datetime DEFAULT NULL,
  `total_harga` varchar(120) DEFAULT NULL,
  `total_denda` varchar(120) DEFAULT NULL,
  `tanggal_pengembalian` datetime DEFAULT NULL,
  `status_transaksi` varchar(50) DEFAULT NULL,
  `jam_terlambat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_customer_fk`, `id_motor_fk`, `id_tarif_fk`, `tanggal_rental`, `tanggal_kembali`, `total_harga`, `total_denda`, `tanggal_pengembalian`, `status_transaksi`, `jam_terlambat`) VALUES
(43, 64, 13, 34, '2022-02-25 07:53:00', '2022-02-28 07:53:00', '70000', '10000', '2022-02-28 09:00:00', '2', 1),
(44, 65, 13, 34, '2022-02-25 09:28:00', '2022-02-27 09:28:00', '70000', '140000', '2022-02-28 00:00:00', '2', 14),
(45, 66, 17, 39, '2022-04-10 12:02:00', '2022-04-12 12:02:00', '70000', '20000', '2022-04-12 14:06:00', '2', 2),
(46, 67, 15, 43, '2022-04-16 19:00:00', '2022-04-17 19:00:00', '250000', '1440000', '2022-04-23 19:00:00', '1', 144);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(120) NOT NULL,
  `username` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `telpon` varchar(15) DEFAULT NULL,
  `level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama`, `username`, `password`, `telpon`, `level`) VALUES
(1, 'Admin', 'admin', 'admin', '081789000000', 1),
(3, 'Pramita', 'mita', 'mita', '085701000000', 2);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indeks untuk tabel `denda`
--
ALTER TABLE `denda`
  ADD PRIMARY KEY (`id_denda`);

--
-- Indeks untuk tabel `jenis_tarif`
--
ALTER TABLE `jenis_tarif`
  ADD PRIMARY KEY (`id_jenis_tarif`);

--
-- Indeks untuk tabel `motor`
--
ALTER TABLE `motor`
  ADD PRIMARY KEY (`id_motor`);

--
-- Indeks untuk tabel `tarif`
--
ALTER TABLE `tarif`
  ADD PRIMARY KEY (`id_tarif`),
  ADD KEY `id_mobil_fk` (`id_motor_fk`),
  ADD KEY `id_jenis_tarif_fk` (`id_jenis_tarif_fk`),
  ADD KEY `id_motor_fk` (`id_motor_fk`),
  ADD KEY `id_jenis_tarif_fk_2` (`id_jenis_tarif_fk`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_customer_fk` (`id_customer_fk`),
  ADD KEY `id_mobil_fk` (`id_motor_fk`),
  ADD KEY `id_tarif_fk` (`id_tarif_fk`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT untuk tabel `denda`
--
ALTER TABLE `denda`
  MODIFY `id_denda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `jenis_tarif`
--
ALTER TABLE `jenis_tarif`
  MODIFY `id_jenis_tarif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `motor`
--
ALTER TABLE `motor`
  MODIFY `id_motor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `tarif`
--
ALTER TABLE `tarif`
  MODIFY `id_tarif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tarif`
--
ALTER TABLE `tarif`
  ADD CONSTRAINT `tarif_ibfk_1` FOREIGN KEY (`id_motor_fk`) REFERENCES `motor` (`id_motor`),
  ADD CONSTRAINT `tarif_ibfk_2` FOREIGN KEY (`id_jenis_tarif_fk`) REFERENCES `jenis_tarif` (`id_jenis_tarif`);

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_customer_fk`) REFERENCES `customer` (`id_customer`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_motor_fk`) REFERENCES `motor` (`id_motor`),
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`id_tarif_fk`) REFERENCES `tarif` (`id_tarif`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

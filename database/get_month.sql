/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.4.19-MariaDB : Database - dml_231008
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dml_231008` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `dml_231008`;

/*Table structure for table `get_month` */

DROP TABLE IF EXISTS `get_month`;

CREATE TABLE `get_month` (
  `id_get_month` int(11) NOT NULL AUTO_INCREMENT,
  `bulan` varchar(15) DEFAULT NULL,
  `value` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id_get_month`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `get_month` */

insert  into `get_month`(`id_get_month`,`bulan`,`value`) values 
(1,'Januari','01'),
(2,'Februari','02'),
(3,'Maret','03'),
(4,'April','04'),
(5,'Mei','05'),
(6,'Juni','06'),
(7,'Juli','07'),
(8,'Agustus','08'),
(9,'September','09'),
(10,'Oktober','10'),
(11,'November','11'),
(12,'Desember','12');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
